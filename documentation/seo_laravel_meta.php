<?php
$title = 'Login Page';
$description = 'Login Page | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are';
$page = 'login page';
meta()->set('csrf-token', csrf_token())
->set('charset', 'utf-8')
->title($title = 'Login Page')->set('og:title', $title)
->description($description = 'Login Page | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are')
->set('og:description', $description)
->keywords('my cloud player '.$page.',
            your own media music player online '.$page.',
            private music player '.$page.',
            private online music player '.$page.',
            private offline music player '.$page.', 
            private online offline music player $page,
            storage music player '.$page.', 
            cache music player '.$page.',
            local music player '.$page.', 
            portable music player '.$page.',
            free 4GB online storage music player '.$page)
->canonical(URL::current())
->robots('all')
->viewport('initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no')
->set('apple-mobile-web-app-capable', 'yes')
->set('apple-touch-fullscreen', 'yes')
->setRawTag('<link rel="preconnect" href="https://fonts.googleapis.com" />')
->setRawTag('<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />')
->setRawTag('<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif+Georgian:wght@100;400;900&family=Nunito:wght@400;700&display=swap">')
->setRawTag('<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>')
->setRawTag('<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>')
->setRawTag('<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>');
?>