css dependency {
    Bulma CSS (installed)
    https://bulma.io/
}

laravel dependency {
    Laravel Breeze (installed)
    https://laravel.com/docs/10.x/starter-kits

    Laravel Telescope
    https://laravel.com/docs/10.x/telescope

    Laravel Application's Security Using a CSP
    https://laravel-news.com/laravel-content-security-policies
    https://spatie.be/docs/laravel-permission/v5/introduction

    Laravel Socialite
    https://github.com/laravel/socialite

    Laravel Meta (installed)
    https://github.com/f9webltd/laravel-meta

    Captcha
    https://www.positronx.io/laravel-captcha-tutorial-example/
}

vuejs dependency {
    Vue Helmet
    https://www.npmjs.com/package/vue-helmet

    Vue Filepond
    https://www.npmjs.com/package/vue-filepond

    Vue Validation (installed)
    https://github.com/logaretm/vee-validate

    Vue Draggable
    https://www.npmjs.com/package/vuedraggable

    Vue CryptoJS (installed)
    https://www.npmjs.com/package/vue-cryptojs

    Swiper.js (installed)
    https://swiperjs.com/vue

    Sweet Alert (installed)
    https://sweetalert2.github.io/

    js-cookie (installed)
    https://github.com/js-cookie/js-cookie
}








