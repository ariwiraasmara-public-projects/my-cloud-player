HTTP response status codes
https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

Laravel Response Classes
https://laravel-news.com/laravel-response-classes

A Laravel Package to Protect Routes With a PIN Code
https://laravel-news.com/requirepin-for-laravel

A PHP SDK for the Dolby API
https://laravel-news.com/a-php-sdk-for-the-dolby-api

Vue drag-and-drop component based on Sortable.js
https://github.com/SortableJS/Vue.Draggable
