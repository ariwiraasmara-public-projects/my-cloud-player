import { inject } from 'vue';


export function getText() {
    // Buat custom hook untuk mengakses variabel tersebut di komponen lain
    return inject('ptext');
}
