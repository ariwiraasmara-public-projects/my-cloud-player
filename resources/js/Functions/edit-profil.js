function detailformcard(id) {
    if( $("#iconup-"+id).hasClass("is-hidden") ) {
        $("#iconup-"+id).removeClass("is-hidden");
        $("#formcard-"+id).removeClass("is-hidden");
        $("#icondown-"+id).addClass("is-hidden");
    }
    else cancelformcard(id);
}

function editinformation(id) {
    if((id == 'pass') || (id == 'pin')) console.log('edit information',id);

    $("#formcardfill-"+id).prop("required", true);
    $("#formcardfill-"+id).prop("disabled", false);
    $("#formcardedit-"+id).prop("disabled", true);
    $("#formcardsave-"+id).prop("disabled", false);
}

function cancelformcard(id) {
    if( $("#icondown-"+id).hasClass("is-hidden") ) {
        $("#iconup-"+id).addClass("is-hidden");
        $("#formcard-"+id).addClass("is-hidden");
        $("#icondown-"+id).removeClass("is-hidden");
    
        $("#formcardfill-"+id).prop("required", false);
        $("#formcardfill-"+id).prop("disabled", true);
        $("#formcardedit-"+id).prop("disabled", false);
        $("#formcardsave-"+id).prop("disabled", true);
    }
}