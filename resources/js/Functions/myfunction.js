class myfunction {


    constructor() {
        let title = null;
        let title_company = null;
        let project_title = null;
    }

    static setTitle(title = null) {
        try {
            this.title = title;
        }
        catch(err) {
            console.log('myfunction setTitle(): ' + err);
        }
    }

    static setTitle_Company(title = null) {
        try {
            this.title_company = title;
        }
        catch(err) {
            console.log('myfunction setTitle_Company(): ' + err);
        }
    }

    setProjectTitle(title = null) {
        try {
            this.project_title = title;
        }
        catch(err) {
            console.log('myfunction setProjectTitle(): ' + err);
        }
    }

     setSepatatorTitle(title = null) {
        try {
            this.separator_title = title;
        }
        catch(err) {
            console.log('myfunction setSepatatorTitle(): ' + err);
        }
    }

    getTitle() {
        try {
            return this.title;
        }
        catch(err) {
            console.log('myfunction getTitle(): ' + err);
        }
    }

    getProjectTitle() {
        try {
            return this.project_title;
        }
        catch(err) {
            console.log('myfunction getProjectTitle(): ' + err);
        }
    }

    getSeparatorTitle() {
        try {
            return this.separator_title;
        }
        catch(err) {
            console.log('myfunction getSeparatorTitle(): ' + err);
        }
    }

    getDocTitle(title = "__-__") {
        document.title = this.getTitle() + this.getSeparatorTitle() + this.getProjectTitle();
        $("#" + title).html(this.getTitle());
    }

    redirect(str) {
        window.location.href = str;
    }

    escapeHtml(text) {
        var map = {
          '&': '&amp;',
          '<': '&lt;',
          '>': '&gt;',
          '"': '&quot;',
          "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }

    readableHTML(text) {
        var entities = [
            ['amp', '&'],
            ['apos', '\''],
            ['#x27', '\''],
            ['#x2F', '/'],
            ['#39', '\''],
            ['#47', '/'],
            ['lt', '<'],
            ['gt', '>'],
            ['nbsp', ' '],
            ['quot', '"']
        ];

        for (var i = 0, max = entities.length; i < max; ++i)
            text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);

        return text;
    }

    formatNumber(thenumber, koma=0) {
        var withkoma = thenumber.toFixed(koma);
        var parts = withkoma.toString().split(".");
        const numberPart = parts[0];
        const decimalPart = parts[1];
        const thousands = /\B(?=(\d{3})+(?!\d))/g;
        return numberPart.replace(thousands, ".") + (decimalPart ? "," + decimalPart : "");
    }

    rupiah(thenumber) {
        return thenumber.toLocaleString('id-ID', { style: 'currency', currency: 'IDR' })
    }

    encrypt(str) {

    }

    decrypt() {

    }

    enval(str) {
        return "<?php echo bin2hex(base64_encode(" + str + ")); ?>";
    }

    denval(str) {
        return "<?php echo base64_decode(hex2bin(" + str + ")); ?>";
    }

    toCrypt(str) {
        return "<?php echo crypt(" + str + ", rand()); ?>";
    }

    toMD5(str, bool) {
        return "<?php echo md5(" + str + ", false); ?>";
    }

    toSHA1(str) {
        return "<?php echo hash('sha512', " + str + "); ?>";
    }

    toSHA512(str) {
        return "<?php echo hash('haval256,5', " + str + "); ?>";
    }

    enlink(str) {
        this.toSHA512(this.toMD5(str));
    }

    enparam() {

    }

    setRawOneCookie(name, val, hari=1, jam=24, menit=60, detik=60) {
        const d = new Date();
        d.setTime(d.getTime() + (hari*jam*menit*detik*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = name + '=' + val + ";" + expires;
    }

    setOneCookie() {

    }

    setCookie() {

    }

    getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    deleteCookie() {

    }

    random(str, length) {
        let seed = null;
        switch(str) {
            case 'char':
                seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            break;
            case 'numb':
                seed = str_split('0123456789');
            break;
            case 'numbwize':
                seed = str_split('123456789');
            break;
            case 'pass':
                seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]}|;:,<.>?');
            break;
            case 'spec':
                seed = str_split('`~!@#$%^&*()-_=+[{]}\'\"|;:,<.>/?/');
            break;
            case 'combwisp':
                seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
            break;
            default:
                seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~!@#$%^&*()-_=+[{]}\'\"|;:,<.>/?/');
        }
    }

}
