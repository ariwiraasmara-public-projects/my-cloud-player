const tabs = document.querySelectorAll('.tabs li')
const tabContentBoxes = document.querySelectorAll('#tab-content > div')
const tablink = document.querySelectorAll('#tabli > a#tab-a')

tabs.forEach((tab) => {
    tab.addEventListener('click', () => {
        tabs.forEach(item => item.classList.remove('is-active'))
            tab.classList.add('is-active');

        const target = tab.dataset.target;
        tabContentBoxes.forEach(box => {
            if(box.getAttribute('id') === target) {
                box.classList.remove('is-hidden');
            }
            else {
                box.classList.add('is-hidden');
            }
        })

        /*
        const targetA = tab.dataset.target;
        tabContentBoxes.forEach(box => {
            if(box.getAttribute('id') === targetA) {
                box.classList.remove('');
            }
            else {
                box.classList.add('');
            }
        })
        */
    })
})
