function setCookie(name, val, hari=1, jam=24, menit=60, detik=60) {
    const d = new Date();
    d.setTime(d.getTime() + (hari*jam*menit*detik*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = name + '=' + val + ";" + expires;
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(name) {
    let cookiename = getCookie(name);
    if (cookiename != "") {
        alert("Exist : " + cookiename);
    }
}

function back(x, y) {
    $(document).ready(function(){
        $('#' + x).addClass('is-hidden');
        $('#' + y).removeClass('is-hidden');
        $("#nav-folder-child").remove();
        $("#item-content").html();
    });
}

function setText(id, value) {
    $("#"+id).val(value);
}

function isFileEdit(type) {
    if(type > 0) {
        $("#genre").prop("readonly", false);
        $("#artist").prop("readonly", false);
        $("#album").prop("readonly", false);
        $("#composer").prop("readonly", false);
        $("#publisher").prop("readonly", false);
        $("#description").prop("readonly", false);
    }
    $("#genre").prop("readonly", true);
    $("#artist").prop("readonly", true);
    $("#album").prop("readonly", true);
    $("#composer").prop("readonly", true);
    $("#publisher").prop("readonly", true);
    $("#description").prop("readonly", true);
}
