export interface UserGSInterface {

    construct(nama:string, tempat_lahir:string, tgl_lahir:string,
              pass:string, pin:number, alamat:string, path:string)

    getNama(): string
    setNama(nama: string): void
    getTempatlahir(): string
    setTempatlahir(tempat_lahir: string): void
    getTgllahir(): string
    setTgllahir(tgl_lahir: string): void
    getPass(): string
    setPass(pass: string): void
    getPin(): number
    setPin(pin: number): void
    getAlamat(): string
    setAlamat(alamat: string): void
    getPath(): string
    setPath(path: string): void
}

export class UserGS implements UserGSInterface {

    protected nama:string;
    protected tempat_lahir:string;
    protected tgl_lahir:string;
    protected pass:string;
    protected pin:number;
    protected alamat:string;
    protected path:string;

    construct(nama:string, tempat_lahir:string, tgl_lahir:string,
        pass:string, pin:number, alamat:string, path:string) {

    }

    public getNama(): string {
        return this.nama;
    }

    public setNama(nama: string): void {
        this.nama = nama;
    }

    public getTempatlahir(): string {
        return this.tempat_lahir;
    }

    public setTempatlahir(tempat_lahir: string): void {
        this.tempat_lahir = tempat_lahir;
    }

    public getTgllahir(): string {
        return this.tgl_lahir;
    }

    public setTgllahir(tgl_lahir: string): void {
        this.tgl_lahir = tgl_lahir;
    }

    public getPass(): string {
        return this.pass;
    }

    public setPass(pass: string): void {
        this.pass = pass;
    }

    public getPin(): number {
        return this.pin;
    }

    public setPin(pin: number): void {
        this.pin = pin;
    }

    public getAlamat(): string {
        return this.alamat;
    }

    public setAlamat(alamat: string): void {
        this.alamat = alamat;
    }

    public getPath(): string {
        return this.path;
    }

    public setPath(path: string): void {
        this.path = path;
    }

}
