import { ref, provide, inject } from 'vue';

// Buat variabel reaktif yang akan dibagikan
const ptheme = ref('');
const pbar = ref('');
const ptext = ref('');

// Berikan variabel tersebut
provide('ptheme', ptheme);
provide('pbar', pbar);
provide('ptext', ptext);

export function setTheme(newValue) {
    ptheme.value = newValue;
}

export function setBar(newValue) {
    pbar.value = newValue;
}

export function setText(newValue) {
    ptext.value = newValue;
}

export function getTheme() {
    // Buat custom hook untuk mengakses variabel tersebut di komponen lain
    return inject('ptheme');
}

export function getBar() {
    // Buat custom hook untuk mengakses variabel tersebut di komponen lain
    return inject('pbar');
}

export function getText() {
    // Buat custom hook untuk mengakses variabel tersebut di komponen lain
    return inject('ptext');
}
