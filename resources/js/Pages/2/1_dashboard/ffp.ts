import axios from 'axios';
// Folder {
    export async function getFolders(id: String, order: String, by: String) {
        try {
            const response = await axios.get(`../api/user/folder/get/${id}/${order}/${by}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            // console.log('response getFolders', response.data.data);
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function detailFolder(id: String) {
        try {
            const response = await axios.get(`../api/user/folder/detail/${id}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function createFolder(foldername: String) {
        try {
            const response = await axios.post(`../api/user/folder/create/${this.getCookie('mcr_x_aswq_2')}`, {
                'foldername': foldername,
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function updateFolder(id: String, foldername: String) {
        try {
            const response = await axios.post(`../api/user/folder/update/${id}`, {
                'foldername': foldername,
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function deleteFolder(id: String) {
        try {
            const response = await axios.post(`../api/user/folder/delete/${id}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }
// }

// PLAYLIST {
    export async function getPlaylists(id: String, by: String) {
        try {
            const response = await axios.get(`../api/user/playlist/get/${id}/${by}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            // console.log('response getPlaylists', response.data.data);
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function detailPlaylist(id: String) {
        try {
            const response = await axios.get(`../api/user/playlist/detail/${id}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function createPlaylist(foldername: String) {
        try {
            const response = await axios.post(`../api/user/playlist/create/${this.getCookie('mcr_x_aswq_2')}`, {
                'foldername': foldername,
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function updatePlaylist(id: String, foldername: String) {
        try {
            const response = await axios.post(`../api/user/playlist/update/${id}`, {
                'foldername': foldername,
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function deletePlaylist(id: String) {
        try {
            const response = await axios.post(`../api/user/playlist/delete/${id}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }
// }

// FILE {
    export async function uploadFile(filename: String) {
        try {
            const response = await axios.post(`../api/user/file/create/${this.getCookie('mcr_x_aswq_2')}`, {
                'filename': filename,
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function getFiles(id1: String, id2: String, order: String, by: String, fp: String) {
        try {
            const response = await axios.get(`../api/user/file/get/${id1}/${id2}/${order}/${by}/${fp}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            // console.log('response getFiles', response);
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function detailFile(id: String) {
        try {
            const response = await axios.get(`../api/user/file/detail/${id}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function updateFile(id: String, filename: String, genre: String, artist: String, album: String, composer: String, publisher: String, description: String) {
        try {
            const response = await axios.post(`../api/user/file/update/${id}`, {
                'filename': filename,
                'genre': genre,
                'artist': artist,
                'album': album,
                'composer': composer,
                'publisher': publisher,
                'ket': description,
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function movetoFolder(folder1: String, folder2: String) {
        try {
            const response = await axios.post(`../api/user/file/move/${id}`, {
                'from_folder': folder1,
                'to_folder': folder2,
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }

    export async function deleteFile(id: String) {
        try {
            const response = await axios.delete(`../api/user/file/delete/${id}`, {
                'headers': {
                    'accept' : 'application/json',
                    'mcr-x-aswq' : 'mC12x@12!5Of!a'
                }
            });
            return response.data.data;
        }
        catch(error) {
            return error.response;
        }
    }
// }
