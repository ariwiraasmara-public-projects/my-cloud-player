@inject('fun', 'App\MyLibs\myfunction')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title></title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="My Cloud Player | Your Own Media Music Player Online" />
        <meta content="IE=edge" http-equiv="x-ua-compatible">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no" >
        <meta name="apple-mobile-web-app-capable" content="yes" >
        <meta name="apple-touch-fullscreen" content="yes" >

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine"/>
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/additional.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/custel.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.min.css">
        @livewireStyles

        <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
        
    </head>

    <body class="the-content">
        <!-- <div id="notif" class="notification is-hidden top-fixed">
            <button class="delete" onClick="closeNotif()"></button>
            <div id="notif-text"></div>
        </div>  -->

        @yield('content')

        
        @include('js')
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/myutil.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.all.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.0/dist/sweetalert2.all.min.js"></script>

        @isset($_COOKIE['islogin'])
            <script type="text/javascript" src="{{ URL::asset('js/folder_treeview.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('js/edit-profil.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('bulma/js/tabs.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('bulma/js/navs.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('bulma/js/modal.js') }}"></script>
        @endisset

        @livewireScripts
    </body>

    <footer>
        @if ($footer == 1)
            @include('footer_copyright')
        @endif
        @include('0.title')
    </footer>
</html>
