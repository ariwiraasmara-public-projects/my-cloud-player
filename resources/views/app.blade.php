<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title inertia>{{ config('app.name', 'My Cloud Player') }}</title>
        {{-- <meta name="description" content="My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are" /> --}}
        {!! meta()->toHtml() !!}

        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/additional.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/animations.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/custel.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="https://www.w3schools.com/w3css/4/w3.css">

        <!-- Scripts -->
        <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        @routes
        @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
        {{-- @vite(['resources/js/ssr.js', "resources/js/Pages/{$page['component']}.vue"]) --}}
        @inertiaHead
    </head>
    <body class="font-sans antialiased">
        @inertia

        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/myutil.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/folder_treeview.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/edit-profil.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/tabs.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/navs.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/modal.js') }}"></script>
    </body>
</html>
