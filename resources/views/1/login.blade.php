@extends('index')
@section('content')
<div class="p-50 is-mobile">
    <form action="{{ route('loginprocess') }}" method="post" class="box1" name="login_form" id="login_form">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="title text-is-black center bold">{{ $title }}</div>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="field">
            <div class="control has-icons-left has-icons-right">
                <input type="text" class="input" name="user" id="user" placeholder="Username / Email / Phone Number">
                <span class="icon is-small is-left text-is-black bold">
                    <ion-icon name="person-outline"></ion-icon>
                </span>
            </div>
        </div>

        <div class="field">
            <div class="control has-icons-left has-icons-right">
                <input type="password" class="input" name="pass" id="pass" placeholder="********">
                <span class="icon is-small is-left text-is-black bold">
                    <ion-icon name="lock-closed-outline"></ion-icon>
                </span>
            </div>
        </div>

        <div class="m-t-30">
            <div class="buttons has-addons is-centered">
                <button type="submit" name="loginok" id="loginok" class="button is-primary is-link is-rounded is-selected">Log in</button>
                <a href="signin" class="button is-danger is-light is-rounded is-selected">Sign in</a>
            </div>
        </div>

        <div class="field m-t-20 right">
            <p class="control">
                <a href="{{ route('aboutus') }}" class="tag is-link is-success is-rounded">What About Us?</a>
                <a href="{{ route('storagelevelup') }}" class="tag is-danger is-rounded">Level Up!</a>
                <a href="{{ route('forgotpassword') }}" class="tag is-link is-rounded">Forgot Password</a>
            </p>
        </div>
    </form>

    @if(Session::has('msg'))
    <div class="m-t-30 center" id="notif">
        <div id="notif" class="notification is-hidden top-fixed">
            <button class="delete" onClick="closeNotif()"></button>
            <div id="notif-text"></div>
        </div>

        <div id="notif" class="notification text-is-center {{ Session::get('typenotif'); }}">
            <button class="delete" onClick="closeNotif()"></button>
            <div id="notif-text">
                {{ html_entity_decode(Session::get('msg')); }}
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
