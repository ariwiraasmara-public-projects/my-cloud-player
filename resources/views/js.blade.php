<script>
    @if( !isset($_COOKIE['islogin']) ) 
        $('#tologin').click(function() {
            window.location.href = "{!! route('login') !!}";
        });
    @endif

    @isset($_COOKIE['islogin'])
        $('#tologout').click(function() {
            window.location.href = "{!! route('logout') !!}";
        });

        $('#toeditprofile').click(function() {
            $('#profile').addClass('is-hidden');
            $('#nav-bottom').addClass('is-hidden');
            $('#editprofile').removeClass('is-hidden');

        });

        $('#toprofile').click(function() {
            $('#profile').removeClass('is-hidden');
            $('#nav-bottom').removeClass('is-hidden');
            $('#editprofile').addClass('is-hidden');
            cancelformcard('nama');
            cancelformcard('pass');
            cancelformcard('pin');
            cancelformcard('tlp');
            cancelformcard('tgl');
            $(".delete").click();
            $(document).ready(function(){
                $(".delete").click();
            });
        });

        function folderpopup(x, y, z) {
            $('#modal-folder').addClass('is-active');
            $('#modal-folder').removeClass('is-hidden');
            $('#modal-title-folder').html(y);
            $('#modal-text').html(z);
            // console.log('safe');
        }

        function itempopup(x, y) {
            const token = $("meta[name='csrf-token']").attr("content");
            $('#modal-file').addClass('is-active');
            $('#modal-file').removeClass('is-hidden');
            $('#modal-title-file').html(y);
            
            $.ajax({
                url: "{!! route('userdetailfile') !!}",
                type: "POST",
                cache: true,
                data: {
                    "_token": token,
                    "file": x,
                },
                headers: {
                    'X-CSRF-TOKEN': token,
                    'mcr-x-aswq' : 'mC12x@12!5Of!a',
                    'Accept' : 'application/json'
                },
                success:function(response){
                    // alert('SUCCESS!');
                    // $('#modal-text-file').html(response);
                },
                error:function(error){
                    // console.log("error : ", error);
                    onNotif("Error! Can't Update User Profile!<br/>"+error, "is-danger");
                }
            });

            // $('#modal-text-file').html(z);
        }

        function item(item, idsf, x, y, div_content) {
            const token = $("meta[name='csrf-token']").attr("content");
            $("#" + idsf).addClass('is-hidden');
            $("#" + item).removeClass('is-hidden');
            $("#nav-item-back").removeClass('is-hidden');

            $.ajax({
                url: "{!! route('userfileinfolderorplaylist') !!}",
                type: "POST",
                cache: true,
                data: {
                    "_token": token,
                    "fv": x,
                    "order": "filename",
                    "sort": "asc",
                },
                headers: {
                    'X-CSRF-TOKEN': token,
                    'mcr-x-aswq' : 'mC12x@12!5Of!a',
                    'Accept' : 'application/json'
                },
                success:function(response){
                    // alert('SUCCESS!');
                    $("#nav-folder-child").html(y);
                },
                error:function(error){
                    // console.log("error : ", error);
                    onNotif("Error! Can't Update User Profile!<br/>"+error, "is-danger");
                }
            });

            $.ajax({
                url: "{!! route('userfiletemplate') !!}",
                type: "POST",
                cache: true,
                data: {
                    "_token": token,
                    "fv": x,
                    "order": "filename",
                    "sort": "asc",
                },
                headers: {
                    'X-CSRF-TOKEN': token,
                    'mcr-x-aswq' : 'mC12x@12!5Of!a',
                    'Accept' : 'application/json'
                },
                success:function(response){
                    // alert('SUCCESS!');
                    $("#nav-item-back").removeClass("is-hidden");
                    $("#" + item).removeClass('is-hidden');
                    $("#" + idsf).addClass('is-hidden');
                    $("#" + div_content).html(response);
                },
                error:function(error){
                    // console.log("error : ", error);
                    onNotif("Error! Can't Update User Profile!<br/>"+error, "is-danger");
                }
            });
        }

        function item_back(idsf, item) {
            $("#nav-item-back").addClass("is-hidden");
            $('#' + item).addClass('is-hidden');
            $('#' + idsf).removeClass('is-hidden');
            $("#item-content").html("");
            $("#nav-folder-child").html("");
        }

        $("#file-to-move").change(function(){
            if( $("#file-to-move").val() == 'move_cancel' ) {
                $("#file-to-move").val($("#target option:first").val());
            }
        });

        function movefile(id1, id2) {
            let fv = $("#"+id1+" :selected").val();
            $.ajax({
                url: "{!! route('userfilemoveto') !!}",
                type: "POST",
                cache: true,
                data: {
                    "_token": token,
                    "id": id2,
                    "fv": fv,
                },
                headers: {
                    'X-CSRF-TOKEN': token,
                    'mcr-x-aswq' : 'mC12x@12!5Of!a',
                    'Accept' : 'application/json'
                },
                success:function(response){
                    // alert('SUCCESS!');
                },
                error:function(error){
                    // console.log("error : ", error);
                    onNotif("Error! Can't Update User Profile!<br/>"+error, "is-danger");
                }
            });
        }
    @endisset

    function onNotif(is_text, is_class) {
        $("#notif").removeClass('is-hidden');
        $("#notif").addClass(is_class);
        $("#notif-text").html(is_text);
    }

    function closeNotif() {
        $("#notif").addClass('is-hidden');
        $("#notif").removeClass('is-success');
        $("#notif").removeClass('is-warning');
        $("#notif").removeClass('is-danger');
        $("#notif").removeClass('is-information');
        $("#notif-text").html('');
    }

    // document.addEventListener('DOMContentLoaded', () => {
    //     (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
    //         const $notification = $delete.parentNode;

    //         $delete.addEventListener('click', () => {
    //             // $notification.parentNode.removeChild($notification);
    //             $notification.parentNode.addClass('is-hidden');
    //         });
    //     });
    // });
</script>
