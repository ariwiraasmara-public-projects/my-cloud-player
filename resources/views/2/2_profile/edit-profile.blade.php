@inject('fun', 'App\MyLibs\myfunction')
<div class="is-hidden" id="editprofile">
    <section class="hero nav-theme p-t-10 p-b-10 p-l-20">
        <p class="title bold nav-text">
            <span id="toprofile"><ion-icon name="arrow-back-outline"></ion-icon></span>
            Edit Profile
        </p>
    </section>

    <div class="p-30 text-is-black">
        <!-- Name { -->
            <div class="card m-b-10">
                <header class="card-header" id="formcardbtn-nama" onClick="detailformcard('nama')">
                    <p class="card-header-title">
                        <span class="icon is-small is-left text-is-black bold m-r-10">
                            <ion-icon name="person-outline"></ion-icon>
                        </span> Name
                    </p>
                    <a href="#" class="card-header-icon" aria-label="more options" >
                        <span class="icon" id="icondown-nama">
                            <ion-icon name="chevron-down-outline"></ion-icon>
                        </span>
                        <span class="icon is-hidden" id="iconup-nama">
                            <ion-icon name="chevron-up-outline"></ion-icon>
                        </span>
                    </a>
                </header>
                <div class="is-hidden" id="formcard-nama">
                    <div class="card-content">
                        <div class="content">
                            <div class="field has-addons borad-10">
                                <div class="control is-expanded">
                                    <input type="text" class="input" name="val" id="formcardfill-nama" placeholder="Put Your Name Here.." disabled value="{{ $nama }}">
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardedit-nama" id="formcardedit-nama" href="#edit-nama" class="button is-info" onClick="editinformation('nama')">
                                        <ion-icon name="pencil-outline"></ion-icon>
                                    </button>
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardsave" id="formcardsave-nama" href="#save-nama" value="{{ $fun->encrypt('nama') }}" onClick="profileupdate('nama')" class="button is-success" disabled>
                                        <ion-icon name="save-outline"></ion-icon>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="card-footer" onClick="cancelformcard('nama')">
                        <a id="formcardfooterbtn-nama" href="#cancel-nama" class="card-footer-item">Cancel</a>
                    </footer>
                </div>
            </div>
        <!-- } Name -->

        <!-- Password { -->
            <div class="card m-b-10">
                <header class="card-header" id="formcardbtn-pass" onClick="detailformcard('pass')">
                    <p class="card-header-title">
                        <span class="icon is-small is-left text-is-black bold m-r-10">
                            <ion-icon name="lock-closed-outline"></ion-icon>
                        </span> Password
                    </p>
                    <a href="#" class="card-header-icon" aria-label="more options" >
                        <span class="icon" id="icondown-pass">
                            <ion-icon name="chevron-down-outline"></ion-icon>
                        </span>
                        <span class="icon is-hidden" id="iconup-pass">
                            <ion-icon name="chevron-up-outline"></ion-icon>
                        </span>
                    </a>
                </header>
                <div class="is-hidden" id="formcard-pass">
                    <div class="card-content">
                        <div class="content">
                            <div class="field has-addons borad-10">
                                <div class="control is-expanded">
                                    <input type="text" class="input" name="pass" id="formcardfill-pass" placeholder="Put Your Password Here.." disabled value="***">
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardedit-pass" id="formcardedit-pass" href="#edit-pass" class="button is-info" onClick="editinformation('pass')">
                                        <ion-icon name="pencil-outline"></ion-icon>
                                    </button>
                                </div>
                                <div class="control">
                                    <button type="submit" name="formcardsave" id="formcardsave-pass" href="#save-pass" value="{{ $fun->encrypt('pass') }}" onClick="profileupdate('pass')" class="button is-success" disabled>
                                        <ion-icon name="save-outline"></ion-icon>
                                    </button>
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardsee-pin" id="formcardsee-pass" href="#see-pass" class="button is-danger" value="0" onClick="see('pass')">
                                        <ion-icon name="eye-outline"></ion-icon>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="card-footer" onClick="cancelformcard('pass')">
                        <a id="formcardfooterbtn-pass" href="#cancel-pass" class="card-footer-item">Cancel</a>
                    </footer>
                </div>
            </div>
        <!-- } Password  -->

        <!-- PIN { -->
            <div class="card m-b-10">
                <header class="card-header" id="formcardbtn-pin" onClick="detailformcard('pin')">
                    <p class="card-header-title">
                        <span class="icon is-small is-left text-is-black bold m-r-10">
                            <ion-icon name="lock-closed-outline"></ion-icon>
                        </span> PIN
                    </p>
                    <a href="#" class="card-header-icon" aria-label="more options" >
                        <span class="icon" id="icondown-pin">
                            <ion-icon name="chevron-down-outline"></ion-icon>
                        </span>
                        <span class="icon is-hidden" id="iconup-pin">
                            <ion-icon name="chevron-up-outline"></ion-icon>
                        </span>
                    </a>
                </header>
                <div class="is-hidden" id="formcard-pin">
                    <div class="card-content">
                        <div class="content">
                            <div class="field has-addons borad-10">
                                <div class="control is-expanded">
                                    <input type="text" class="input" name="pin" id="formcardfill-pin" placeholder="Put Your PIN Here.." disabled value="***">
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardedit-pin" id="formcardedit-pin" href="#edit-pin" class="button is-info" onClick="editinformation('pin')">
                                        <ion-icon name="pencil-outline"></ion-icon>
                                    </button>
                                </div>
                                <div class="control">
                                    <button type="submit" name="formcardsave" id="formcardsave-pin" href="#save-pin" value="{{ $fun->encrypt('pin') }}" onClick="profileupdate('pin')" class="button is-success" disabled>
                                        <ion-icon name="save-outline"></ion-icon>
                                    </button>
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardsee-pin" id="formcardsee-pin" href="#see-pin" class="button is-danger" onClick="see('pin')">
                                        <ion-icon name="eye-outline"></ion-icon>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="card-footer" onClick="cancelformcard('pin')">
                        <a id="formcardfooterbtn-pin" href="#cancel-pin" class="card-footer-item">Cancel</a>
                    </footer>
                </div>
            </div>
        <!-- } PIN  -->

        <!-- Tlp { -->
            <div class="card m-b-10">
                <header class="card-header" id="formcardbtn-tlp" onClick="detailformcard('tlp')">
                    <p class="card-header-title">
                        <span class="icon is-small is-left text-is-black bold m-r-10">
                            <ion-icon name="phone-portrait-outline"></ion-icon>
                        </span> Phone Number
                    </p>
                    <a href="#" class="card-header-icon" aria-label="more options" >
                        <span class="icon" id="icondown-tlp">
                            <ion-icon name="chevron-down-outline"></ion-icon>
                        </span>
                        <span class="icon is-hidden" id="iconup-tlp">
                            <ion-icon name="chevron-up-outline"></ion-icon>
                        </span>
                    </a>
                </header>
                <div class="is-hidden" id="formcard-tlp">
                    <div class="card-content">
                        <div class="content">
                            <div class="field has-addons borad-10">
                                <div class="control is-expanded">
                                    <input type="text" class="input" name="tlp" id="formcardfill-tlp" placeholder="Put Your Phone Number Here.." disabled value="{{ $tlp; }}">
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardedit-tlp" id="formcardedit-tlp" href="#edit-tlp" class="button is-info" onClick="editinformation('tlp')">
                                        <ion-icon name="pencil-outline"></ion-icon>
                                    </button>
                                </div>
                                <div class="control">
                                    <button type="submit" name="formcardsave" id="formcardsave-tlp" href="#save-tlp" value="{{ $fun->encrypt('tlp') }}" onClick="profileupdate('tlp')" class="button is-success" disabled>
                                        <ion-icon name="save-outline"></ion-icon>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="card-footer" onClick="cancelformcard('tlp')">
                        <a id="formcardfooterbtn-tlp" href="#cancel-tlp" class="card-footer-item">Cancel</a>
                    </footer>
                </div>
            </div>
        <!-- } Tlp  -->

        <!-- Tgl { -->
            <div class="card m-b-10">
                <header class="card-header" id="formcardbtn-tgl_lahir" onClick="detailformcard('tgl_lahir')">
                    <p class="card-header-title">
                        <span class="icon is-small is-left text-is-black bold m-r-10">
                            <ion-icon name="calendar-outline"></ion-icon>
                        </span> Birthdate
                    </p>
                    <a href="#" class="card-header-icon" aria-label="more options" >
                        <span class="icon" id="icondown-tgl_lahir">
                            <ion-icon name="chevron-down-outline"></ion-icon>
                        </span>
                        <span class="icon is-hidden" id="iconup-tgl_lahir">
                            <ion-icon name="chevron-up-outline"></ion-icon>
                        </span>
                    </a>
                </header>
                <div class="is-hidden" id="formcard-tgl_lahir">
                    <div class="card-content">
                        <div class="content">
                            <div class="field has-addons borad-10">
                                <div class="control is-expanded">
                                    <input type="date" class="input" name="tgl_lahir" id="formcardfill-tgl_lahir" placeholder="Your Birthdate.." disabled value="{{ $tgl; }}">
                                </div>
                                <div class="control">
                                    <button type="button" name="formcardedit-tgl" id="formcardedit-tgl_lahir" href="#edit-tgl_lahir" class="button is-info" onClick="editinformation('tgl_lahir')">
                                        <ion-icon name="pencil-outline"></ion-icon>
                                    </button>
                                </div>
                                <div class="control">
                                    <button type="submit" name="formcardsave" id="formcardsave-tgl_lahir" href="#save-tgl_lahir" value="{{ $fun->encrypt('tgl_lahir') }}" onClick="profileupdate('tgl_lahir')" class="button is-success" disabled>
                                        <ion-icon name="save-outline"></ion-icon>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="card-footer" onClick="cancelformcard('tgl_lahir')">
                        <a id="formcardfooterbtn-tgl_lahir" href="#cancel-tgl_lahir" class="card-footer-item">Cancel</a>
                    </footer>
                </div>
            </div>
        <!-- } Tgl  -->
    </div>
</div>

<script>
    let xlcb = 1;
    function profileupdate(field) {
        // $("#formcardsave-"+field).click(function(e){
        // e.preventDefault();
        let token   = $("meta[name='csrf-token']").attr("content");
        let fval    = $("#formcardfill-"+field).val();

        // console.log('save : [', id, field, val, ']');

        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': token,
        //         'mcr-x-aswq' : 'mC12x@12!5Of!a',
        //         'Accept' : 'application/json'
        //     }
        // });

        // $.post("{{ route('usereditsave', $fun->getRawCookie('mcr-x-aswq-2')) }}", {
        //     "_token": token,
        //     "field": field,
        //     "val": val,
        // },
        // function(response){
        //     console.log("response : ", response);
        // });

        //ajax
        $.ajax({
            url: "{!! route('usereditsave', $fun->getRawCookie('mcr-x-aswq-2')) !!}",
            type: "POST",
            cache: true,
            data: {
                "_token": token,
                "field": field,
                "val": fval,
            },
            headers: {
                'X-CSRF-TOKEN': token,
                'mcr-x-aswq' : 'mC12x@12!5Of!a',
                'Accept' : 'application/json'
            },
            success:function(response){
                // alert('SUCCESS!');
                console.log(response['res']);
                if(response['res'] > 0) {
                    // onNotif("Update " + field +" success!", "is-success");
                    $("#profile_"+field).html(fval);
                    $("#formcardfill-"+field).val(fval);
                    // console.log("new "+field + " : " + response['val']);

                    $(document).ready(function(){
                        // Do stuff here, including _calling_ codeAddress(), but not _defining_ it!
                        let mcp_upv = "mcp-upv-" + response['mcp_upv'];
                        console.log('mcp-upv-', response['mcp_upv']);
                        setCookie(mcp_upv, response['val']);
                    });
                    Swal.fire({
                        icon: 'success',
                        title: 'Update Profile Sucess!',
                    });
                    cancelformcard(field);
                }
            },
            error:function(error){
                // console.log("error : ", error);
                // onNotif("Error! Can't Update User Profile!<br/>"+error, "is-danger");
                Swal.fire({
                    icon: 'error',
                    title: 'Update Fail!',
                    text: 'Something went wrong! Check your connection!',
                    // footer: '<a href="#">Why do I have this issue?</a>'
                })
            }
        });
    }

    function see(field) {
        let seepp = null;
        if(field == "pass") seepp = "{!! $pass; !!}";
        else if(field == "pin") seepp = "{!! $pin; !!}";
    
        if($("#formcardsee-"+field).val() == 0) {
            $("#formcardsee-"+field).val(1);
            $("#formcardfill-"+field).val(seepp);
            $("#formcardsee-"+field).html('<ion-icon name="eye-off-outline"></ion-icon>');
        }
        else if($("#formcardsee-"+field).val() == 1) {
            $("#formcardsee-"+field).val(0);
            $("#formcardfill-"+field).val("***");
            $("#formcardsee-"+field).html('<ion-icon name="eye-outline"></ion-icon>');
        }
    }
</script>
