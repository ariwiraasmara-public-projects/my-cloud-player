@inject('fun', 'App\MyLibs\myfunction')
<div class="is-hidden" id="profile">
    <section class="hero nav-theme">
        <div class="hero-body nav-theme">
            <p class="title bold italic nav-text" id="profile_nama">{{ $nama }}</p>
            <p class="subtitle nav-theme nav-text"></p>
                <span class="nav-text" id="profile_username">{{ $username }}</span><br/>
                <span class="nav-text" id="profile_email">{{ $email }}</span><br/>
                <span class="nav-text">Level :</span> <span class="nav-text" id="profile_level">{{ $level }}</span>
            </p>
        </div>
    </section>

    <div class="p-30 has-background nav-text">
        <p>
            <span class="bold">Birthdate :</span> <span id="profile_tgl">{{ $tgl }}</span><br/>
            <span class="bold">Phone Number :</span> <span id="profile_tlp">{{ $tlp }}</span><br/>
            <span class="bold">Pin :</span> <span id="profile_pin"></span>{{ $pin }}<br/>
        </p>

        <div class="bottom m-b-50">
            <div class="buttons has-addons is-centered">
                <a href="#levelup" class="button is-primary is-success is-rounded is-selected" title="Level Up!">
                    <ion-icon name="chevron-up-outline"></ion-icon>
                </a>

                <a href="#edit" id="toeditprofile" class="button is-s is-link is-rounded is-selected" title="Edit Profil">
                    <ion-icon name="create-outline"></ion-icon>
                </a>

                <a href="#photo" class="button is-primary is-link is-rounded is-selected" title="Change Photo">
                    <ion-icon name="camera-outline"></ion-icon>
                </a>

                <a href="#logout" id="tologout" class="button is-s is-danger is-rounded is-selected" title="Logout">
                    <ion-icon name="log-out-outline"></ion-icon>
                </a>
            </div>
        </div>
    </div>
</div>

