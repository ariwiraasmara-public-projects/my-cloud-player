@extends('index')
@section('content')
<!--auth-->
<div id="bodyplay">
    @include('2/1_dashboard/dashboard')

    <!--include('2/2_profile/profile')-->
    <livewire:profile />
    <livewire:edit-profile />

    <!--include('2/3_setting/setting')-->
    <livewire:setting />
    @include('2/navbottom')
</div>
<!--endauth-->
@endsection
