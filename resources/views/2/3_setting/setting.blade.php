@inject('fun', 'App\MyLibs\myfunction')
@php 
$bcv = match($stgtheme) {
        '#0A758F' => '#000',
        '#76D7EA' => '#fff',
        default => '#000'
    }; 
@endphp
<div class="is-hidden m-b-0" id="setting">
    <section class="hero nav-theme p-t-10 p-b-10 p-l-20" id="nav_top_setting">
        <p class="title bold nav-text">Setting</p>
    </section>

    <div class="m-t-50 p-10 p-l-20 has-background">
        <form action="" method="post" name="setting_form" id="setting_form">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <div class="control">
                <span class="bold">Theme :</span>
                <label class="radio">
                  <input type="radio" name="theme" id="theme-dark" value="#0A758F" @if($stgtheme == '#0A758F') {{ 'checked' }} @endif> Dark
                </label>
                <label class="radio">
                  <input type="radio" name="theme" id="theme-light" value="#76D7EA" @if($stgtheme == '#76D7EA') {{ 'checked' }} @endif> Light
                </label>
            </div>

            <div class="control m-t-10">
                <span class="bold">Text Color : </span>
                <label class="radio">
                  <input type="radio" name="text" id="text-black" value="#000" @if($stgtext == '#000') {{ 'checked' }} @endif> Black
                </label>
                <label class="radio">
                    <input type="radio" name="text" id="text-grey" value="#787878" @if($stgtext == '#787878') {{ 'checked' }} @endif> Grey
                  </label>
                <label class="radio">
                  <input type="radio" name="text" id="text-white" value="#fff" @if($stgtext == '#fff') {{ 'checked' }} @endif> White
                </label>
            </div>

            @php /*
            <div class="field m-t-10">
                <div class="field-label"></div>
                <div class="field-body">
                    <div class="field is-expanded">
                        <div class="field has-addons">
                            <p class="control">
                                <a class="button is-static text-is-black bold">Bar Color</a>
                            </p>
                            <p class="control is-expanded">
                                <input type="color" name="bar" id="bar" class="input" placeholder="Input RGB Hex Color Code.." value="{{ $stgbar }}">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            */ @endphp


            <div class="file has-name is-fullwidth">
                <label class="file-label">
                    <input class="file-input" type="file" name="wall_img" id="wall_img">
                    <span class="file-cta">
                        <span class="file-icon">
                            <i class="fas fa-upload"></i>
                        </span>
                        <span class="file-label bold text-is-black">
                            You can upload image for wallpaper
                        </span>
                    </span>
                    <span class="file-name">
                        {{ $stgwall_img }}
                    </span>
                </label>
            </div>

            <div class="field m-t-10">
                <div class="field-label"></div>
                <div class="field-body">
                    <div class="field is-expanded">
                        <div class="field has-addons">
                            <p class="control">
                                <a class="button is-static text-is-black bold">Wallpaper Height</a>
                            </p>
                            <p class="control is-expanded">
                                <input type="number" name="wall_height" id="wall_height" class="input" placeholder="Input number of height.." value="{{ $stgwall_height }}">
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="field m-t-10">
                <div class="field-label"></div>
                <div class="field-body">
                    <div class="field is-expanded">
                        <div class="field has-addons">
                            <p class="control">
                                <a class="button is-static text-is-black bold">Wallpaper Width</a>
                            </p>
                            <p class="control is-expanded">
                                <input type="number" name="wall_width" id="wall_width" class="input" placeholder="Input number of width.." value="{{ $stgwall_width }}">
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="field m-t-10">
                <div class="field-label"></div>
                <div class="field-body">
                    <div class="field is-expanded">
                        <div class="field has-addons">
                            <p class="control">
                                <a class="button is-static text-is-black bold">Wallpaper Size</a>
                            </p>
                            <p class="control is-expanded">
                                <input type="text" name="wall_size" id="wall_size" class="input" placeholder="Read the description below.." value="{{ $stgwall_size }}">
                            </p>
                        </div>
                        <p class="help">Choose:
                            <span class="italic underline">auto</span>
                            or
                            <span class="italic underline">contain</span>
                            or
                            <span class="italic underline">number of length, ex: 70 (it will convert to px)</span>
                        </p>
                    </div>
                </div>
            </div>

            <div class="control">
                <div class="select is-fullwidth">
                    <select name="wall_position" id="wall_position" class="text-is-black">
                        <option value="" disabled selected>-- Wallpaper Position --</option>
                        @foreach ($wapo as $wapo)
                            <option value="{{ $wapo }}" @if($wapo == $stgwall_position) {{ 'selected' }} @endif>{{ $wapo }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="control m-t-10">
                <div class="select is-fullwidth">
                    <select name="wall_repeat" id="wall_repeat" class="text-is-black">
                        <option value="" disabled selected>-- Wallpaper Repeat Type --</option>
                        @foreach ($ware as $ware)
                            <option value="{{ $ware }}" @if($ware == $stgwall_repeat) {{ 'selected' }} @endif>{{ $ware }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="control m-t-10">
                <div class="select is-fullwidth">
                    <select name="wall_attachment" id="wall_attachment" class="text-is-black">
                        <option value="" disabled selected>-- Wallpaper Attachment Type --</option>
                        @foreach ($waat as $waat)
                            <option value="{{ $waat }}" @if($waat == $stgwall_attachment) {{ 'selected' }} @endif>{{ $waat }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="m-t-30 m-b-50">
                <div class="buttons is-centered">
                    <button type="button" name="savesetting" id="savesetting" class="button is-primary is-link is-rounded is-selected is-large" onClick="settingup()">
                        <ion-icon name="save-outline"></ion-icon>
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>

<style>
    .bodyplay {
        background-color: #000;
        color: #aaa;
    }

    .nav-theme {
        /*
        For Dark Theme (Default) : #0A758F
        For Ligh Theme : #76D7EA
        background-color: #76D7EA !important;
        */
        background-color: {{ $stgtheme }} !important;
    }

    .nav-top-text {
        color: #9f9f9f !important;
        font-weight: bold !important;
    }

    .nav-text {
        /*
        color: #fff !important;
        */
        color: {{ $stgtext }} !important;
        font-weight: bold !important;
    }

    /*
    body {
        height: 100% !important;
        background-color: {{ $bcv }} !important;
        color: {{ $stgtext }} !important;
    }
    */
</style>

<script>
    function settingup() {
        let token           = $("meta[name='csrf-token']").attr("content");
        let theme           = $("input:radio[name=theme]:checked ").val();
        let text            = $("input:radio[name=text]:checked ").val();
        let bar             = ''; //$("#bar").val();
        let wall_img        = $("#wall_img").val();
        let wall_height     = $("#wall_height").val();
        let wall_width      = $("#wall_width").val();
        let wall_size       = $("#wall_size").val();
        let wall_position   = $("#wall_position").val();
        let wall_repeat     = $("#wall_repeat").val();
        let wall_attachment = $("#wall_attachment").val();

        //ajax
        $.ajax({
            url: "{{ route('usersettingupdate', $fun->getRawCookie('mcr-x-aswq-2')) }}",
            type: "POST",
            cache: false,
            data: {
                "_token": token,
                "theme": theme,
                "text": text,
                "bar": bar,
                "wall_img": wall_img,
                "wall_height": wall_height,
                "wall_width": wall_width,
                "wall_size": wall_size,
                "wall_position": wall_position,
                "wall_repeat": wall_repeat,
                "wall_attachment": wall_attachment,
            },
            headers: {
                'X-CSRF-TOKEN': token,
                'mcr-x-aswq' : 'mC12x@12!5Of!a',
                'Accept' : 'application/json'
            },
            success:function(response){
                // alert('SUCCESS!');
                console.log(response['res']);
                if(response['res'] > 0) {
                    // onNotif("Update " + field +" success!", "is-success");
                    $("#profile_name").html(fval);
                    $("#formcardfill-"+field).val(fval);
                    // console.log("new "+field + " : " + response['val']);

                    $(document).ready(function(){
                        // Do stuff here, including _calling_ codeAddress(), but not _defining_ it!
                        let mcp_upv = "mcp-upv-" + response['mcp_upv'];
                        setCookie(mcp_upv, response['val']);
                    });
                    Swal.fire({
                        icon: 'success',
                        title: 'Update Setting Sucess!',
                    });
                }
            },
            error:function(error){
                // console.log("error : ", error);
                // onNotif("Error! Can't Update User Setting!<br/>"+error, "is-danger");
                Swal.fire({
                    icon: 'error',
                    title: 'Update Fail!',
                    text: 'Something went wrong! Check your connection!',
                    // footer: '<a href="#">Why do I have this issue?</a>'
                })
            }
        });
    }
</script>