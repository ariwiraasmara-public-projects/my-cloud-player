<div class="nav-theme" id="nav-top">
    <div class="tabs is-boxed is-large is-toggle-rounded is-centered" id="tabs">
        <ul>
            <li data-target="musiclist" id="tabli">
                <a href="#musiclist" id="tab-a" title="Music List" class="nav-top-text">
                    <ion-icon name="musical-note-outline"></ion-icon>
                </a>
            </li>
            <li class="is-active" data-target="play" id="tabli">
                <a href="#play" id="tab-a" title="Play" class="nav-top-text">
                    <ion-icon name="play-outline"></ion-icon>
                </a>
            </li>
            <li data-target="folder" id="tabli">
                <a href="#folder" id="tab-a" title="Folder" class="nav-top-text">
                    <ion-icon name="folder-open-outline"></ion-icon>
                </a>
            </li>
            <li data-target="playlist" id="tabli">
                <a href="#playlist" id="tab-a" title="Playlist" class="nav-top-text">
                    <ion-icon name="list-outline"></ion-icon>
                </a>
            </li>
        </ul>
    </div>
</div>
