@inject('fun', 'App\MyLibs\myfunction')
<div id="folder" class="is-hidden">
    <div class="field has-addons">
        <p class="control is-expanded">
            <input class="input is-rounded" type="text" placeholder="Find your music here..">
        </p>
        <p class="control">
            <span id="add-folder" class="button is-success">
                <ion-icon name="add-circle-outline"></ion-icon>
            </span>
        </p>
        <p class="control">
            <span id="upload-file" class="button is-link">
                <ion-icon name="cloud-upload-outline"></ion-icon>
            </span>
        </p>
    </div>

    <div class="m-l-r-10 m-t-10 text-is-black" id="nav-breadcumb">
        <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul id="nav-folder">
                <li id="nav-item-back" class="is-hidden" onClick="item_back('{{ $idsf }}','{{ $item }}')"><ion-icon name="arrow-back-outline"></ion-icon></li>
                <li class="m-l-10"><a href="#">root</a></li>
                <li><a href="#" id="nav-folder-child"></a></li>
            </ul>
        </nav>
    </div>

    <div class="m-t-10 m-b-50 text-is-black" id="{{ $idsf }}">
        @foreach($data as $dt)
            <div id="{{ $folder.$fun->toMD5($dt['id_1004']) }}" class="item-list">
                <div class="columns is-mobile is-responsive">
                    <div class="column" onClick="item('{{ $folder.$fun->toMD5($dt['id_1004']) }}', 
                                                      '{{ $item }}', 
                                                      '{{ $dt['id_1004'] }}', 
                                                      '{{ $dt['foldername'] }}',
                                                      'item-content-folder')">
                        <span id="{{ $list.$fun->toMD5($dt['id_1004']) }}" class="">
                            {{ $dt['foldername'] }}
                        </span>
                    </div>

                    <div class="column right" onclick="folderpopup('{{ $dt['id_1004'] }}', '{{ $dt['foldername'] }}')">
                        <span class="">
                            <ion-icon name="ellipsis-vertical-outline"></ion-icon>
                        </span>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="m-t-10 m-b-50 text-is-black is-hidden" id="{{ $item }}">
        <div id="item-content-folder">
            
        </div>
    </div>
</div>

<div class="modal is-rounded" id="modal-folder">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title text-is-black" id="modal-title-folder">Modal title</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body text-is-black">
            <p class="m-b-0" id="modal-text-folder">Modal text</p>    
        </section>
        <footer class="modal-card-foot center">
            <span class="button" id="btn_modal_edit_data">
                <ion-icon name="create-outline"></ion-icon>
            </span>

            <span class="button" id="btn_modal_delete_data">
                <ion-icon name="trash-outline"></ion-icon>
            </span>
        </footer>
    </div>
</div>

<div class="modal is-rounded" id="modal-file">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title text-is-black" id="modal-title-file">Modal title</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body text-is-black" id="modal-text-file">
            <div class="columns is-mobile is-responsive">
                <div class="column">
                    <div class="field is-half">
                        <div class="control">
                            <input class="input" type="text" id="genre" placeholder="Genre.." readonly>
                        </div>
                    </div>
                </div>

                <div class="column is-half">
                    <div class="field">
                        <div class="control">
                            <input class="input" type="text" id="artist" placeholder="Artist.." readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="columns is-mobile is-responsive">
                <div class="column is-half">
                    <div class="field">
                        <div class="control">
                            <input class="input" type="text" id="album" placeholder="Album.." readonly>
                        </div>
                    </div>
                </div>

                <div class="column is-half">
                    <div class="field">
                        <div class="control">
                            <input class="input" type="text" id="composer" placeholder="Composer.." readonly>
                        </div>
                    </div>
                </div>
            </div>    

            <div class="columns is-mobile is-responsive">
                <div class="column is-half">
                    <div class="field">
                        <div class="control">
                            <input class="input" type="text" id="publisher" placeholder="Publisher.." readonly>
                        </div>
                    </div>
                </div>

                <div class="column is-half">
                    <div class="field">
                        <div class="control">
                            <input class="input" type="text" id="description" placeholder="Description.." readonly>
                        </div>
                    </div>
                </div>
            </div>    
        </section>
        <footer class="modal-card-foot center">
            <span class="button" id="btn_modal_edit_data">
                <ion-icon name="create-outline"></ion-icon>
            </span>

            <span class="button" id="btn_modal_delete_data">
                <ion-icon name="trash-outline"></ion-icon>
            </span>

            <div class="field has-addons">
                <p class="m-l-30 control is-expanded">
                    <span class="select">
                    <select class="is-expanded" id="file-to-move-to-folder">
                        <option value="" disabled selected>-- Choose folder to move --</option>
                        @foreach($data as $dt)
                        <option value="{{ $dt['id_1004'] }}">{{ $dt['foldername'] }}</option>
                        @endforeach
                        <option value="move_cancel">-- Cancel --</option>
                    </select>
                    </span>
                </p>
                <p class="control">
                    <button type="button" class="button" id="btn_modal_oke_to_move_to" onClick="movefile('file-to-move-to-folder', '{{ $dt['id_1004'] }}')">
                        <ion-icon name="enter-outline"></ion-icon>
                    </button>
                </p>
            </div>
        </footer>
    </div>
</div>

<script>
    
</script>
