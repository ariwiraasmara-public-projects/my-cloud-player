@foreach($data as $dt)
<div class="item-list">
    <div class="columns is-mobile is-responsive">
        <div class="column" onClick="play()">
            <span id="file-option">
                {{ $dt['filename'] }}
            </span>
        </div>
        <div class="column right" onClick="itempopup('{{ $dt['id_1006'] }}', '{{ $dt['filename'] }}')">
            <span id="file-option">
                <ion-icon name="ellipsis-vertical-outline"></ion-icon>
            </span>
        </div>
    </div>
</div>
@endforeach