@inject('fun', 'App\MyLibs\myfunction')
<div id="playlist" class="is-hidden has-text-black">
    <div class="">
        @foreach($playlist as $play => $pl)
            <div class="item-list">
                {{ $pl['playlist'] }}
            </div>
        @endforeach
    </div>
</div>
