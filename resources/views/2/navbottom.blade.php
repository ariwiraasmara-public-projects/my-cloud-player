<div class="page-bottom" id="nav-bottom">
    @php /*
    <div class="tabs is-boxed is-large is-toggle-rounded is-centered" id="tabs">
        <ul>
            <li data-target="profile">
                <a href="#profile" id="tab-a" title="Music List" class="nav-top-text">
                    <ion-icon name="person-outline"></ion-icon>
                </a>
            </li>
            <li class="is-active" data-target="dashboard">
                <a href="#dashboard" id="tab-a" title="Play" class="nav-top-text">
                    <ion-icon name="home-outline"></ion-icon>
                </a>
            </li>
            <li data-target="folder">
                <a href="#folder" id="tab-a" title="Folder" class="nav-top-text">
                    <ion-icon name="settings-outline"></ion-icon>
                </a>
            </li>
        </ul>
    </div>
    */ @endphp
    <div class="columns is-mobile nav-theme">
        <div class="column has-text-centered is-size-4 nav-text" id="nb-profile">
            <ion-icon name="person-outline"></ion-icon>
        </div>

        <div class="column has-text-centered is-size-4 nav-text" id="nb-dashboard">
            <ion-icon name="home-outline"></ion-icon>
        </div>

        <div class="column has-text-centered is-size-4 nav-text" id="nb-setting">
            <ion-icon name="settings-outline"></ion-icon>
        </div>
    </div>
</div>
