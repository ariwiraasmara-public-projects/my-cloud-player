import { mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderAttr, ssrRenderSlot } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main$2 = {
  name: "input-text",
  props: {
    type: {
      type: String,
      default: null
    },
    title: {
      type: String,
      default: null
    },
    modelValue: {
      type: String,
      default: null
    },
    hasicon: {
      type: Boolean,
      default: false
    },
    nameinput: {
      type: String,
      default: null
    },
    idinput: {
      type: String,
      default: null
    },
    placeholder: {
      type: String,
      default: null
    }
  }
};
function _sfc_ssrRender$2(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "field" }, _attrs))}>`);
  if ($props.hasicon) {
    _push(`<div class="control has-icons-left"><input${ssrRenderAttr("type", $props.type)}${ssrRenderAttr("name", $props.nameinput)}${ssrRenderAttr("id", $props.idinput)}${ssrRenderAttr("placeholder", $props.placeholder)}${ssrRenderAttr("v-model", $props.modelValue)}${ssrRenderAttr("title", $props.title)} class="input is-rounded"><span class="icon is-small is-left text-is-black bold">`);
    ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
    _push(`</span></div>`);
  } else {
    _push(`<div class="control"><input${ssrRenderAttr("type", $props.type)}${ssrRenderAttr("name", $props.nameinput)}${ssrRenderAttr("id", $props.idinput)}${ssrRenderAttr("placeholder", $props.placeholder)}${ssrRenderAttr("v-model", $props.modelValue)}${ssrRenderAttr("title", $props.title)} class="input is-rounded"></div>`);
  }
  _push(`</div>`);
}
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/MyTextInput.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const Input = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["ssrRender", _sfc_ssrRender$2]]);
const _sfc_main$1 = {
  name: "button",
  props: {
    type: {
      type: String,
      default: null
    },
    title: {
      type: String,
      default: null
    },
    name: {
      type: String,
      default: null
    },
    id: {
      type: String,
      default: null
    }
  }
};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<button${ssrRenderAttrs(mergeProps({
    type: $props.type,
    name: $props.name,
    id: $props.id,
    title: $props.title,
    class: "button is-success is-link is-rounded is-medium"
  }, _attrs))}>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</button>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/MyButton.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const Button = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender$1]]);
const _sfc_main = {
  name: "linkto",
  props: {
    linkto: {
      type: String,
      default: "#"
    },
    title: {
      type: String,
      default: null
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<a${ssrRenderAttrs(mergeProps({
    href: $props.linkto,
    title: $props.title
  }, _attrs))}>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</a>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/MyLink.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Link = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Button as B,
  Input as I,
  Link as L
};
