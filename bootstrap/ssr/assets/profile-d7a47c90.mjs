import axios from "axios";
import { _ as _sfc_main$2 } from "./myfunction-38bad0fd.mjs";
import { _ as _sfc_main$1 } from "./UserGS-f7f3330d.mjs";
import DOMPurify from "isomorphic-dompurify";
import { defineComponent, resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "crypto-js";
import "js-cookie";
const _sfc_main = defineComponent({
  name: "user-profile",
  components: {
    UserGS: _sfc_main$1
  },
  mixins: [_sfc_main$2, _sfc_main$1],
  data() {
    return {
      viewprofiledata: []
      // profile : GS,
    };
  },
  created() {
    this.readDataProfile();
  },
  methods: {
    readDataProfile() {
      axios.get("../api/user/" + this.getCookie("mcr-x-aswq-1"), {
        "headers": {
          "accept": "application/json",
          "mcr-x-aswq": "mC12x@12!5Of!a"
        }
      }).then((response) => {
        this.viewprofiledata = response.data.data;
        this.construct_profile(
          this.viewprofiledata.username,
          this.viewprofiledata.email,
          this.viewprofiledata.nama,
          this.viewprofiledata.tempat_lahir,
          this.viewprofiledata.tgl_lahir,
          this.viewprofiledata.alamat,
          this.viewprofiledata.tlp,
          this.viewprofiledata.level,
          this.viewprofiledata.photo
        );
      }).catch((error) => {
        console.log(error);
      });
    },
    toLogout() {
      window.location.href = "logout";
    },
    toEditprofile() {
      $("#nav-bottom").addClass("is-hidden");
      $("#profile").addClass("is-hidden");
      $("#dashboard").addClass("is-hidden");
      $("#setting").addClass("is-hidden");
      $("#editprofile").removeClass("is-hidden");
      $("#editprofile").addClass("w3-animate-right");
    },
    sanitize(html) {
      const allowedTags = ["p", "strong", "em", "u", "br"];
      const allowedAttributes = {
        "a": ["href", "target"]
      };
      return DOMPurify.sanitize(html, {
        ALLOWED_TAGS: allowedTags,
        ALLOWED_ATTR: allowedAttributes
      });
    }
  }
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<div${ssrRenderAttrs(mergeProps({
    class: "is-hidden",
    id: "profile"
  }, _attrs))}><section class="hero nav-theme"><div class="hero-body nav-theme"><p class="title bold italic nav-text" id="profile_nama">${_ctx.sanitize(_ctx.getNama())}</p><p class="subtitle nav-theme nav-text"><span class="nav-text" id="profile_username">${_ctx.sanitize(_ctx.getUsername())}</span><br><span class="nav-text" id="profile_email">${_ctx.sanitize(_ctx.getEmail())}</span><br><span class="nav-text">Level :</span> <span class="nav-text" id="profile_level">${_ctx.sanitize(_ctx.getLevel())}</span></p></div></section><div class="p-30 has-background nav-text"><p><span class="bold">Birthdate :</span> <span id="profile_tgl">${_ctx.sanitize(_ctx.getTgllahir())}</span><br><span class="bold">Phone Number :</span> <span id="profile_tlp">${_ctx.sanitize(_ctx.getTlp())}</span><br><span class="bold">Pin :</span> <span id="profile_pin">${_ctx.sanitize(_ctx.getPin())}</span><br></p><div class="bottom m-b-50"><div class="buttons has-addons is-centered"><a href="#levelup" class="button is-primary is-success is-rounded is-selected" title="Level Up!">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-up-outline" }, null, _parent));
  _push(`</a><a href="#edit" id="toeditprofile" class="button is-s is-link is-rounded is-selected" title="Edit Profil">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "create-outline" }, null, _parent));
  _push(`</a><a href="#photo" class="button is-primary is-link is-rounded is-selected" title="Change Photo">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "camera-outline" }, null, _parent));
  _push(`</a><a href="#logout" id="tologout" class="button is-s is-danger is-rounded is-selected" title="Logout">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "log-out-outline" }, null, _parent));
  _push(`</a></div></div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/2_profile/profile.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Profile = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Profile as default
};
