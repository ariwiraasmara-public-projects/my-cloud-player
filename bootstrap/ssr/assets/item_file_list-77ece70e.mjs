import { resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrInterpolate, ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = {
  props: {
    isnull: {
      type: Boolean,
      default: false
    },
    id: {
      type: String,
      default: null
    },
    filename: {
      type: String,
      default: null
    },
    text: {
      type: String,
      default: null
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  if ($props.isnull === true) {
    _push(`<div${ssrRenderAttrs(mergeProps({ class: "item-list" }, _attrs))}><div class="columns is-mobile is-responsive"><div class="column" onClick="play()"><span id="file-option">${ssrInterpolate($props.filename)}</span></div><div class="column right" onClick="itempopup(&#39;{{ id }}&#39;, &#39;{{ filename }}&#39;)"><span id="file-option">`);
    _push(ssrRenderComponent(_component_ion_icon, { name: "ellipsis-vertical-outline" }, null, _parent));
    _push(`</span></div></div></div>`);
  } else {
    _push(`<h3${ssrRenderAttrs(mergeProps({ class: "text-is-black item-list" }, _attrs))}>${ssrInterpolate(_ctx.$text)}</h3>`);
  }
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/1_dashboard/2_folder/item_file_list.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const item_file_list = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  item_file_list as default
};
