import axios from "axios";
import { Head } from "@inertiajs/vue3";
import { Form, ErrorMessage } from "vee-validate";
import { I as Input, B as Button, L as Link } from "./MyLink-3afb4754.mjs";
import { resolveComponent, withCtx, createVNode, useSSRContext } from "vue";
import { ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const register_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  name: "register",
  components: {
    Head,
    Input,
    Button,
    Link,
    Form,
    ErrorMessage
  },
  data() {
    return {
      data: []
    };
  },
  props: {
    link_login: {
      type: String,
      default: null
    }
  },
  methods: {
    send() {
      axios.post("../api/user/save", {}).then((response) => Swal.fire({
        title: "Congratulations! You're Data Had Been Save Into Our System!",
        icon: "success",
        confirmButtonText: "OK"
      }).then((result) => {
        this.setNull();
      })).catch((err) => console.log(err)).finally(() => this.loading = false);
    },
    validateEmail(value) {
      if (!value) {
        return "This field is required";
      }
      const regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
      if (!regex.test(value)) {
        return "This field must be a valid email";
      }
      return true;
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Form = resolveComponent("Form");
  const _component_Input = resolveComponent("Input");
  const _component_ion_icon = resolveComponent("ion-icon");
  const _component_ErrorMessage = resolveComponent("ErrorMessage");
  const _component_Link = resolveComponent("Link");
  const _component_Button = resolveComponent("Button");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Sign In" }, null, _parent));
  _push(`<div class="p-50 is-mobile fadein">`);
  _push(ssrRenderComponent(_component_Form, {
    onSubmit: ($event) => _ctx.createUser(),
    name: "register_form",
    id: "register_form",
    class: "boxbf"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="title center bold text-is-black shadowtext-3-3"${_scopeId}>Sign In</div>`);
        _push2(ssrRenderComponent(_component_Input, {
          type: "text",
          nameinput: "username",
          idinput: "username",
          hasicon: true,
          placeholder: "Username.."
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(ssrRenderComponent(_component_ion_icon, { name: "person-outline" }, null, _parent3, _scopeId2));
            } else {
              return [
                createVNode(_component_ion_icon, { name: "person-outline" })
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
        _push2(ssrRenderComponent(_component_Input, {
          type: "text",
          nameinput: "email",
          idinput: "email",
          hasicon: true,
          rules: $options.validateEmail,
          placeholder: "Email.."
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(ssrRenderComponent(_component_ion_icon, { name: "person-outline" }, null, _parent3, _scopeId2));
            } else {
              return [
                createVNode(_component_ion_icon, { name: "person-outline" })
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
        _push2(ssrRenderComponent(_component_ErrorMessage, { name: "email" }, null, _parent2, _scopeId));
        _push2(ssrRenderComponent(_component_Input, {
          type: "password",
          nameinput: "password",
          idinput: "password",
          hasicon: true,
          placeholder: "Password.."
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(ssrRenderComponent(_component_ion_icon, { name: "lock-closed-outline" }, null, _parent3, _scopeId2));
            } else {
              return [
                createVNode(_component_ion_icon, { name: "lock-closed-outline" })
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
        _push2(ssrRenderComponent(_component_Input, {
          type: "password",
          nameinput: "repassword",
          idinput: "repassword",
          hasicon: true,
          placeholder: "Repeat Password.."
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(ssrRenderComponent(_component_ion_icon, { name: "lock-closed-outline" }, null, _parent3, _scopeId2));
            } else {
              return [
                createVNode(_component_ion_icon, { name: "lock-closed-outline" })
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
        _push2(`<div class="m-t-30"${_scopeId}><div class="buttons has-addons is-centered"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_Link, {
          linkto: $props.link_login,
          title: "Login",
          class: "button is-danger is-light is-rounded is-medium"
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(ssrRenderComponent(_component_ion_icon, { name: "arrow-back-outline" }, null, _parent3, _scopeId2));
            } else {
              return [
                createVNode(_component_ion_icon, { name: "arrow-back-outline" })
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
        _push2(ssrRenderComponent(_component_Button, {
          type: "submit",
          name: "registerok",
          id: "registerok",
          title: "OK",
          onClick: ($event) => $options.send(),
          class: "button is-primary is-link is-rounded is-medium"
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(ssrRenderComponent(_component_ion_icon, { name: "log-in-outline" }, null, _parent3, _scopeId2));
            } else {
              return [
                createVNode(_component_ion_icon, { name: "log-in-outline" })
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
        _push2(`</div></div>`);
      } else {
        return [
          createVNode("div", { class: "title center bold text-is-black shadowtext-3-3" }, "Sign In"),
          createVNode(_component_Input, {
            type: "text",
            nameinput: "username",
            idinput: "username",
            hasicon: true,
            placeholder: "Username.."
          }, {
            default: withCtx(() => [
              createVNode(_component_ion_icon, { name: "person-outline" })
            ]),
            _: 1
          }),
          createVNode(_component_Input, {
            type: "text",
            nameinput: "email",
            idinput: "email",
            hasicon: true,
            rules: $options.validateEmail,
            placeholder: "Email.."
          }, {
            default: withCtx(() => [
              createVNode(_component_ion_icon, { name: "person-outline" })
            ]),
            _: 1
          }, 8, ["rules"]),
          createVNode(_component_ErrorMessage, { name: "email" }),
          createVNode(_component_Input, {
            type: "password",
            nameinput: "password",
            idinput: "password",
            hasicon: true,
            placeholder: "Password.."
          }, {
            default: withCtx(() => [
              createVNode(_component_ion_icon, { name: "lock-closed-outline" })
            ]),
            _: 1
          }),
          createVNode(_component_Input, {
            type: "password",
            nameinput: "repassword",
            idinput: "repassword",
            hasicon: true,
            placeholder: "Repeat Password.."
          }, {
            default: withCtx(() => [
              createVNode(_component_ion_icon, { name: "lock-closed-outline" })
            ]),
            _: 1
          }),
          createVNode("div", { class: "m-t-30" }, [
            createVNode("div", { class: "buttons has-addons is-centered" }, [
              createVNode(_component_Link, {
                linkto: $props.link_login,
                title: "Login",
                class: "button is-danger is-light is-rounded is-medium"
              }, {
                default: withCtx(() => [
                  createVNode(_component_ion_icon, { name: "arrow-back-outline" })
                ]),
                _: 1
              }, 8, ["linkto"]),
              createVNode(_component_Button, {
                type: "submit",
                name: "registerok",
                id: "registerok",
                title: "OK",
                onClick: ($event) => $options.send(),
                class: "button is-primary is-link is-rounded is-medium"
              }, {
                default: withCtx(() => [
                  createVNode(_component_ion_icon, { name: "log-in-outline" })
                ]),
                _: 1
              }, 8, ["onClick"])
            ])
          ])
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/1/register.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const register = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  register as default
};
