import axios from "axios";
import { Head } from "@inertiajs/vue3";
import { I as Input, B as Button, L as Link } from "./MyLink-3afb4754.mjs";
import { _ as _sfc_main$1 } from "./myfunction-38bad0fd.mjs";
import Swal from "sweetalert2";
import { defineComponent, resolveComponent, withCtx, createVNode, createTextVNode, useSSRContext } from "vue";
import { ssrRenderComponent, ssrRenderAttr } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "crypto-js";
import "js-cookie";
const _sfc_main = defineComponent({
  name: "login",
  components: {
    Head,
    Input,
    Button,
    Link
    // Fun,
  },
  mixins: [_sfc_main$1],
  props: {
    link_aboutus: {
      type: String,
      default: null
    },
    link_levelup: {
      type: String,
      default: null
    },
    link_forgotpassword: {
      type: String,
      default: null
    }
  },
  data() {
    return {
      user: "",
      pass: "",
      cookie: null
    };
  },
  created() {
  },
  methods: {
    async login() {
      try {
        const response = await axios.post("../api/login", {
          user: this.user,
          pass: this.pass
        });
        this.setCookie("mcr-x-aswq-1", response.data.data.mcr_x_aswq_1, true);
        this.setCookie("mcr-x-aswq-2", response.data.data.mcr_x_aswq_2, true);
        this.setCookie("mcr-x-aswq-3", response.data.data.mcr_x_aswq_3, true);
        this.setCookie("mcr-x-aswq-4", response.data.data.mcr_x_aswq_4, true);
        this.setCookie("mcr-x-aswq-5", response.data.data.mcr_x_aswq_5, true);
        this.setCookie("theme", response.data.data.theme);
        this.setCookie("text", response.data.data.text);
        this.setCookie("bar", response.data.data.bar);
        this.setCookie("wall_img", response.data.data.wall_img);
        this.setCookie("wall_heigth", response.data.data.wall_heigth);
        this.setCookie("wall_width", response.data.data.wall_width);
        this.setCookie("wall_size", response.data.data.wall_size);
        this.setCookie("wall_position", response.data.data.wall_position);
        this.setCookie("wall_repeat", response.data.data.wall_repeat);
        this.setCookie("wall_attachment", response.data.data.wall_attachment);
        Swal.fire({
          title: "You're Login!",
          icon: "success",
          confirmButtonText: "OK"
        }).then((result) => {
        });
      } catch (error) {
        console.error(error);
      }
    }
  }
});
const login_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_ion_icon = resolveComponent("ion-icon");
  const _component_Button = resolveComponent("Button");
  const _component_Link = resolveComponent("Link");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Login" }, null, _parent));
  _push(`<div class="p-50 is-mobile fadein"><form name="login_form" id="login_form" class="boxbf"><div class="title center bold text-is-black shadowtext-3-3">Login</div><div class="field"><div class="control has-icons-left"><input type="text" name="user" id="user"${ssrRenderAttr("value", _ctx.user)} placeholder="Username / Email.." class="input is-rounded"><span class="icon is-small is-left text-is-black bold">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "person-outline" }, null, _parent));
  _push(`</span></div></div><div class="field"><div class="control has-icons-left"><input type="password" name="pass" id="pass"${ssrRenderAttr("value", _ctx.pass)} placeholder="Password.." class="input is-rounded"><span class="icon is-small is-left text-is-black bold">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "person-outline" }, null, _parent));
  _push(`</span></div></div><div class="m-t-30"><div class="buttons has-addons is-centered">`);
  _push(ssrRenderComponent(_component_Button, {
    type: "submit",
    name: "loginok",
    id: "loginok",
    title: "OK",
    class: "button is-success is-link is-rounded is-medium"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_ion_icon, { name: "log-in-outline" }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_ion_icon, { name: "log-in-outline" })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(ssrRenderComponent(_component_Link, {
    linkto: "signin",
    title: "Sign In",
    class: "button is-link is-rounded is-medium"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_ion_icon, { name: "cloud-upload-outline" }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_ion_icon, { name: "cloud-upload-outline" })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div><div class="field m-t-20 right"><p class="control">`);
  _push(ssrRenderComponent(_component_Link, {
    linkto: _ctx.link_aboutus,
    title: "What About Us",
    class: "tag is-link is-info is-rounded"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` What About Us? `);
      } else {
        return [
          createTextVNode(" What About Us? ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(ssrRenderComponent(_component_Link, {
    linkto: _ctx.link_levelup,
    title: "Level Up!",
    class: "tag is-danger is-rounded"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Level Up! `);
      } else {
        return [
          createTextVNode(" Level Up! ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(ssrRenderComponent(_component_Link, {
    linkto: _ctx.link_forgotpassword,
    title: "Forgot Password",
    class: "tag is-link is-rounded"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Forgot Password `);
      } else {
        return [
          createTextVNode(" Forgot Password ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</p></div></form></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/1/login.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const login = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  login as default
};
