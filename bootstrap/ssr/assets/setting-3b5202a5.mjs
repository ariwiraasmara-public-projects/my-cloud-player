import axios from "axios";
import { _ as _sfc_main$1 } from "./myfunction-38bad0fd.mjs";
import Swal from "sweetalert2";
import { defineComponent, useCssVars, resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderAttr, ssrIncludeBooleanAttr, ssrLooseEqual, ssrRenderComponent, ssrInterpolate, ssrRenderList } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "crypto-js";
import "js-cookie";
const __default__ = defineComponent({
  name: "setting",
  components: {
    // Fun,
  },
  mixins: [_sfc_main$1],
  data() {
    return {
      vTheme: null,
      vText: null,
      vBar: null,
      vWall_img: null,
      vWall_heigth: null,
      vWall_width: null,
      vWall_size: null,
      selwapo: null,
      selware: null,
      selwaat: null,
      wapo: [
        { "value": null, "text": "no-position" },
        { "value": "left top", "text": "left top" },
        { "value": "left center", "text": "left center" },
        { "value": "left bottom", "text": "left bottom" },
        { "value": "right top", "text": "right top" },
        { "value": "right center", "text": "right center" },
        { "value": "right bottom", "text": "right bottom" },
        { "value": "center top", "text": "center top" },
        { "value": "center center", "text": "center center" },
        { "value": "center bottom", "text": "center bottom" }
      ],
      ware: [
        { "value": null, "text": "no-repeat" },
        { "value": "repeat", "text": "repeat" },
        { "value": "repeat-x", "text": "repeat-x" },
        { "value": "repeat-y", "text": "repeat-y" }
      ],
      waat: [
        { "value": null, "text": "no-attachment" },
        { "value": "scroll", "text": "scroll" },
        { "value": "fixed", "text": "fixed" }
      ],
      viewsettingdata: []
    };
  },
  computed: {},
  created() {
    this.readDataSetting();
  },
  methods: {
    readDataSetting() {
      this.vTheme = this.getCookie("theme"), this.vText = this.getCookie("text"), this.vBar = this.getCookie("bar"), this.vWall_img = this.getCookie("wall_img"), this.vWall_heigth = this.getCookie("wall_heigth"), this.vWall_width = this.getCookie("wall_width"), this.vWall_size = this.getCookie("wall_size"), this.selwapo = { 0: this.getCookie("wall_position") }, this.selware = { 0: this.getCookie("wall_repeat") }, this.selwaat = { 0: this.getCookie("wall_attachment") };
    },
    settingup() {
      axios.post("../user/setting/save/" + this.getCookie("mcr-x-aswq-2"), {
        theme: this.vTheme,
        text: this.vText,
        bar: this.vBar,
        wall_img: this.vWall_img,
        wall_heigth: this.vWall_heigth,
        wall_width: this.vWall_width,
        wall_size: this.vWall_size,
        wall_position: this.selwapo,
        wall_repeat: this.selware,
        wall_attachment: this.selwaat
      }).then((res) => {
        Swal.fire({
          title: "Success Update Setting!",
          icon: "success",
          confirmButtonText: "OK"
        }).then((result) => {
          this.readDataSetting();
          this.setSettingCookie(
            this.vTheme,
            this.vText,
            this.vBar,
            this.vWall_img,
            this.vWall_heigth,
            this.vWall_width,
            this.vWall_size,
            this.selwapo,
            this.selware,
            this.selwaat
          );
        });
      });
    },
    setSettingCookie(theme = null, text = null, bar = null, wall_img = null, wall_heigth = null, wall_width = null, wall_size = null, wall_position = null, wall_repeat = null, wall_attachment = null) {
      this.setCookie("theme", theme);
      this.setCookie("text", text);
      this.setCookie("bar", bar);
      this.setCookie("wall_img", wall_img);
      this.setCookie("wall_heigth", wall_heigth);
      this.setCookie("wall_width", wall_width);
      this.setCookie("wall_size", wall_size);
      this.setCookie("wall_position", wall_position);
      this.setCookie("wall_repeat", wall_repeat);
      this.setCookie("wall_attachment", wall_attachment);
    }
  }
});
const __injectCSSVars__ = () => {
  useCssVars((_ctx) => ({
    "02aa1536": (--_ctx.background, "#000"),
    "0eeeb6f9": (--_ctx.bartheme, "#c0c0c0"),
    "d9ec4f82": (--_ctx.text, "#fff")
  }));
};
const __setup__ = __default__.setup;
__default__.setup = __setup__ ? (props, ctx) => {
  __injectCSSVars__();
  return __setup__(props, ctx);
} : __injectCSSVars__;
const _sfc_main = __default__;
const setting_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  const _cssVars = { style: {
    "--02aa1536": (--_ctx.background, "#000"),
    "--0eeeb6f9": (--_ctx.bartheme, "#c0c0c0"),
    "--d9ec4f82": (--_ctx.text, "#fff")
  } };
  _push(`<div${ssrRenderAttrs(mergeProps({
    class: "is-hidden m-b-0",
    id: "setting"
  }, _attrs, _cssVars))}><section class="hero nav-theme p-t-10 p-b-10 p-l-20" id="nav_top_setting"><p class="title bold nav-text">Setting</p></section><div class="m-t-50 p-10 p-l-20 has-background"><form class="" name="setting_form" id="setting_form"><input type="hidden" name="_token" value="&lt;?php echo csrf_token(); ?&gt;"><div class="control"><span class="bold">Theme :</span><label class="radio"><input type="radio" name="theme" id="theme-dark"${ssrRenderAttr("value", "#0A758F")}${ssrIncludeBooleanAttr(ssrLooseEqual(_ctx.vTheme, "#0A758F")) ? " checked" : ""}${ssrIncludeBooleanAttr(_ctx.vTheme == "#0A758F") ? " checked" : ""}> Dark </label><label class="radio"><input type="radio" name="theme" id="theme-light"${ssrRenderAttr("value", "#76D7EA")}${ssrIncludeBooleanAttr(ssrLooseEqual(_ctx.vTheme, "#76D7EA")) ? " checked" : ""}${ssrIncludeBooleanAttr(_ctx.vTheme == "#76D7EA") ? " checked" : ""}> Light </label></div><div class="control m-t-10"><span class="bold">Text Color : </span><label class="radio"><input type="radio" name="text" id="text-black"${ssrRenderAttr("value", "#000")}${ssrIncludeBooleanAttr(ssrLooseEqual(_ctx.vText, "#000")) ? " checked" : ""}${ssrIncludeBooleanAttr(_ctx.vText == "#000") ? " checked" : ""}> Black </label><label class="radio"><input type="radio" name="text" id="text-grey"${ssrRenderAttr("value", "#787878")}${ssrIncludeBooleanAttr(ssrLooseEqual(_ctx.vText, "#787878")) ? " checked" : ""}${ssrIncludeBooleanAttr(_ctx.vText == "#787878") ? " checked" : ""}> Grey </label><label class="radio"><input type="radio" name="text" id="text-white"${ssrRenderAttr("value", "#fff")}${ssrIncludeBooleanAttr(ssrLooseEqual(_ctx.vText, "#fff")) ? " checked" : ""}${ssrIncludeBooleanAttr(_ctx.vText == "#fff") ? " checked" : ""}> White </label></div><div class="field m-t-10"><div class="field-label"></div><div class="field-body"><div class="field is-expanded"><div class="field has-addons"><p class="control"><a class="button is-static text-is-black bold">Bar Color</a></p><p class="control is-expanded"><input type="color" name="bar" id="bar" class="input" placeholder="Input RGB Hex Color Code.."${ssrRenderAttr("value", _ctx.vBar)}></p></div></div></div></div><div class="file has-name is-fullwidth"><label class="file-label"><input class="file-input" type="file" name="wall_img" id="wall_img"><span class="file-cta"><span class="file-icon">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "cloud-upload-outline" }, null, _parent));
  _push(`</span><span class="file-label bold text-is-black"> You can upload image for wallpaper </span></span><span class="file-name">${ssrInterpolate(_ctx.viewsettingdata.wall_img)}</span></label></div><div class="field m-t-10"><div class="field-label"></div><div class="field-body"><div class="field is-expanded"><div class="field has-addons"><p class="control"><a class="button is-static text-is-black bold">Wallpaper Height</a></p><p class="control is-expanded"><input type="number" name="wall_height" id="wall_height" class="input" placeholder="Input number of height.."${ssrRenderAttr("value", _ctx.vWall_height)}></p></div></div></div></div><div class="field m-t-10"><div class="field-label"></div><div class="field-body"><div class="field is-expanded"><div class="field has-addons"><p class="control"><a class="button is-static text-is-black bold">Wallpaper Width</a></p><p class="control is-expanded"><input type="number" name="wall_width" id="wall_width" class="input" placeholder="Input number of width.."${ssrRenderAttr("value", _ctx.vWall_width)}></p></div></div></div></div><div class="field m-t-10"><div class="field-label"></div><div class="field-body"><div class="field is-expanded"><div class="field has-addons"><p class="control"><a class="button is-static text-is-black bold">Wallpaper Size</a></p><p class="control is-expanded"><input type="text" name="wall_size" id="wall_size" class="input" placeholder="Read the description below.."${ssrRenderAttr("value", _ctx.vWall_size)}></p></div><p class="help">Choose: <span class="italic underline">auto</span> or <span class="italic underline">contain</span> or <span class="italic underline">number of length, ex: 70 (it will convert to px)</span></p></div></div></div><div class="control"><span class="bold">Wallpaper Position :</span><div class="select is-fullwidth"><select name="wall_position" id="wall_position" class="text-is-black"><option value="" disabled selected>-- Wallpaper Position --</option><!--[-->`);
  ssrRenderList(_ctx.wapo, (wapo) => {
    _push(`<option${ssrRenderAttr("value", wapo.value)}${ssrIncludeBooleanAttr(wapo.value == _ctx.selwapo[0]) ? " selected" : ""}>${ssrInterpolate(wapo.text)}</option>`);
  });
  _push(`<!--]--></select></div></div><div class="control m-t-10"><span class="bold">Wallpaper Repeat Type :</span><div class="select is-fullwidth"><select name="wall_repeat" id="wall_repeat" class="text-is-black"><option value="" disabled selected>-- Wallpaper Repeat Type --</option><!--[-->`);
  ssrRenderList(_ctx.ware, (ware) => {
    _push(`<option${ssrRenderAttr("value", ware.value)}${ssrIncludeBooleanAttr(ware.value === _ctx.selware[0]) ? " selected" : ""}>${ssrInterpolate(ware.text)}</option>`);
  });
  _push(`<!--]--></select></div></div><div class="control m-t-10"><span class="bold">Wallpaper Attachment :</span><div class="select is-fullwidth"><select name="wall_attachment" id="wall_attachment" class="text-is-black"><option value="" disabled selected>-- Wallpaper Attachment Type --</option><!--[-->`);
  ssrRenderList(_ctx.waat, (waat) => {
    _push(`<option${ssrRenderAttr("value", waat.value)}${ssrIncludeBooleanAttr(waat.value === _ctx.selwaat[0]) ? " selected" : ""}>${ssrInterpolate(waat.text)}</option>`);
  });
  _push(`<!--]--></select></div></div><div class="m-t-30 m-b-50"><div class="buttons is-centered"><button type="button" name="savesetting" id="savesetting" class="button is-primary is-link is-rounded is-selected is-large" onClick="settingup()">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "save-outline" }, null, _parent));
  _push(`</button></div></div></form></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/3_setting/setting.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Setting = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Setting as default
};
