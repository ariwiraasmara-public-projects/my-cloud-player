import { Head } from "@inertiajs/vue3";
import Player from "./body-e8ae8ef9.mjs";
import NavBottom from "./navbottom-a6360c98.mjs";
import CryptoJS from "crypto-js";
import { _ as _sfc_main$1 } from "./myfunction-38bad0fd.mjs";
import MyMusicList from "./musiclist-a3dff9de.mjs";
import { resolveComponent, useSSRContext } from "vue";
import { ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "./dashboardplayer-c5a0f18b.mjs";
import "./navtop-bbe8f378.mjs";
import "./play-ab9a6dd5.mjs";
import "./folder-9308e109.mjs";
import "axios";
import "./playlist-9444f6c0.mjs";
import "./profile-d7a47c90.mjs";
import "./UserGS-f7f3330d.mjs";
import "isomorphic-dompurify";
import "./editprofile-a49010d5.mjs";
import "sweetalert2";
import "./setting-3b5202a5.mjs";
import "swiper/vue";
import "js-cookie";
const _sfc_main = {
  name: "parent-dashboard",
  mixins: [_sfc_main$1],
  components: {
    Head,
    Player,
    NavBottom,
    MyMusicList,
    Crypto: CryptoJS
  },
  props: {
    userid: {
      type: String,
      default: null
    },
    id1001: {
      type: String,
      default: null
    },
    iconplayer: {
      type: Array,
      default: null
    }
  },
  data() {
    return {
      // enuserid: Crypto.AES.encrypt(this.userid.toString(), '123').toString()
      enuserid: this.encrypt(this.userid)
    };
  },
  created() {
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Player = resolveComponent("Player");
  const _component_NavBottom = resolveComponent("NavBottom");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Dashboard" }, null, _parent));
  _push(ssrRenderComponent(_component_Player, { iconplayer: $props.iconplayer }, null, _parent));
  _push(ssrRenderComponent(_component_NavBottom, null, null, _parent));
  _push(`<!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Dashboard.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Dashboard = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Dashboard as default
};
