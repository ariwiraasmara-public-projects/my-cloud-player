import { defineComponent, resolveComponent, mergeProps, withCtx, createVNode, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderAttr, ssrRenderStyle } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = defineComponent({
  name: "play-music",
  props: {
    icon: {
      type: Object,
      default: null
    }
  },
  data() {
    return {
      iconfirst: this.icon["first"],
      iconprevious: this.icon["previous"],
      iconplay: this.icon["play"],
      iconnext: this.icon["next"],
      iconlast: this.icon["last"],
      iconreone: this.icon["reone"],
      iconrelist: this.icon["relist"],
      iconreall: this.icon["reall"],
      iconreoff: this.icon["reoff"]
    };
  },
  created() {
    $("#dashboard_musiclist").addClass("w3-animate-right");
  },
  methods: {}
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  const _component_center = resolveComponent("center");
  _push(`<div${ssrRenderAttrs(mergeProps({
    id: "dashboard_player",
    class: "fadein p-0 m-t-50 m-b-0"
  }, _attrs))}><div class="center bold" id="msgsts"></div><div class="p-20 m-t-10 m-b-10"></div><div class="bottom center m-b-50"><div class="columns is-mobile"><div class="column is-one-fifth nav-text"><span class="button is-responsive" id="btnvol_onoff" value="0" title="Volume Off">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "volume-high-outline" }, null, _parent));
  _push(`</span></div><div class="column is-responsive"><h2 class="nav-text is-responsive" id="player_title" title="No Title">-- No Title --</h2><input type="hidden" id="player_title_in"><audio id="my_mp3"></audio></div><div class="column is-one-quarter"><div class="columns is-mobile"><div class="column"><span class="button is-responsive" id="repeat" title="Repeat is off" value="0"><img${ssrRenderAttr("src", _ctx.iconreoff)} width="16"></span><input type="hidden" id="track_repeat"></div><div class="column"><span class="button is-responsive" id="random" title="Random" value="0">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "swap-horizontal-outline" }, null, _parent));
  _push(`</span></div></div></div></div><div class="columns is-mobile"><div class="column is-one-fifth nav-text" id="durationSeekBar"><span>00:00:00</span></div><div class="column"><progress class="progress is-primary is-hidden" value="" max="100">30%</progress><input type="range" name="audioSeekBar" id="audioSeekBar" value="0" max="100" class="progress is-small is-info"></div><div class="column is-one-fifth nav-text" id="player_duration"><span>00:00:00</span></div></div>`);
  _push(ssrRenderComponent(_component_center, null, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="columns is-mobile" style="${ssrRenderStyle({ "margin-top": "-20px" })}"${_scopeId}><div class="column"${_scopeId}><span id="btn_mfirst" title="First"${_scopeId}><img${ssrRenderAttr("src", _ctx.iconfirst)} width="32"${_scopeId}></span></div><div class="column"${_scopeId}><span id="btn_mprev" title="Previous"${_scopeId}><img${ssrRenderAttr("src", _ctx.iconprevious)} width="32"${_scopeId}></span></div><div class="column"${_scopeId}><span id="btn_mplaypause" title="Play"${_scopeId}><img${ssrRenderAttr("src", _ctx.iconplay)} width="32"${_scopeId}></span></div><div class="column"${_scopeId}><span id="btn_mnext" title="Next"${_scopeId}><img${ssrRenderAttr("src", _ctx.iconnext)} width="32"${_scopeId}></span></div><div class="column"${_scopeId}><span id="btn_mlast" title="Last"${_scopeId}><img${ssrRenderAttr("src", _ctx.iconlast)} width="32"${_scopeId}></span></div></div>`);
      } else {
        return [
          createVNode("div", {
            class: "columns is-mobile",
            style: { "margin-top": "-20px" }
          }, [
            createVNode("div", { class: "column" }, [
              createVNode("span", {
                id: "btn_mfirst",
                title: "First"
              }, [
                createVNode("img", {
                  src: _ctx.iconfirst,
                  width: "32"
                }, null, 8, ["src"])
              ])
            ]),
            createVNode("div", { class: "column" }, [
              createVNode("span", {
                id: "btn_mprev",
                title: "Previous"
              }, [
                createVNode("img", {
                  src: _ctx.iconprevious,
                  width: "32"
                }, null, 8, ["src"])
              ])
            ]),
            createVNode("div", { class: "column" }, [
              createVNode("span", {
                id: "btn_mplaypause",
                title: "Play"
              }, [
                createVNode("img", {
                  src: _ctx.iconplay,
                  width: "32"
                }, null, 8, ["src"])
              ])
            ]),
            createVNode("div", { class: "column" }, [
              createVNode("span", {
                id: "btn_mnext",
                title: "Next"
              }, [
                createVNode("img", {
                  src: _ctx.iconnext,
                  width: "32"
                }, null, 8, ["src"])
              ])
            ]),
            createVNode("div", { class: "column" }, [
              createVNode("span", {
                id: "btn_mlast",
                title: "Last"
              }, [
                createVNode("img", {
                  src: _ctx.iconlast,
                  width: "32"
                }, null, 8, ["src"])
              ])
            ])
          ])
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/1_dashboard/1_play/play.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const ThePlayer = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  ThePlayer as default
};
