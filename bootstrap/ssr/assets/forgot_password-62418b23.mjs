import { Head } from "@inertiajs/vue3";
import { I as Input, B as Button, L as Link } from "./MyLink-3afb4754.mjs";
import { resolveComponent, withCtx, createVNode, useSSRContext } from "vue";
import { ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const forgot_password_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  name: "forgot-password",
  components: {
    Head,
    Input,
    Button,
    Link
  },
  props: {
    link_login: {
      type: String,
      default: null
    }
  },
  methods: {
    send() {
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Input = resolveComponent("Input");
  const _component_ion_icon = resolveComponent("ion-icon");
  const _component_Link = resolveComponent("Link");
  const _component_Button = resolveComponent("Button");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Forgot Password" }, null, _parent));
  _push(`<div class="p-50 fadein"><form action="" method="post" class="boxbf" name="forgot_pass" id="forgot_pass"><p class="text-is-black"><span class="bold italic underline">Forgot your password?</span> send you email here, we will give you a link to reset your password</p>`);
  _push(ssrRenderComponent(_component_Input, {
    type: "text",
    nameinput: "user",
    idinput: "user",
    hasicon: true,
    placeholder: "Username / Email / Phone Number"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_ion_icon, { name: "person-outline" }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_ion_icon, { name: "person-outline" })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<div class="m-t-30"><div class="buttons has-addons is-centered">`);
  _push(ssrRenderComponent(_component_Link, {
    linkto: $props.link_login,
    title: "What About Us",
    class: "button is-danger is-light is-rounded is-medium"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_ion_icon, { name: "arrow-back-outline" }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_ion_icon, { name: "arrow-back-outline" })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(ssrRenderComponent(_component_Button, {
    type: "submit",
    name: "registerok",
    id: "registerok",
    title: "OK",
    onClick: ($event) => $options.send(),
    class: "button is-link is-rounded is-medium"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_ion_icon, { name: "send-outline" }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_ion_icon, { name: "send-outline" })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div></form></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/1/forgot_password.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const forgot_password = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  forgot_password as default
};
