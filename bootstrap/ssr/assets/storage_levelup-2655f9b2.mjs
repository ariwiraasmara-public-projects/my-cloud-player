import { Head } from "@inertiajs/vue3";
import Level from "./table_purchase_storage-42fc0042.mjs";
import { I as Input, B as Button, L as Link } from "./MyLink-3afb4754.mjs";
import { resolveComponent, withCtx, createVNode, useSSRContext } from "vue";
import { ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "axios";
const storage_levelup_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  name: "storage-levelup",
  components: {
    Level,
    Head,
    Input,
    Button,
    Link
  },
  props: {
    link_login: {
      type: String,
      default: null
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Level = resolveComponent("Level");
  const _component_Link = resolveComponent("Link");
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Level Up" }, null, _parent));
  _push(`<div class="p-50 is-mobile fadein"><div class="boxbf p-30 m-b-50"><div class="title center bold text-is-black shadowtext-3-3">Level Up</div><div class="m-t-30 justify content text-is-black">`);
  _push(ssrRenderComponent(_component_Level, null, null, _parent));
  _push(`<div class="center">`);
  _push(ssrRenderComponent(_component_Link, {
    linkto: $props.link_login,
    title: "Login",
    class: "button is-link is-rounded is-medium"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_ion_icon, { name: "home-outline" }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_ion_icon, { name: "home-outline" })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div></div></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/1/storage_levelup.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const storage_levelup = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  storage_levelup as default
};
