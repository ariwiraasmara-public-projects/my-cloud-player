import axios from "axios";
import { mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderList, ssrInterpolate } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const table_purchase_storage_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  name: "table-purchase-storage",
  data() {
    return {
      levelsystem: []
    };
  },
  created() {
    this.readData();
  },
  methods: {
    readData() {
      axios.get("../api/level/all").then((response) => {
        this.levelsystem = response.data.data;
      }).catch((error) => {
        console.log(error);
      });
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<table${ssrRenderAttrs(mergeProps({
    class: "table tablebf is-mobile text-is-black",
    id: "table_purchase_levelup"
  }, _attrs))}><thead><th class="bold center text-is-black">Level</th><th class="bold center text-is-black">Memory</th><th class="bold center text-is-black">Monthly</th><th class="bold center text-is-black">Yearly</th><th class="bold center text-is-black">Lifetime</th></thead><tbody><!--[-->`);
  ssrRenderList($data.levelsystem, (dls, x) => {
    _push(`<tr><td class="center">${ssrInterpolate(dls["level"])}</td><td class="right">${ssrInterpolate(dls["memory"] + " GB")}</td><td class="right">${ssrInterpolate("$" + dls["monthly_pay_usd"] + " USD")}</td><td class="right">${ssrInterpolate("$" + dls["yearly_pay_usd"] + " USD")}</td><td class="right">${ssrInterpolate("$" + dls["lifetime_pay_usd"] + " USD")}</td></tr>`);
  });
  _push(`<!--]--></tbody></table>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/0/table_purchase_storage.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Level = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Level as default
};
