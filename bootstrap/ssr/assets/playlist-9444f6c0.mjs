import { defineComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderList, ssrInterpolate } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = defineComponent({
  name: "playlist-music",
  props: {
    playlist: {
      type: Object,
      default: null
    }
  },
  data() {
    return {};
  },
  created() {
  },
  methods: {}
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<div${ssrRenderAttrs(mergeProps({
    id: "dashboard_playlist",
    class: "is-hidden fadein has-text-black p-0 m-t-0"
  }, _attrs))}><div class=""><!--[-->`);
  ssrRenderList(_ctx.playlist.data, (pl, xplay) => {
    _push(`<div class="item-list">${ssrInterpolate(pl)}</div>`);
  });
  _push(`<!--]--></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/1_dashboard/3_playlist/playlist.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const ThePlaylist = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  ThePlaylist as default
};
