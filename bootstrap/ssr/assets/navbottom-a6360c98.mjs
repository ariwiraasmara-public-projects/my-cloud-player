import { defineComponent, resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = defineComponent({
  name: "nav-bottom",
  methods: {
    nb_dashboard() {
      $("#profile").addClass("is-hidden");
      $("#editprofile").addClass("is-hidden");
      $("#dashboard").removeClass("is-hidden");
      $("#dashboard").addClass("w3-animate-bottom");
      $("#setting").addClass("is-hidden");
    },
    nb_profile() {
      $("#profile").removeClass("is-hidden");
      $("#profile").addClass("w3-animate-bottom");
      $("#editprofile").addClass("is-hidden");
      $("#dashboard").addClass("is-hidden");
      $("#setting").addClass("is-hidden");
    },
    nb_setting() {
      $("#profile").addClass("is-hidden");
      $("#editprofile").addClass("is-hidden");
      $("#dashboard").addClass("is-hidden");
      $("#setting").removeClass("is-hidden");
      $("#setting").addClass("w3-animate-bottom");
    }
  }
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<div${ssrRenderAttrs(mergeProps({
    class: "page-bottom",
    id: "nav-bottom"
  }, _attrs))}><div class="columns is-mobile nav-theme"><div class="column has-text-centered is-size-4 nav-text" id="nb-profile">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "person-outline" }, null, _parent));
  _push(`</div><div class="column has-text-centered is-size-4 nav-text" id="nb-dashboard">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "home-outline" }, null, _parent));
  _push(`</div><div class="column has-text-centered is-size-4 nav-text" id="nb-setting">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "settings-outline" }, null, _parent));
  _push(`</div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/navbottom.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const NavBottom = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  NavBottom as default
};
