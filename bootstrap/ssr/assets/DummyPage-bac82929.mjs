import { defineComponent, useSSRContext } from "vue";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const DummyPage_vue_vue_type_style_index_0_lang = "";
const _sfc_main = defineComponent({
  name: "dummy-page"
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/DummyPage.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const DummyPage = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  DummyPage as default
};
