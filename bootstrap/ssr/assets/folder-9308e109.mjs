import axios from "axios";
import { _ as _sfc_main$1 } from "./myfunction-38bad0fd.mjs";
import { defineComponent, resolveComponent, useSSRContext } from "vue";
import { ssrRenderComponent, ssrRenderAttr, ssrRenderList, ssrInterpolate } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "crypto-js";
import "js-cookie";
const _sfc_main = defineComponent({
  name: "folder-music",
  components: {
    myfunction: _sfc_main$1
  },
  props: {
    idsf: {
      type: String,
      default: null
    },
    item: {
      type: String,
      default: null
    },
    data: {
      type: Object,
      default: null
    },
    folder: {
      type: String,
      default: null
    },
    list: {
      type: String,
      default: null
    }
  },
  data() {
    return {
      viewfoldersdata: []
    };
  },
  created() {
    this.readDataFolders();
  },
  methods: {
    readDataFolders() {
      axios.get("../api/user/folder/get/" + this.userid, {
        "headers": {
          "accept": "application/json",
          "mcr-x-aswq": "mC12x@12!5Of!a"
        }
      }).then((response) => {
        this.viewfoldersdata = response.data.data;
        this.viewfoldersdata = this.viewfoldersdata[0];
      }).catch((error) => {
        console.log(error);
      });
    },
    createFolder() {
    }
  }
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<!--[--><div id="dashboard_folder" class="is-hidden fadein p-0 m-t-0"><div class="field has-addons"><p class="control is-expanded"><input class="input is-rounded" type="text" placeholder="Find your music here.."></p><p class="control"><span id="add-folder" class="button is-success">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "add-circle-outline" }, null, _parent));
  _push(`</span></p><p class="control"><span id="upload-file" class="button is-link">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "cloud-upload-outline" }, null, _parent));
  _push(`</span></p></div><div class="m-l-r-10 m-t-10 text-is-black" id="nav-breadcumb"><nav class="breadcrumb" aria-label="breadcrumbs"><ul id="nav-folder"><li id="nav-item-back" class="is-hidden" onClick="item_back(&#39;{{ $idsf }}&#39;,&#39;{{ $item }}&#39;)">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "arrow-back-outline" }, null, _parent));
  _push(`</li><li class="m-l-10"><a href="#">root</a></li><li><a href="#" id="nav-folder-child"></a></li></ul></nav></div><div class="m-t-10 m-b-50 text-is-black"${ssrRenderAttr("id", [_ctx.idsf])}><!--[-->`);
  ssrRenderList(_ctx.data, (dt, xfolder) => {
    _push(`<div${ssrRenderAttr("id", xfolder + "." + dt["foldername"])} class="item-list"><div class="columns is-mobile is-responsive"><div class="column" onClick="item(&#39;{{ folder+fun.toMD5(dt[&#39;id_1004&#39;]) }}&#39;,
                                                      &#39;{{ item }}&#39;,
                                                      &#39;{{ dt[&#39;id_1004&#39;] }}&#39;,
                                                      &#39;{{ dt[&#39;foldername&#39;] }}&#39;,
                                                      &#39;item-content-folder&#39;)"><span id="{{ list+fun.toMD5(dt[&#39;id_1004&#39;]) }}" class="">${ssrInterpolate(dt["foldername"])}</span></div><div class="column right" onclick="folderpopup(&#39;{{ dt[&#39;id_1004&#39;] }}&#39;, &#39;{{ dt[&#39;foldername&#39;] }}&#39;)"><span class="">`);
    _push(ssrRenderComponent(_component_ion_icon, { name: "ellipsis-vertical-outline" }, null, _parent));
    _push(`</span></div></div></div>`);
  });
  _push(`<!--]--></div><div class="m-t-10 m-b-50 text-is-black is-hidden"${ssrRenderAttr("id", [_ctx.item])}><div id="item-content-folder"></div></div></div><div class="modal is-rounded" id="modal-folder"><div class="modal-background"></div><div class="modal-card"><header class="modal-card-head"><p class="modal-card-title text-is-black" id="modal-title-folder">Modal title</p><button class="delete" aria-label="close"></button></header><section class="modal-card-body text-is-black"><p class="m-b-0" id="modal-text-folder">Modal text</p></section><footer class="modal-card-foot center"><span class="button" id="btn_modal_edit_data">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "create-outline" }, null, _parent));
  _push(`</span><span class="button" id="btn_modal_delete_data">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "trash-outline" }, null, _parent));
  _push(`</span></footer></div></div><div class="modal is-rounded" id="modal-file"><div class="modal-background"></div><div class="modal-card"><header class="modal-card-head"><p class="modal-card-title text-is-black" id="modal-title-file">Modal title</p><button class="delete" aria-label="close"></button></header><section class="modal-card-body text-is-black" id="modal-text-file"><div class="columns is-mobile is-responsive"><div class="column"><div class="field is-half"><div class="control"><input class="input" type="text" id="genre" placeholder="Genre.." readonly></div></div></div><div class="column is-half"><div class="field"><div class="control"><input class="input" type="text" id="artist" placeholder="Artist.." readonly></div></div></div></div><div class="columns is-mobile is-responsive"><div class="column is-half"><div class="field"><div class="control"><input class="input" type="text" id="album" placeholder="Album.." readonly></div></div></div><div class="column is-half"><div class="field"><div class="control"><input class="input" type="text" id="composer" placeholder="Composer.." readonly></div></div></div></div><div class="columns is-mobile is-responsive"><div class="column is-half"><div class="field"><div class="control"><input class="input" type="text" id="publisher" placeholder="Publisher.." readonly></div></div></div><div class="column is-half"><div class="field"><div class="control"><input class="input" type="text" id="description" placeholder="Description.." readonly></div></div></div></div></section><footer class="modal-card-foot center"><span class="button" id="btn_modal_edit_data">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "create-outline" }, null, _parent));
  _push(`</span><span class="button" id="btn_modal_delete_data">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "trash-outline" }, null, _parent));
  _push(`</span><div class="field has-addons"><p class="m-l-30 control is-expanded"><span class="select"><select class="is-expanded" id="file-to-move-to-folder"><option value="" disabled selected>-- Choose folder to move --</option><!--[-->`);
  ssrRenderList(_ctx.data, (dt, xselectfolder) => {
    _push(`<option value="{{ $dt[&#39;id_1004&#39;] }}">${ssrInterpolate(_ctx.$dt["foldername"])}</option>`);
  });
  _push(`<!--]--><option value="move_cancel">-- Cancel --</option></select></span></p><p class="control"><button type="button" class="button" id="btn_modal_oke_to_move_to" onClick="movefile(&#39;file-to-move-to-folder&#39;, &#39;{{ dt[&#39;id_1004&#39;] }}&#39;)">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "enter-outline" }, null, _parent));
  _push(`</button></p></div></footer></div></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/1_dashboard/2_folder/folder.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const TheFolder = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  TheFolder as default
};
