import { defineComponent, useSSRContext } from "vue";
import CryptoJS from "crypto-js";
import Cookies from "js-cookie";
const _sfc_main = defineComponent({
  name: "my-function",
  components: {
    CryptoJS,
    Cookies
  },
  methods: {
    key() {
      const fun = new myfunction();
      return fun.getKey();
    },
    toCrypt(str) {
      return "<?php echo crypt(" + str + ", rand()); ?>";
    },
    cjsMD5(val) {
      return CryptoJS.MD5(val.toString(), this.key()).toString(CryptoJS.enc.Base64);
    },
    phpMD5(str) {
      return "<?php echo md5(" + str + ", false); ?>";
    },
    cjsSHA(val) {
      return CryptoJS.SHA512(val.toString(), this.key()).toString(CryptoJS.enc.Base64);
    },
    phpSHA(str) {
      return "<?php echo hash('sha512', " + str + "); ?>";
    },
    phpHaval(str) {
      return "<?php echo hash('haval256,5', " + str + "); ?>";
    },
    encrypt(val) {
      return CryptoJS.AES.encrypt(val.toString(), this.key()).toString();
    },
    decrypt(val) {
      let decrypted = CryptoJS.AES.decrypt(val.toString(), this.key()).toString();
      return CryptoJS.enc.Utf8.stringify(CryptoJS.enc.Hex.parse(decrypted.toString()));
    },
    enval(str) {
      return "<?php echo bin2hex(base64_encode(" + str + ")); ?>";
    },
    denval(str) {
      return "<?php echo base64_decode(hex2bin(" + str + ")); ?>";
    },
    enlink(str) {
      this.toSHA512(this.toMD5(str));
    },
    setrawCookie(name, value, hari = 1, issecure = false, strictness = "strict") {
      const d = new Date();
      d.setTime(d.getTime() + hari * 24 * 60 * 60 * 1e3);
      let expires = "expires=" + d.toUTCString();
      document.cookie = name + "=" + value + ";" + expires;
    },
    setCookie(name, value, isencrypt = false, hari = 1, issecure = false, strictness = "strict") {
      if (isencrypt) {
        Cookies.set(name, this.encrypt(value), { expires: hari, path: "", secure: issecure, sameSite: strictness });
      }
      Cookies.set(name, value, { expires: hari, path: "", secure: issecure, sameSite: strictness });
    },
    getrawCookie(cname) {
      let name = cname + "=";
      let decodedCookie = decodeURIComponent(document.cookie);
      let ca = decodedCookie.split(";");
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == " ") {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    },
    getCookie(name, isdecrypt = false) {
      if (isdecrypt) {
        return this.decrypt(Cookies.get(name));
      }
      return Cookies.get(name);
    },
    checkCookie(name) {
      let cookiename = getCookie(name);
      if (cookiename != "") {
        return true;
      }
      return false;
    },
    deleteCookie(name) {
      Cookies.remove(name, { path: "" });
    },
    redirect(str) {
      window.location.href = str;
    },
    escapeHtml(text) {
      var map = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#039;"
      };
      return text.replace(/[&<>"']/g, function(m) {
        return map[m];
      });
    },
    readableHTML(text) {
      var entities = [
        ["amp", "&"],
        ["apos", "'"],
        ["#x27", "'"],
        ["#x2F", "/"],
        ["#39", "'"],
        ["#47", "/"],
        ["lt", "<"],
        ["gt", ">"],
        ["nbsp", " "],
        ["quot", '"']
      ];
      for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp("&" + entities[i][0] + ";", "g"), entities[i][1]);
      return text;
    },
    formatNumber(thenumber, koma = 0) {
      var withkoma = thenumber.toFixed(koma);
      var parts = withkoma.toString().split(".");
      const numberPart = parts[0];
      const decimalPart = parts[1];
      const thousands = /\B(?=(\d{3})+(?!\d))/g;
      return numberPart.replace(thousands, ".") + (decimalPart ? "," + decimalPart : "");
    },
    rupiah(thenumber) {
      return thenumber.toLocaleString("id-ID", { style: "currency", currency: "IDR" });
    },
    random(str, length) {
    },
    back(x, y) {
      $(document).ready(function() {
        $("#" + x).addClass("is-hidden");
        $("#" + y).removeClass("is-hidden");
        $("#nav-folder-child").remove();
        $("#item-content").html();
      });
    },
    setText(id, value) {
      $("#" + id).val(value);
    },
    isFileEdit(type) {
      if (type > 0) {
        $("#genre").prop("readonly", false);
        $("#artist").prop("readonly", false);
        $("#album").prop("readonly", false);
        $("#composer").prop("readonly", false);
        $("#publisher").prop("readonly", false);
        $("#description").prop("readonly", false);
      }
      $("#genre").prop("readonly", true);
      $("#artist").prop("readonly", true);
      $("#album").prop("readonly", true);
      $("#composer").prop("readonly", true);
      $("#publisher").prop("readonly", true);
      $("#description").prop("readonly", true);
    }
  }
});
class myfunction {
  constructor() {
    this.setKey();
  }
  setKey() {
    this.keySafe = "<--{[@12iW3721195S0f!@]}-->";
  }
  getKey() {
    return this.keySafe;
  }
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Functions/myfunction.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as _
};
