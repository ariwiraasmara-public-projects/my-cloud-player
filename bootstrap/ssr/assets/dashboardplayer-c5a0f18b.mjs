import NavTop from "./navtop-bbe8f378.mjs";
import ThePlayer from "./play-ab9a6dd5.mjs";
import TheFolder from "./folder-9308e109.mjs";
import ThePlaylist from "./playlist-9444f6c0.mjs";
import MyMusicList from "./musiclist-a3dff9de.mjs";
import { defineComponent, resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "axios";
import "./myfunction-38bad0fd.mjs";
import "crypto-js";
import "js-cookie";
const _sfc_main = defineComponent({
  name: "dashboard-player",
  components: {
    MyMusicList,
    NavTop,
    ThePlayer,
    TheFolder,
    ThePlaylist
  },
  props: {
    iconplayer: {
      type: Object,
      default: null
    }
  }
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_NavTop = resolveComponent("NavTop");
  const _component_MyMusicList = resolveComponent("MyMusicList");
  const _component_ThePlayer = resolveComponent("ThePlayer");
  const _component_TheFolder = resolveComponent("TheFolder");
  const _component_ThePlaylist = resolveComponent("ThePlaylist");
  _push(`<div${ssrRenderAttrs(mergeProps({ id: "dashboard" }, _attrs))}>`);
  _push(ssrRenderComponent(_component_NavTop, null, null, _parent));
  _push(`<div id="content">`);
  _push(ssrRenderComponent(_component_MyMusicList, { musiclist: { data: [
    { name: "Music List 1", description: "Deskripsi Music List 1" },
    { name: "Music List 2", description: "Deskripsi Music List 2" },
    { name: "Music List 3", description: "Deskripsi Music List 3" },
    { name: "Music List 4", description: "Deskripsi Music List 4" },
    { name: "Music List 5", description: "Deskripsi Music List 5" }
  ] } }, null, _parent));
  _push(ssrRenderComponent(_component_ThePlayer, { icon: _ctx.iconplayer }, null, _parent));
  _push(ssrRenderComponent(_component_TheFolder, null, null, _parent));
  _push(ssrRenderComponent(_component_ThePlaylist, { playlist: { data: ["PlayList 1", "PlayList 2", "PlayList 3", "PlayList 4", "PlayList 5"] } }, null, _parent));
  _push(`</div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/1_dashboard/dashboardplayer.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Dashboard = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Dashboard as default
};
