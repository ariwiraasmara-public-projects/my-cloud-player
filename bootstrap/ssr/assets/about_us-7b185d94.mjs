import { Head } from "@inertiajs/vue3";
import { I as Input, B as Button, L as Link } from "./MyLink-3afb4754.mjs";
import { resolveComponent, withCtx, createVNode, useSSRContext } from "vue";
import { ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const about_us_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  name: "about-us",
  components: {
    Head,
    Input,
    Button,
    Link
  },
  props: {
    link_login: {
      type: String,
      default: null
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Link = resolveComponent("Link");
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "About Us" }, null, _parent));
  _push(`<div class="p-50 fadein"><div class="boxbf p-30 m-b-50"><div class="title center bold text-is-black shadowtext-3-3">About Us</div><div class="m-t-30 justify content text-is-black"><p class="center"> Cloud Storage Drive <span class="italic">(music files only)</span> <br> + <br> Music Player </p><p><span class="bold italic underline">My Cloud Player</span> is a music platform that can store your <span class="italic">(only)</span> music files, after you store it, you can play it on the built-in player. So that you can hear the music wherever and whenever you are! <br><br> Doesn&#39;t matter you&#39;re logged in on your PC Device (any os) or Smarphone Device (any os) or any other OS, just enter and use our platform. We hope it suits you! <br><br> Well, technically it&#39;s created based on our creator experiences. 🤭 <br><br> Wether it&#39;s just for your music collection or store your favorites and/or likes musics. <br><br> Interested in our product? Feel free to register <a href="signin" class="is-link bold underline">here</a>, it won&#39;t charge you for the first time use! Well, anyway you got free 4GB storage for the first time. If you Kepo, you can check our upgrade storage level, <a href="upgradestorage" class="is-link bold underline">here</a></p></div><div class="center">`);
  _push(ssrRenderComponent(_component_Link, {
    linkto: $props.link_login,
    title: "Login",
    class: "button is-link is-rounded is-medium"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_ion_icon, { name: "home-outline" }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_ion_icon, { name: "home-outline" })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/1/about_us.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const about_us = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  about_us as default
};
