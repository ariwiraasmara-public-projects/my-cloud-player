import Dashboard from "./dashboardplayer-c5a0f18b.mjs";
import Profile from "./profile-d7a47c90.mjs";
import EditProfile from "./editprofile-a49010d5.mjs";
import Setting from "./setting-3b5202a5.mjs";
import CryptoJS from "crypto-js";
import { Swiper, SwiperSlide } from "swiper/vue";
import { resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "./navtop-bbe8f378.mjs";
import "./play-ab9a6dd5.mjs";
import "./folder-9308e109.mjs";
import "axios";
import "./myfunction-38bad0fd.mjs";
import "js-cookie";
import "./playlist-9444f6c0.mjs";
import "./musiclist-a3dff9de.mjs";
import "./UserGS-f7f3330d.mjs";
import "isomorphic-dompurify";
import "sweetalert2";
const _sfc_main = {
  name: "parent-body",
  components: {
    Dashboard,
    Profile,
    Setting,
    EditProfile,
    CryptoJS,
    Swiper,
    SwiperSlide
  },
  props: {
    userid: {
      type: String,
      default: null
    },
    id1001: {
      type: String,
      default: null
    },
    dataprofile: {
      type: Object,
      default: null
    },
    iconplayer: {
      type: Object,
      default: null
    }
  },
  created() {
  }
  /*
  setup() {
      const onSwiper = (swiper) => {
          console.log(swiper);
      };
      const onSlideChange = () => {
          console.log('slide change');
      };
      return {
          onSwiper, onSlideChange,
      };
  },
  */
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_EditProfile = resolveComponent("EditProfile");
  const _component_Dashboard = resolveComponent("Dashboard");
  const _component_Profile = resolveComponent("Profile");
  const _component_Setting = resolveComponent("Setting");
  _push(`<div${ssrRenderAttrs(mergeProps({ id: "bodyplay" }, _attrs))}>`);
  _push(ssrRenderComponent(_component_EditProfile, null, null, _parent));
  _push(ssrRenderComponent(_component_Dashboard, { iconplayer: $props.iconplayer }, null, _parent));
  _push(ssrRenderComponent(_component_Profile, { userid: $props.userid }, null, _parent));
  _push(ssrRenderComponent(_component_Setting, { userid: $props.id1001 }, null, _parent));
  _push(`</div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/body.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Player = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Player as default
};
