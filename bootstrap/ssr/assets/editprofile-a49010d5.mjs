import axios from "axios";
import { _ as _sfc_main$1 } from "./myfunction-38bad0fd.mjs";
import { _ as _sfc_main$2 } from "./UserGS-f7f3330d.mjs";
import Swal from "sweetalert2";
import { defineComponent, resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderAttr } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
import "crypto-js";
import "js-cookie";
const _sfc_main = defineComponent({
  name: "user-editprofile",
  mixins: [_sfc_main$1, _sfc_main$2],
  data() {
    return {
      vieweditprofiledata: []
    };
  },
  created() {
    this.readDataEditProfile();
  },
  methods: {
    readDataEditProfile() {
      axios.get("../api/user/edit/get/" + this.getCookie("mcr-x-aswq-1"), {
        "headers": {
          "accept": "application/json",
          "mcr-x-aswq": "mC12x@12!5Of!a"
        }
      }).then((response) => {
        this.vieweditprofiledata = response.data.data;
        this.construct_editprofile();
      }).catch((error) => {
        console.log(error);
      });
    },
    profileupdate(id, val1, val2) {
      axios.post("../api/user/edit/save/" + this.getCookie("mcr-x-aswq-1"), {
        field: val1,
        value: val2
      }).then((res) => {
        Swal.fire({
          title: "Success Update Profile!",
          icon: "success",
          confirmButtonText: "OK"
        }).then((result) => {
          this.cancelformcard(id);
        });
      });
    },
    toProfile() {
      $("#nav-bottom").removeClass("is-hidden");
      $("#profile").removeClass("is-hidden");
      $("#profile").removeClass("w3-animate-bottom");
      $("#profile").addClass("w3-animate-left");
      $("#dashboard").addClass("is-hidden");
      $("#setting").addClass("is-hidden");
      $("#editprofile").addClass("is-hidden");
    },
    detailformcard(id) {
      if ($("#iconup-" + id).hasClass("is-hidden")) {
        $("#iconup-" + id).removeClass("is-hidden");
        $("#formcard-" + id).removeClass("is-hidden");
        $("#icondown-" + id).addClass("is-hidden");
      } else
        cancelformcard(id);
    },
    editinformation(id) {
      if (id == "pass" || id == "pin")
        console.log("edit information", id);
      $("#formcardfill-" + id).prop("required", true);
      $("#formcardfill-" + id).prop("disabled", false);
      $("#formcardedit-" + id).prop("disabled", true);
      $("#formcardsave-" + id).prop("disabled", false);
    },
    cancelformcard(id) {
      if ($("#icondown-" + id).hasClass("is-hidden")) {
        $("#iconup-" + id).addClass("is-hidden");
        $("#formcard-" + id).addClass("is-hidden");
        $("#icondown-" + id).removeClass("is-hidden");
        $("#formcardfill-" + id).prop("required", false);
        $("#formcardfill-" + id).prop("disabled", true);
        $("#formcardedit-" + id).prop("disabled", false);
        $("#formcardsave-" + id).prop("disabled", true);
      }
    },
    see(field) {
      let seepp = null;
      if (field == "pass")
        seepp = "[variabel pass]";
      else if (field == "pin")
        seepp = "[variabel_pin]";
      if ($("#formcardsee-" + field).val() == 0) {
        $("#formcardsee-" + field).val(1);
        $("#formcardfill-" + field).val(seepp);
        $("#formcardsee-" + field).html('<ion-icon name="eye-off-outline"></ion-icon>');
      } else if ($("#formcardsee-" + field).val() == 1) {
        $("#formcardsee-" + field).val(0);
        $("#formcardfill-" + field).val("***");
        $("#formcardsee-" + field).html('<ion-icon name="eye-outline"></ion-icon>');
      }
    }
  }
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<div${ssrRenderAttrs(mergeProps({
    class: "is-hidden",
    id: "editprofile"
  }, _attrs))}><section class="hero nav-theme p-t-10 p-b-10 p-l-20"><p class="title bold nav-text"><span id="toprofile">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "arrow-back-outline" }, null, _parent));
  _push(`</span> Edit Profile </p></section><div class="p-30 text-is-black"><div class="card m-b-10"><header class="card-header" id="formcardbtn-nama"><p class="card-header-title"><span class="icon is-small is-left text-is-black bold m-r-10">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "person-outline" }, null, _parent));
  _push(`</span> Name </p><a href="#" class="card-header-icon" aria-label="more options"><span class="icon" id="icondown-nama">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-down-outline" }, null, _parent));
  _push(`</span><span class="icon is-hidden" id="iconup-nama">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-up-outline" }, null, _parent));
  _push(`</span></a></header><div class="is-hidden dropdown-content" id="formcard-nama"><div class="card-content"><div class="content"><div class="field has-addons borad-10"><div class="control is-expanded"><input type="text" class="input" name="val" id="formcardfill-nama" placeholder="Put Your Name Here.." disabled${ssrRenderAttr("value", _ctx.vieweditprofiledata.nama)}></div><div class="control"><button type="button" name="formcardedit-nama" id="formcardedit-nama" href="#edit-nama" class="button is-info">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "pencil-outline" }, null, _parent));
  _push(`</button></div><div class="control"><button type="button" name="formcardsave" id="formcardsave-nama" href="#save-nama" value="$fun-&gt;encrypt(&#39;nama&#39;)" class="button is-success" disabled>`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "save-outline" }, null, _parent));
  _push(`</button></div></div></div></div><footer class="card-footer"><a id="formcardfooterbtn-nama" href="#cancel-nama" class="card-footer-item">Cancel</a></footer></div></div><div class="card m-b-10"><header class="card-header" id="formcardbtn-pass"><p class="card-header-title"><span class="icon is-small is-left text-is-black bold m-r-10">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "lock-closed-outline" }, null, _parent));
  _push(`</span> Password </p><a href="#" class="card-header-icon" aria-label="more options"><span class="icon" id="icondown-pass">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-down-outline" }, null, _parent));
  _push(`</span><span class="icon is-hidden" id="iconup-pass">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-up-outline" }, null, _parent));
  _push(`</span></a></header><div class="is-hidden dropdown-content" id="formcard-pass"><div class="card-content"><div class="content"><div class="field has-addons borad-10"><div class="control is-expanded"><input type="text" class="input" name="pass" id="formcardfill-pass" placeholder="Put Your Password Here.." disabled${ssrRenderAttr("value", _ctx.vieweditprofiledata.password)}></div><div class="control"><button type="button" name="formcardedit-pass" id="formcardedit-pass" href="#edit-pass" class="button is-info">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "pencil-outline" }, null, _parent));
  _push(`</button></div><div class="control"><button type="submit" name="formcardsave" id="formcardsave-pass" href="#save-pass" value="$fun-&gt;encrypt(&#39;pass&#39;)" class="button is-success" disabled>`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "save-outline" }, null, _parent));
  _push(`</button></div><div class="control"><button type="button" name="formcardsee-pin" id="formcardsee-pass" href="#see-pass" class="button is-danger" value="0">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "eye-outline" }, null, _parent));
  _push(`</button></div></div></div></div><footer class="card-footer"><a id="formcardfooterbtn-pass" href="#cancel-pass" class="card-footer-item">Cancel</a></footer></div></div><div class="card m-b-10"><header class="card-header" id="formcardbtn-pin"><p class="card-header-title"><span class="icon is-small is-left text-is-black bold m-r-10">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "lock-closed-outline" }, null, _parent));
  _push(`</span> PIN </p><a href="#" class="card-header-icon" aria-label="more options"><span class="icon" id="icondown-pin">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-down-outline" }, null, _parent));
  _push(`</span><span class="icon is-hidden" id="iconup-pin">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-up-outline" }, null, _parent));
  _push(`</span></a></header><div class="is-hidden dropdown-content" id="formcard-pin"><div class="card-content" id="dropcard"><div class="content"><div class="field has-addons borad-10"><div class="control is-expanded"><input type="text" class="input" name="pin" id="formcardfill-pin" placeholder="Put Your PIN Here.." disabled${ssrRenderAttr("value", _ctx.vieweditprofiledata.pin)}></div><div class="control"><button type="button" name="formcardedit-pin" id="formcardedit-pin" href="#edit-pin" class="button is-info">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "pencil-outline" }, null, _parent));
  _push(`</button></div><div class="control"><button type="submit" name="formcardsave" id="formcardsave-pin" href="#save-pin" value="$fun-&gt;encrypt(&#39;pin&#39;)" class="button is-success" disabled>`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "save-outline" }, null, _parent));
  _push(`</button></div><div class="control"><button type="button" name="formcardsee-pin" id="formcardsee-pin" href="#see-pin" class="button is-danger">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "eye-outline" }, null, _parent));
  _push(`</button></div></div></div></div><footer class="card-footer"><a id="formcardfooterbtn-pin" href="#cancel-pin" class="card-footer-item">Cancel</a></footer></div></div><div class="card m-b-10"><header class="card-header" id="formcardbtn-tlp"><p class="card-header-title"><span class="icon is-small is-left text-is-black bold m-r-10">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "phone-portrait-outline" }, null, _parent));
  _push(`</span> Phone Number </p><a href="#" class="card-header-icon" aria-label="more options"><span class="icon" id="icondown-tlp">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-down-outline" }, null, _parent));
  _push(`</span><span class="icon is-hidden" id="iconup-tlp">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-up-outline" }, null, _parent));
  _push(`</span></a></header><div class="is-hidden dropdown-content" id="formcard-tlp"><div class="card-content"><div class="content"><div class="field has-addons borad-10"><div class="control is-expanded"><input type="text" class="input" name="tlp" id="formcardfill-tlp" placeholder="Put Your Phone Number Here.." disabled${ssrRenderAttr("value", _ctx.vieweditprofiledata.tlp)}></div><div class="control"><button type="button" name="formcardedit-tlp" id="formcardedit-tlp" href="#edit-tlp" class="button is-info">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "pencil-outline" }, null, _parent));
  _push(`</button></div><div class="control"><button type="submit" name="formcardsave" id="formcardsave-tlp" href="#save-tlp" value="$fun-&gt;encrypt(&#39;tlp&#39;)" class="button is-success" disabled>`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "save-outline" }, null, _parent));
  _push(`</button></div></div></div></div><footer class="card-footer"><a id="formcardfooterbtn-tlp" href="#cancel-tlp" class="card-footer-item">Cancel</a></footer></div></div><div class="card m-b-10"><header class="card-header" id="formcardbtn-tgl_lahir"><p class="card-header-title"><span class="icon is-small is-left text-is-black bold m-r-10">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "calendar-outline" }, null, _parent));
  _push(`</span> Birthdate </p><a href="#" class="card-header-icon" aria-label="more options"><span class="icon" id="icondown-tgl_lahir">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-down-outline" }, null, _parent));
  _push(`</span><span class="icon is-hidden" id="iconup-tgl_lahir">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "chevron-up-outline" }, null, _parent));
  _push(`</span></a></header><div class="is-hidden dropdown-content" id="formcard-tgl_lahir"><div class="card-content"><div class="content"><div class="field has-addons borad-10"><div class="control is-expanded"><input type="date" class="input" name="tgl_lahir" id="formcardfill-tgl_lahir" placeholder="Your Birthdate.." disabled${ssrRenderAttr("value", _ctx.vieweditprofiledata.tgl_lahir)}></div><div class="control"><button type="button" name="formcardedit-tgl" id="formcardedit-tgl_lahir" href="#edit-tgl_lahir" class="button is-info">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "pencil-outline" }, null, _parent));
  _push(`</button></div><div class="control"><button type="submit" name="formcardsave" id="formcardsave-tgl_lahir" href="#save-tgl_lahir" value="fun-&gt;encrypt(&#39;tgl_lahir&#39;)" class="button is-success" disabled>`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "save-outline" }, null, _parent));
  _push(`</button></div></div></div></div><footer class="card-footer"><a id="formcardfooterbtn-tgl_lahir" href="#cancel-tgl_lahir" class="card-footer-item">Cancel</a></footer></div></div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/2_profile/editprofile.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const EditProfile = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  EditProfile as default
};
