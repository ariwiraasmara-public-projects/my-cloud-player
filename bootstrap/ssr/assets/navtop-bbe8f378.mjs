import { defineComponent, resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = defineComponent({
  name: "nav-top",
  methods: {
    dashboard_musiclist() {
      $("#dashboard_musiclist").removeClass("is-hidden");
      $("#tab_musiclist").addClass("is-active");
      $("#dashboard_player").addClass("is-hidden");
      $("#tab_player").removeClass("is-active");
      $("#dashboard_folder").addClass("is-hidden");
      $("#tab_folder").removeClass("is-active");
      $("#dashboard_playlist").addClass("is-hidden");
      $("#tab_playlist").removeClass("is-active");
      $("#dashboard_musiclist").removeClass("is-hidden");
      $("#dashboard_musiclist").removeClass("is-hidden");
      $("#dashboard_player").removeClass("w3-animate-left");
      $("#dashboard_player").addClass("w3-animate-right");
      $("#dashboard_folder").removeClass("w3-animate-left");
      $("#dashboard_folder").addClass("w3-animate-right");
    },
    dashboard_player() {
      $("#dashboard_musiclist").addClass("is-hidden");
      $("#tab_musiclist").removeClass("is-active");
      $("#dashboard_player").removeClass("is-hidden");
      $("#tab_player").addClass("is-active");
      $("#dashboard_folder").addClass("is-hidden");
      $("#tab_folder").removeClass("is-active");
      $("#dashboard_playlist").addClass("is-hidden");
      $("#tab_playlist").removeClass("is-active");
      $("#dashboard_player").removeClass("w3-animate-left");
      $("#dashboard_player").removeClass("w3-animate-right");
      $("#dashboard_folder").removeClass("w3-animate-left");
      $("#dashboard_folder").addClass("w3-animate-right");
    },
    dashboard_folder() {
      $("#dashboard_musiclist").addClass("is-hidden");
      $("#tab_musiclist").removeClass("is-active");
      $("#dashboard_player").addClass("is-hidden");
      $("#tab_player").removeClass("is-active");
      $("#dashboard_folder").removeClass("is-hidden");
      $("#tab_folder").addClass("is-active");
      $("#dashboard_playlist").addClass("is-hidden");
      $("#tab_playlist").removeClass("is-active");
      $("#dashboard_player").addClass("w3-animate-left");
      $("#dashboard_player").removeClass("w3-animate-right");
      $("#dashboard_folder").removeClass("w3-animate-left");
      $("#dashboard_folder").removeClass("w3-animate-right");
    },
    dashboard_playlist() {
      $("#dashboard_musiclist").addClass("is-hidden");
      $("#tab_musiclist").removeClass("is-active");
      $("#dashboard_player").addClass("is-hidden");
      $("#tab_player").removeClass("is-active");
      $("#dashboard_folder").addClass("is-hidden");
      $("#tab_folder").removeClass("is-active");
      $("#dashboard_playlist").removeClass("is-hidden");
      $("#tab_playlist").addClass("is-active");
      $("#dashboard_player").addClass("w3-animate-left");
      $("#dashboard_player").removeClass("w3-animate-right");
      $("#dashboard_folder").addClass("w3-animate-left");
      $("#dashboard_folder").removeClass("w3-animate-right");
    }
  }
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<div${ssrRenderAttrs(mergeProps({
    class: "nav-theme",
    id: "dashboard-nav-top"
  }, _attrs))}><div class="tabs is-boxed is-large is-toggle-rounded is-centered" id="nav-tabs"><ul><li data-target="musiclist" id="tab_musiclist"><a href="#musiclist" id="tab-a" title="Music List" class="nav-top-text">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "musical-note-outline" }, null, _parent));
  _push(`</a></li><li class="is-active" data-target="play" id="tab_player"><a href="#player" id="tab-a" title="Play" class="nav-top-text">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "play-outline" }, null, _parent));
  _push(`</a></li><li data-target="folder" id="tab_folder"><a href="#folderlist" id="tab-a" title="Folder" class="nav-top-text">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "folder-open-outline" }, null, _parent));
  _push(`</a></li><li data-target="playlist" id="tab_playlist"><a href="#playlist" id="tab-a" title="Playlist" class="nav-top-text">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "list-outline" }, null, _parent));
  _push(`</a></li></ul></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/1_dashboard/navtop.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const NavTop = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  NavTop as default
};
