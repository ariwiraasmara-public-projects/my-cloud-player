import { defineComponent, useSSRContext } from "vue";
const _sfc_main = defineComponent({
  name: "user-gs",
  data() {
    return {
      username: null,
      email: null,
      nama: null,
      tempat_lahir: null,
      tgl_lahir: null,
      tlp: null,
      pass: null,
      pin: null,
      alamat: null,
      path: null,
      level: null,
      photo: null
    };
  },
  methods: {
    construct_profile(username = null, email = null, nama = null, tempat_lahir = null, tgl_lahir = null, alamat = null, tlp = null, level = 0, path = null, photo = null) {
      this.setUsername(username);
      this.setEmail(email);
      this.setNama(nama);
      this.setTempatlahir(tempat_lahir);
      this.setTgllahir(tgl_lahir);
      this.setAlamat(alamat);
      this.setTlp(tlp);
      this.setLevel(level);
      this.setPath(path);
      this.setPhoto(photo);
    },
    construct_editprofile(nama = null, pass = null, pin = null, tlp = null, tgl = null) {
      this.setNama(nama);
      this.setPass(pass);
      this.setPin(pin);
      this.setTlp(tlp);
      this.setTgllahir(tgl);
    },
    getUsername() {
      return this.username;
    },
    setUsername(username = null) {
      this.username = username;
    },
    getEmail() {
      return this.email;
    },
    setEmail(email = null) {
      this.email = email;
    },
    getNama() {
      return this.nama;
    },
    setNama(nama = null) {
      this.nama = nama;
    },
    getTempatlahir() {
      return this.tempat_lahir;
    },
    setTempatlahir(tempat_lahir = null) {
      this.tempat_lahir = tempat_lahir;
    },
    getTgllahir() {
      return this.tgl_lahir;
    },
    setTgllahir(tgl_lahir = null) {
      this.tgl_lahir = tgl_lahir;
    },
    getTlp() {
      return this.tlp;
    },
    setTlp(tlp = null) {
      this.tlp = tlp;
    },
    getPass() {
      return this.pass;
    },
    setPass(pass = null) {
      this.pass = pass;
    },
    getPin() {
      return this.pin;
    },
    setPin(pin = null) {
      this.pin = pin;
    },
    getAlamat() {
      return this.alamat;
    },
    setAlamat(alamat = null) {
      this.alamat = alamat;
    },
    getPath() {
      return this.path;
    },
    setPath(path = null) {
      this.path = path;
    },
    getPhoto() {
      return this.photo;
    },
    setPhoto(photo = null) {
      this.photo = photo;
    },
    getLevel() {
      return this.tlp;
    },
    setLevel(level = null) {
      this.level = level;
    }
  }
});
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/GetterSetter/UserGS.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as _
};
