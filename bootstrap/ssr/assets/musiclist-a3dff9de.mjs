import { defineComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderList, ssrInterpolate } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = defineComponent({
  name: "musiclist-music",
  props: {
    musiclist: {
      type: Object,
      default: null
    }
  },
  data() {
    return {};
  }
});
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<div${ssrRenderAttrs(mergeProps({
    id: "dashboard_musiclist",
    class: "is-hidden fadein p-0 m-t-0"
  }, _attrs))}><div class="field has-addons"><div class="control is-link"><div class="select is-fullwidth is-rounded"><select name="select_playlist" id="select_playlist" class=""><option value="" disabled selected>-- Select Playlist --</option></select></div></div><div class="control is-expanded"><input type="text" name="" id="" class="input is-rounded" placeholder="Find your music here within selected playlist.."></div></div><div class="has-text-black"><!--[-->`);
  ssrRenderList(_ctx.musiclist.data, (ml, xmusiclist) => {
    _push(`<div class="item-list">${ssrInterpolate(ml.name)}</div>`);
  });
  _push(`<!--]--></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/2/1_dashboard/4_musiclist/musiclist.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const MyMusicList = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  MyMusicList as default
};
