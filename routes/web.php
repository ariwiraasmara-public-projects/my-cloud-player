<?php

use App\Models\User;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TestViewController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Libraries\myfunction as fun;
use App\MyLibs\auth;
use App\MyLibs\mcr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
*/

Route::get('/', function(){
    return Inertia::render('DummyPage');
});

Route::get('test/function', function(){
    echo fun::encrypt('password');
    // return
});

Route::get('test/coba', 'App\Http\Controllers\APIController@coba');
Route::get('test/login', 'App\Http\Controllers\TestViewController@login');
Route::get('test/signin', 'App\Http\Controllers\TestViewController@signin');
Route::get('test/aboutus', 'App\Http\Controllers\TestViewController@aboutus');
Route::get('test/forgot_password', 'App\Http\Controllers\TestViewController@forgot_password');
Route::get('test/storage_levelup', 'App\Http\Controllers\TestViewController@storage_levelup');
Route::get('test/dashboard', 'App\Http\Controllers\TestViewController@dashboard');

Route::get('test/query', function(){

});

Route::get('echo', function(){
    echo URL::to('/');
});

Route::get('error/get/', function() {
    echo 'df';
});

// Home or Dashboard routes
// Route::get('', auth::ifLogin_onRoute())->name('home');

// Test User Unauth routes
Route::get('/utest', mcr::UserUnauth('test'));
Route::get('/utest/{id}', mcr::UserUnauth('test'));

// Routes before login {
    Route::get('/p/login', mcr::UserUnauth('login'))->name('login');
    Route::get('/p/logout', mcr::UserAuth('logout'))->name('logout');

    Route::get('/p/signin', mcr::UserUnauth('signin'))->name('signin');
    Route::get('/p/password/forgot', mcr::UserUnauth('forgotpassword'))->name('forgotpassword');
    Route::get('/p/aboutus', mcr::UserUnauth('aboutus'))->name('aboutus');
    Route::get('/p/storage_levelup', mcr::asmcp1008('index'))->name('storagelevelup');

    Route::get('/player', mcr::UserAuth('index'))->name('player');
// } Routes before login

require __DIR__.'/auth.php';
