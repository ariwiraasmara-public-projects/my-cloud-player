<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIController;
use App\Http\Controllers\Asmcp1008LevelingsystemController;
use App\MyLibs\auth;
use App\MyLibs\mcr;
use App\Libraries\jsr;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/coba', mcr::API('coba'));
// Route::get('/coba', function(){
//     return new jsr(['message'=>'hello', 'success'=>1], 'ok');
// });

Route::post('/user/login', mcr::API('login'));
Route::post('/user/save', mcr::API('saveuser'))->name('user/save');
Route::get('/user/logout', mcr::API('logout'));

Route::get('/level/list', mcr::asmcp1008('getAll'))->name('listalllevel');

Route::group(array('middleware' => ['checkHeader']), function () {
    // Route::get('/coba', mcr::API('coba'));
    //Route::get('/logout', mcr::API('logout'));

    // User Profile {
        Route::get('/user/{id}', mcr::API('getUserProfile'))->name('user');
        Route::get('/user/edit/get/{id}', mcr::API('getUserEditProfile'))->name('useredit');
        Route::post('/user/edit/save/{id}', mcr::API('updateUserProfile'))->name('usereditsave');
    // } User Profile

    // User Setting {
        Route::get('/user/setting/get/{id}', mcr::API('getUserSetting'))->name('usersetting');
        Route::post('/user/setting/save/{id}', mcr::API('updateUserSetting'))->name('usersettingupdate');
    // } User Setting

    // Folder {
        Route::get('/user/folder/get/{id}/{order}/{by}', mcr::API('getFolder'))->name('userfolders');
        Route::get('/user/folder/detail{id}', mcr::API('getDetailFolder'))->name('detailplaylist');
        Route::post('/user/folder/create/{id}', mcr::API('createFolder'))->name('createfolder');
        Route::post('/user/folder/update/{id}', mcr::API('updateFolder'))->name('updatefolder');
        Route::delete('/user/folder/delete/{id}', mcr::API('deleteFolder'))->name('deletefolder');
    // }

    // Playlist {
        Route::get('/user/playlist/get/{id}/{by}', mcr::API('getPlaylist'))->name('userplaylist');
        Route::get('/user/playlist/detail{id}', mcr::API('getDetailPlaylist'))->name('detailplaylist');
        Route::post('/user/playlist/create/{id}', mcr::API('createPlaylist'))->name('createplaylist');
        Route::post('/user/playlist/update/{id}', mcr::API('updatePlaylist'))->name('updateplaylist');
        Route::delete('/user/playlist/delete/{id}', mcr::API('deletePlaylist'))->name('deleteplaylist');
    // } Playlist

    // FILE {
        Route::get('/user/file/get/{id1}/{id2}/{order}/{by}/{fp}', mcr::API('getFile'))->name('userfiles');
        Route::get('/user/file/detail/{id}', mcr::API('getDetailFile'))->name('userdetailfile');
        Route::post('/user/file/store/{id}', mcr::API('userUploadFile'))->name('uploadfile');
        Route::post('/user/file/update/{id}', mcr::API('userUpdateFile'))->name('updatefile');
        Route::post('/user/file/move', mcr::API('moveFile'))->name('userfilemoveto');
        Route::delete('/user/file/delete/{id}', mcr::API('userDeleteFile'))->name('deletefile');
    // }

    // 1005 UserPlayList API
    //Route::get('/user/playlist', mcr::API('getPlayList'))->name('user/playlist');
});

// 1008 LevelingSystem API
Route::get('/level/all', mcr::asmcp1008('getAll'));
// Route::resource('/level/all', '\App\Http\Controllers\Asmcp1008LevelingsystemController@getAll');
// Route::resource('/level/all', [App\Http\Controllers\Asmcp1008LevelingsystemController::class, 'getAll']);
