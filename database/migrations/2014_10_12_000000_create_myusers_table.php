<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
			$table->id();
            $table->string('username', 255)->unique(); // username
            $table->string('email', 255)->unique();
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->text('password')->nullable()->default(null);
            $table->integer('pin')->nullable()->default(null);
            $table->string('tlp', 20)->nullable()->default(null);
            $table->rememberToken()->nullable()->default(null);
            $table->text('path')->nullable()->default(null);
            $table->integer('role')->default(2);
            $table->timestamps();
            $table->softDeletesTz($column = 'deleted_at', $precision = 0)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
