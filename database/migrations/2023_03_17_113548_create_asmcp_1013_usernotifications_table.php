<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('asmcp_1013_usernotification', function (Blueprint $table) {
            $table->string('id_1013')->primary(); /*[ID_USER]#NO0000000000000000000000000000000000000000000001*/
            $table->string('id_user');
            $table->dateTime('date')->nullable()->default(null);
            $table->text('message')->nullable()->default(null);
            $table->integer('isread')->nullable()->default(null);
            $table->softDeletesTz($column = 'deleted_at', $precision = 0)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('asmcp_1013_usernotification');
    }
};
