<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asmcp_1001_userid', function (Blueprint $table) {
            $table->string('id_1001', 255)->primary();
            $table->unsignedBigInteger('id')->unique()->default(null);
            $table->string('nama')->nullable()->default(null);
            $table->string('tempat_lahir')->nullable()->default(null);
            $table->date('tgl_lahir')->nullable()->default(null);
            $table->text('alamat')->nullable()->default(null);
            $table->text('photo')->nullable()->default(null);

            // $table->foreign('id')->references('id')->on('asmcp_1000_user')->onDelete('cascade');
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
            $table->index(['id_1001', 'id', 'nama']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asmcp_1001_userid');
    }
};
