<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asmcp_1011_onetimepayment', function (Blueprint $table) {
            $table->string('id_1011')->primary();
            $table->string('id_1010');
            $table->date('datepay');
            $table->integer('amount');

            $table->foreign('id_1010')->references('id_1010')->on('asmcp_1010_paymenttype')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asmcp_1011_onetimepayment');
    }
};
