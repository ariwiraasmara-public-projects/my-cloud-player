<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asmcp_1012_subscription', function (Blueprint $table) {
            $table->string('id_1012')->primary();
            $table->string('id_1010');
            $table->integer('is_active')->default(0);
            $table->integer('is_trial')->default(1);
            $table->date('datepay')->nullable()->default(null);
            $table->integer('amount')->default(0);
            $table->date('tilldate')->nullable()->default(null);

            $table->foreign('id_1010')->references('id_1010')->on('asmcp_1010_paymenttype')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asmcp_1012_subscription');
    }
};
