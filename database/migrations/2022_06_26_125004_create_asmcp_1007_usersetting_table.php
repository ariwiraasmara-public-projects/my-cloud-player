<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asmcp_1007_usersetting', function (Blueprint $table) {
            $table->string('id_1001', 255)->primary();
            $table->enum('theme', ['#000', '#fff'])->nullable()->default('#000');
            $table->enum('text', ['#000', '#787878', '#fff'])->nullable()->default('#fff');
            $table->text('bar')->nullable()->default('#046ffb');
            $table->text('wall_img')->nullable()->default('none');
            $table->integer('wall_height')->nullable()->default(0);
            $table->integer('wall_width')->nullable()->default(0);
            $table->text('wall_size')->nullable()->default('none');
            $table->text('wall_position')->nullable()->default('none');
            $table->enum('wall_repeat', ['repeat', 'repeat-x', 'repeat-y', 'no-repeat'])->nullable()->default('no-repeat');
            $table->enum('wall_attachment', ['none','scroll', 'fixed'])->nullable()->default('none');

            $table->foreign('id_1001')->references('id_1001')->on('asmcp_1001_userid')->onDelete('cascade');
            $table->index(['id_1007', 'id_1001']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asmcp_1007_usersetting');
    }
};
