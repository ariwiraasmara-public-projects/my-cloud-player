<?php
namespace App\Mylibs;

class mysession {

    public function __construct(protected $nama = '__name__',
                                protected $pass = '__pass__',
                                protected $pin  = '000000',
                                protected $tlp  = '+628000',
                                protected $tgl  = '__tgl__',
                                protected $theme = '#fff', 
                                protected $text = '#000', 
                                protected $bar = '#00f', 
                                protected $wall_img = null, 
                                protected $wall_height = null, 
                                protected $wall_width = null, 
                                protected $wall_size = null, 
                                protected $wall_position = null,  
                                protected $wall_repeat = null, 
                                protected $wall_attachment = null) {

    }

    // Edit Profile {
        public function setEditProfile($a, $b, $c, $d, $e) {
            $this->nama = $a;
            $this->pass = $b;
            $this->pin  = $c;
            $this->tlp  = $d;
            $this->tgl  = $e;
        }

        public function getNama() {
            return $this->nama;
        }

        public function getPass() {
            return $this->pass;
        }

        public function getPin() {
            return  $this->pin;
        }

        public function getTlp() {
            return $this->tlp;
        }

        public function getTgl() {
            return  $this->tgl;
        }
    // } Edit Profile

    // Setting {
        public function setSetting($a, $b, $c, $d, $e, $f, $g, $h, $i, $j) {
            $this->theme            = $a;
            $this->text             = $b;
            $this->bar              = $c;
            $this->wall_img         = $d;
            $this->wall_height      = $e;
            $this->wall_width       = $f;
            $this->wall_size        = $g;
            $this->wall_position    = $h;
            $this->wall_repeat      = $i;
            $this->wall_attachment  = $j;
        }

        public function getTheme() {
            return $this->theme;
        }

        public function getText() {
            return $this->text;
        }

        public function getBar() {
            return $this->bar;
        }

        public function getWall_img() {
            return $this->wall_img;
        }

        public function getWall_height() {
            return $this->wall_height;
        }

        public function getWall_width() {
            return $this->wall_width;
        }

        public function getWall_size() {
            return $this->wall_size;
        }

        public function getWall_position() {
            return $this->wall_position;
        }

        public function getWall_repeat() {
            return $this->wall_repeat;
        }

        public function getWall_attachment() {
            return $this->wall_attachment;
        }
    // } Setting

}
?>