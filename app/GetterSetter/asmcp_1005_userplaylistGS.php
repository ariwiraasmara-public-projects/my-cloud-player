<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1005_userplaylistfoldersGSInterface;
class asmcp_1005_userplaylistfoldersGS implements asmcp_1005_userplaylistfoldersGSInterface {

    //protected String $id_1005, $id_1001, $playlist;

    public function __construct(protected String $id_1004,
                                protected String $id_1001,
                                protected String $playlist,) {
        $this->setID1005($id_1005);
        $this->setID1001($id_1001);
        $this->setPlaylist($foldername);
    }

    public function setID1005(String $id_1005 = '__null__') {
        $this->id_1005 = $id_1005;
    }

    public function getID1005() : String {
        return $this->id_1005;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001;
    }

    public function setPlaylist(String $playlist = '__null__') {
        $this->playlist = $playlist;
    }

    public function getPlaylist() : String {
        return $this->playlist;
    }

}
?>
