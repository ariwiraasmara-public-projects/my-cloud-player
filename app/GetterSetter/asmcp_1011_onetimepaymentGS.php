<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1011_onetimepaymentGSInterface;
class asmcp_1011_onetimepaymentGS implements asmcp_1011_onetimepaymentGSInterface {

    // protected String $id_1011, $id_1010, $datepay;
    // protected int $amount;

    public function __construct(protected String $id_1011 = '__null__',
                                protected String $id_1010 = '__null__',
                                protected String $datepay = '1990-01-01',
                                protected int $amount = 0) {
        $this->setID1011($id_1011);
        $this->setID1010($id_1010);
        $this->setDatepay($datepay);
        $this->setAmount($amount);
    }

    public function setID1011(String $id_1011 = '__null__') {
        $this->id_1011 = $id_1011;
    }

    public function getID1011() : String {
        return $this->id_1011;
    }

    public function setID1010(String $id_1010 = '__null__') {
        $this->id_1010 = $id_1010;
    }

    public function getID1010() : String {
        return $this->id_1010;
    }

    public function setDatepay(String $type = null) {
        $this->datepay = $datepay;
    }

    public function getDatepay() : String {
        return $this->datepay;
    }

    public function setAmount(int $amount = 0) {
        $this->amount = $amount;
    }

    public function getAmount() : String {
        return $this->amount;
    }

}
?>
