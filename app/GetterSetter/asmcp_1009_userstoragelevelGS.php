<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1009_userstoragelevelGSInterface;
class asmcp_1009_userstoragelevelGS implements asmcp_1009_userstoragelevelGSInterface {

    // protected String $id_1009, $id_1001, $upgraded_lvl2_at, $upgraded_lvl3_at;
    // protected String $upgraded_lvl4_at, $upgraded_lvl5_at, $upgraded_lvl6_at;
    // protected String $upgraded_lvl7_at, $upgraded_lvl8_at, $upgraded_lvl9_at;
    // protected int $level;

    public function __construct(protected String $id_1009 = '__null__',
                                protected String $id_1001 = '__null__',
                                protected int $level = 0,
                                protected String $upgraded_lvl2_at = '__null__',
                                protected String $upgraded_lvl3_at = '__null__',
                                protected String $upgraded_lvl4_at = '__null__',
                                protected String $upgraded_lvl5_at = '__null__',
                                protected String $upgraded_lvl6_at = '__null__',
                                protected String $upgraded_lvl7_at = '__null__',
                                protected String $upgraded_lvl8_at = '__null__',
                                protected String $upgraded_lvl9_at = '__null__') {
        $this->setID1009($id_1009);
        $this->setID1001($id_1001);
        $this->setLevel($level);
        $this->setLevel2At($upgraded_lvl2_at);
        $this->setLevel3At($upgraded_lvl3_at);
        $this->setLevel4At($upgraded_lvl4_at);
        $this->setLevel5At($upgraded_lvl5_at);
        $this->setLevel6At($upgraded_lvl6_at);
        $this->setLevel7At($upgraded_lvl7_at);
        $this->setLevel8At($upgraded_lvl8_at);
        $this->setLevel9At($upgraded_lvl9_at);
    }

    public function setID1009(String $id_1009 = '__null__') {
        $this->id_1009 = $id_1009;
    }

    public function getID1009() : String {
        return $this->id_1009;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : int {
        return $this->id_1001;
    }

    public function setLevel(int $level = 0) {
        $this->level = $level;
    }

    public function getLevel() : int {
        return $this->level;
    }

    public function setLevel2At(String $upgraded_lvl2_at = null) {
        $this->upgraded_lvl2_at = $upgraded_lvl2_at;
    }

    public function getLevel2At() : String {
        return $this->upgraded_lvl2_at;
    }

    public function setLevel3At(String $upgraded_lvl3_at = null) {
        $this->upgraded_lvl3_at = $upgraded_lvl3_at;
    }

    public function getLevel3At() : String {
        return $this->upgraded_lvl3_at;
    }

    public function setLevel4At(String $upgraded_lvl4_at = null) {
        $this->upgraded_lvl4_at = $upgraded_lvl4_at;
    }

    public function getLevel4At() : String {
        return $this->upgraded_lvl4_at;
    }

    public function setLevel5At(String $upgraded_lvl5_at = null) {
        $this->upgraded_lvl5_at = $upgraded_lvl5_at;
    }

    public function getLevel5At() : String {
        return $this->upgraded_lvl5_at;
    }

    public function setLevel6At(String $upgraded_lvl6_at = null) {
        $this->upgraded_lvl6_at = $upgraded_lvl6_at;
    }

    public function getLevel6At() : String {
        return $this->upgraded_lvl6_at;
    }

    public function setLevel7At(String $upgraded_lvl7_at = null) {
        $this->upgraded_lvl7_at = $upgraded_lvl7_at;
    }

    public function getLevel7At() : String {
        return $this->upgraded_lvl7_at;
    }

    public function setLevel8At(String $upgraded_lvl8_at = null) {
        $this->upgraded_lvl8_at = $upgraded_lvl8_at;
    }

    public function getLevel8At() : String {
        return $this->upgraded_lvl8_at;
    }

    public function setLevel9At(String $upgraded_lvl9_at = null) {
        $this->upgraded_lvl9_at = $upgraded_lvl9_at;
    }

    public function getLevel9At() : String {
        return $this->upgraded_lvl9_at;
    }

}
?>
