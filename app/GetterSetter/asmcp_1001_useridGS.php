<?php
namespace App\GetterSetter;

class asmcp_1001_useridGS {

    //protected String $id_1001, $nama, $tempat_lahir, $tgl_lahir, $alamat;
    //protected int $id;

    public function __construct(protected String $id_1001 = '__null__',
                                protected String $nama = '__null__',
                                protected String $tempat_lahir = '__null__',
                                protected String $tgl_lahir = '__null__',
                                protected String $alamat = '__null__',
                                protected int $id = 0) {
        $this->setID1001($id_1001);
        $this->setID($id);
        $this->setNama($nama);
        $this->setTempatLahir($tempat_lahir);
        $this->setTglLahir($tgl_lahir);
        $this->setAlamat($alamat);
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001;
    }

    public function setID(int $id = null) {
        $this->id = $id;
    }

    public function getID() :int {
        return $this->id;
    }

    public function setNama(String $nama = '__null__') {
        $this->nama = $nama;
    }

    public function getNama() : String {
        return $this->nama;
    }

    public function setTempatLahir(String $tempat_lahir = '__null__') {
        $this->tempat_lahir = $tempat_lahir;
    }

    public function getTempatLahir() : String {
        return $this->tempat_lahir;
    }

    public function setTglLahir(String $tgl_lahir = null) {
        $this->tgl_lahir = $tgl_lahir;
    }

    public function getTglLahir() : String {
        return $this->tgl_lahir;
    }

    public function setAlamat(String $alamat = '__null__') {
        $this->alamat = $alamat;
    }

    public function getAlamat() : String {
        return $this->alamat;
    }

}
?>
