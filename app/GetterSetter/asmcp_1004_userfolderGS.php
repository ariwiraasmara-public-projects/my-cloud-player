<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1004_userfoldersGSInterface;
class asmcp_1004_userfoldersGS implements asmcp_1004_userfoldersGSInterface {

    //protected String $id_1004, $id_1001, $foldername, $ket;

    public function __construct(protected String $id_1004,
                                protected String $id_1001,
                                protected String $foldername,
                                protected String $ket) {
        $this->setID1004($id_1004);
        $this->setID1001($id_1001);
        $this->setFoldername($foldername);
        $this->setKet($ket);
    }

    public function setID1004(String $id_1004 = '__null__') {
        $this->id_1004 = $id_1004;
    }

    public function getID1004() : String {
        return $this->id_1004;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001;
    }

    public function setFoldername(String $foldername = '__null__') {
        $this->foldername = $foldername;
    }

    public function getFoldername() : String {
        return $this->foldername;
    }

    public function setKet(String $ket = '__null__') {
        $this->ket = $ket;
    }

    public function getKet() : String {
        return $this->ket;
    }

}
?>
