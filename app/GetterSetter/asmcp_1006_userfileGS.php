<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1006_userfileGSInterface;
class asmcp_1006_userfileGS implements asmcp_1006_userfileGSInterface {

    // protected String $id_1006, $id_1001, $filename;
    // protected String $genre, $artist, $album;
    // protected String $composer, $publisher, $ket;
    // protected String $favorited, $folder, $playlist;

    public function __construct(protected String $id_1006 = '__null__',
                                protected String $id_1001 = '__null__',
                                protected String $filename = '__null__',
                                protected String $genre = '__null__',
                                protected String $artist = '__null__',
                                protected String $album = '__null__',
                                protected String $composer = '__null__',
                                protected String $publisher = '__null__',
                                protected String $ket = '__null__',
                                protected String $favorited = '__null__',
                                protected String $folder = '__null__',
                                protected String $playlist = '__null__') {
        $this->setID1006($id_1006);
        $this->setID1001($id_1001);
        $this->setFilename($filename);
        $this->setGenre($genre);
        $this->setArtist($artist);
        $this->setAlbum($album);
        $this->setComposer($composer);
        $this->setPublisher($publisher);
        $this->setKet($ket);
        $this->setFavorited($favorited);
        $this->setFolder($folder);
        $this->setPlaylist($playlist);
    }

    public function setID1006($id_1006 = '__null__') {
        $this->id_1006 = $id_1006;
    }

    public function getID1006() : String {
        return $this->id_1006;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001;
    }

    public function setFilename($filename = '__null__') {
        $this->filename = $filename;
    }

    public function getFilename() : String {
        return $this->filename;
    }

    public function setGenre($genre = '__null__') {
        $this->genre = $genre;
    }

    public function getGenre() : String {
        return $this->genre;
    }

    public function setArtist($artist = '__null__') {
        $this->artist = $artist;
    }

    public function getArtist() : String {
        return $this->artist;
    }

    public function setAlbum($album = '__null__') {
        $this->album = $album;
    }

    public function getAlbum() : String {
        return $this->album;
    }

    public function setComposer($composer = '__null__') {
        $this->composer = $composer;
    }

    public function getComposer() : String {
        return $this->composer;
    }

    public function setPublisher($publisher = '__null__') {
        $this->publisher = $publisher;
    }

    public function getPublisher() : String {
        return $this->publisher;
    }

    public function setKet($ket = '__null__') {
        $this->ket = $ket;
    }

    public function getKet() : String {
        return $this->ket;
    }

    public function setFavorited($favorited = '__null__') {
        $this->favorited = $favorited;
    }

    public function getFavorited() : String {
        return $this->favorited;
    }

    public function setFolder($folder = '__null__') {
        $this->folder = $folder;
    }

    public function getFolder() : String {
        return $this->folder;
    }

    public function setPlaylist($playlist = '__null__') {
        $this->playlist = $playlist;
    }

    public function getPlaylist() : String {
        return $this->playlist;
    }

}
?>
