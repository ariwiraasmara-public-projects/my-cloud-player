<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1010_userdirGSInterface;
class asmcp_1010_userdirGS implements asmcp_1010_userdirGSInterface {

    // protected String $id_1010, $id_1001, $type;

    public function __construct(protected String $id_1010 = '__null__',
                                protected String $id_1001 = '__null__',
                                protected int $type = 0) {
        $this->setID1010($id_1010);
        $this->setID1001($id_1001);
        $this->setType($type);
    }

    public function setID1010(String $id_1010 = '__null__') {
        $this->id_1010 = $id_1010;
    }

    public function getID1010() : String {
        return $this->id_1010;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001;
    }

    public function setType(int $type = 0) {
        $this->type = $type;
    }

    public function getType() : int {
        return $this->type;
    }

}
?>
