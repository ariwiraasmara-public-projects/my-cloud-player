<?php
namespace App\GetterSetter;

class asmcp_error_reportGS {

    // protected int $id, $line;
    // protected String $message, $file, $stack_trace;

    public function __construct(protected int $id = 0,
                                protected String $message = '__null__',
                                protected String $file = '__null__',
                                protected int $line = 0,
                                protected String $stack_trace = '__null__') {
        $this->setID($id);
        $this->setMessage($message);
        $this->setFile($file);
        $this->setLine($line);
        $this->setStackTrace($stack_trace);
    }

    public function setID($id) {
        $this->id = $id;
    }

    public function getID(): int {
        return $this->id;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function getMessage(): String {
        return $this->message;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function getFile(): String {
        return $this->file;
    }

    public function setLine($line) {
        $this->line = $line;
    }

    public function getLine(): String {
        return $this->line;
    }

    public function setStackTrace($stack_trace) {
        $this->stack_trace = $stack_trace;
    }

    public function getStackTrace(): String {
        return $this->stack_trace;
    }

}
?>
