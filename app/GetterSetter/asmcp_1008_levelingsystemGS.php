<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1002_userdirGSInterface;
class asmcp_1002_userdirGS implements asmcp_1002_userdirGSInterface {

    // protected int $id, $level, $memory;
    // protected int $monthly_pay_usd, $yearly_pay_usd, $lifetime_pay_usd;

    public function __construct(protected int $id = 0,
                                protected int $level = 0,
                                protected int $memory = 0,
                                protected int $monthly_pay_usd = 0,
                                protected int $yearly_pay_usd = 0,
                                protected int $lifetime_pay_usd = 0) {
        $this->setID($id);
        $this->setLevel($level);
        $this->setMemory($memory);
        $this->setMonthlyPayUSD($monthly_pay_usd);
        $this->setYearlyPayUSD($yearly_pay_usd);
        $this->setLifetimePayUSD($lifetime_pay_usd);
    }

    public function setID(int $id = 0) {
        $this->id = $id;
    }

    public function getID() : int {
        return $this->id;
    }

    public function setLevel(int $level = 0) {
        $this->level = $level;
    }

    public function getLevel() : int {
        return $this->level;
    }

    public function setMemory(int $memory = 0) {
        $this->memory = $memory;
    }

    public function getMemory() : int {
        return $this->memory;
    }

    public function setMonthlyPayUSD(int $monthly_pay_usd = 0) {
        $this->monthly_pay_usd = $monthly_pay_usd;
    }

    public function getMonthlyPayUSD() : int {
        return $this->monthly_pay_usd;
    }

    public function setYearlyPayUSD(int $yearly_pay_usd = 0) {
        $this->yearly_pay_usd = $yearly_pay_usd;
    }

    public function getYearlyPayUSD() : int {
        return $this->yearly_pay_usd;
    }

    public function setLifetimePayUSD(int $lifetime_pay_usd = 0) {
        $this->lifetime_pay_usd = $lifetime_pay_usd;
    }

    public function getLifetimePayUSD() : int {
        return $this->lifetime_pay_usd;
    }

}
?>
