<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1002_userdirGSInterface;
class asmcp_1002_userdirGS implements asmcp_1002_userdirGSInterface {

    //protected String $id_1002, $id_1001, $rootdir;

    public function __construct(protected String $id_1002 = '__null__',
                                protected String $id_1001 = '__null__',
                                protected String $rootdir = '__null__') {
        $this->setID1002($id_1002);
        $this->setID1001($id_1001);
        $this->setRootdir($rootdir);
    }

    public function setID1002(String $id_1002 = '__null__') {
        $this->id_1002 = $id_1002;
    }

    public function getID1002() : String {
        return $this->id_1002;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001;
    }

    public function setRootdir(String $rootdir = '__null__') {
        $this->rootdir = $rootdir;
    }

    public function getRootdir() : String {
        return $this->rootdir;
    }

}
?>
