<?php
namespace App\GetterSetter;

use App\Interfaces\UserGSInterface;
class UserGS implements UserGSInterface {

    protected String $username, $email, $password, $path;

    public function __construct(String $username = '__null__',
                                String $email = '__null__',
                                String $password = '__null__',
                                String $path = '__null__') {
        $this->setUsername($username);
        $this->setEmail($email);
        $this->setPassword($password);
        $this->setPath($path);
    }

    public function setUsername(String $username = '__null__') {
        $this->username = $username;
    }

    public function getUsername() : String {
        return $this->username;
    }

    public function setEmail(String $email = '__null__') {
        $this->email = $email;
    }

    public function getEmail() : String {
        return $this->email;
    }

    public function setPassword(String $password = '__null__') {
        $this->password = $password;
    }

    public function getPassword() : String {
        return $this->password;
    }

    public function setPath(String $path = '__null__') {
        $this->path = $path;
    }

    public function getPath() : String {
        return $this->path;
    }

}
?>
