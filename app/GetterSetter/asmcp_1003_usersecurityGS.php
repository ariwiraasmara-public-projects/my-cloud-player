<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1003_usersecurityGSInterface;
class asmcp_1003_usersecurityGS implements asmcp_1003_usersecurityGSInterface {

    //protected String $id_1003, $id_1001;
    //protected String $device1, $device_type1, $device_os1;
    //protected String $device2, $device_type2, $device_os2;

    public function __construct(protected String $id_1003 = '__null__',
                                protected String $id_1001 = '__null__',
                                protected String $device1 = '__null__',
                                protected String $device_type1 = '__null__',
                                protected String $device_os1 = '__null__',
                                protected String $device2 = '__null__',
                                protected String $device_type2 = '__null__',
                                protected String $device_os2 = '__null__') {
        $this->setID1003($id_1003);
        $this->setID1001($id_1001);
        $this->setDevice1($device1);
        $this->setDevicetype1($device_type1);
        $this->setDeviceos1($device_os1);
        $this->setDevice2($device2);
        $this->setDevicetype2($device_type2);
        $this->setDeviceos2($device_os2);
    }

    public function setID1003(String $id_1003 = '__null__') {
        $this->id_1003 = $id_1003;
    }

    public function getID1003() : String {
        return $this->id_1003 = $id_1003;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001 = $id_1001;
    }

    public function setDevice1(String $device1 = '__null__') {
        $this->device1 = $device1;
    }

    public function getDevice1() {
        return $this->device1;
    }

    public function setDevicetype1(String $device_type1 = '__null__') {
        $this->device_type1 = $device_type1;
    }

    public function getDevicetype1() : String {
        return $this->device_type1;
    }

    public function setDeviceos1(String $device_os1 = '__null__') {
        $this->device_os1 = $device_os1;
    }

    public function getDeviceos1() : String {
        return $this->device_os1;
    }

    public function setDevice2(String $device2 = '__null__') {
        $this->device2 = $device2;
    }

    public function getDevice2() {
        return $this->device2;
    }

    public function setDevicetype2(String $device_type2 = '__null__') {
        $this->device_type2 = $device_type2;
    }

    public function getDevicetype2() : String {
        return $device_type2;
    }

    public function setDeviceos2(String $device_os2 = '__null__') {
        $this->device_os2 = $device_os2;
    }

    public function getDeviceos2() : String {
        return $this->device_os2;
    }

}
?>
