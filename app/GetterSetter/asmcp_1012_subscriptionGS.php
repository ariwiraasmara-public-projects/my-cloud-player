<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1012_subscriptionGSInterface;
class asmcp_1012_subscriptionGS implements asmcp_1012_subscriptionGSInterface {

    public function __construct(protected String $id_1012 = '__null__',
                                protected String $id_1010 = '__null__',
                                protected int $is_active = 0,
                                protected int $is_trial = 0,
                                protected String $datepay = '__null__',
                                protected int $amount = 0,
                                protected String $tilldate = '__null__') {
        $this->setID1012($id_1012);
        $this->setID1010($id_1010);
        $this->setIsactive($is_active);
        $this->setIstrial($is_trial);
        $this->setAmount($amount);
        $this->setTilldate($tilldate);
    }

    public function setID1012(String $id_1012 = '__null__') {
        $this->id_1012 = $id_1012;
    }

    public function getID1012() : String {
        return $this->id_1012;
    }

    public function setID1010(String $id_1010 = '__null__') {
        $this->id_1010 = $id_1010;
    }

    public function getID1010() : String {
        return $this->id_1010;
    }

    public function setIsactive(int $is_active = 0) {
        $this->is_active = $is_active;
    }

    public function getIsactive() : int {
        return $this->is_active;
    }

    public function setIstrial(int $is_trial = 0) {
        $this->is_trial = $is_trial;
    }

    public function getIstrial() : int {
        return $this->is_trial;
    }

    public function setDatepay(String $datepay = null) {
        $this->datepay = $datepay;
    }

    public function getDatepay() : String {
        return $this->datepay;
    }

    public function setAmount(int $amount = 0) {
        $this->amount = $amount;
    }

    public function getAmount() : int {
        return $this->amount;
    }

    public function setTilldate(String $tilldate = null) {
        $this->tilldate = $tilldate;
    }

    public function getTilldate() : String {
        return $this->tilldate;
    }

}
?>
