<?php
namespace App\GetterSetter;

use App\Interfaces\asmcp_1006_userfileGSInterface;
class asmcp_1006_userfileGS implements asmcp_1006_userfileGSInterface {

    // protected String $id_1007, $id_1001, $theme;
    // protected String $text, $bar, $wall_img;
    // protected String $wall_heigth, $wall_width, $wall_size;
    // protected String $wall_position, $wall_repeat, $wall_attachment;

    public function __construct(protected String $id_1007 = '__null__',
                                protected String $id_1001 = '__null__',
                                protected String $theme = '__null__',
                                protected String $text = '__null__',
                                protected String $bar = '__null__',
                                protected String $wall_img = '__null__',
                                protected String $wall_heigth = '__null__',
                                protected String $wall_width = '__null__',
                                protected String $wall_size = '__null__',
                                protected String $wall_position = '__null__',
                                protected String $wall_repeat = '__null__',
                                protected String $wall_attachment = '__null__') {
        $this->setID1007($id_1007);
        $this->setID1001($id_1001);
        $this->setTheme($theme);
        $this->setText($text);
        $this->setBar($bar);
        $this->setWallimg($wall_img);
        $this->setWallheight($wall_heigth);
        $this->setWallwidth($wall_width);
        $this->setWallsize($wall_size);
        $this->setWallposition($wall_position);
        $this->setWallrepeat($wall_repeat);
        $this->setWallattachment($wall_attachment);
    }

    public function setID1007(String $id_1006 = '__null__') {
        $this->id_1006 = $id_1006;
    }

    public function getID1007() : String {
        return $this->id_1006;
    }

    public function setID1001(String $id_1001 = '__null__') {
        $this->id_1001 = $id_1001;
    }

    public function getID1001() : String {
        return $this->id_1001;
    }

    public function setTheme(String $theme = '__null__') {
        $this->theme = $theme;
    }

    public function getTheme() : String {
        return $this->theme;
    }

    public function setText(String $text = '__null__') {
        $this->text = $text;
    }

    public function getText() : String {
        return $this->text;
    }

    public function setBar(String $bar = '__null__') {
        $this->bar = $bar;
    }

    public function getBar() : String {
        return $this->bar;
    }

    public function setWallimg(String $wall_img = '__null__') {
        $this->wall_img = $wall_img;
    }

    public function getWallimg() : String {
        return $this->wall_img;
    }

    public function setWallheight(int $wall_height = 0) {
        $this->wall_heigth = $wall_heigth;
    }

    public function getWallheigth() : int {
        return $this->wall_heigth;
    }

    public function setWallwidth(int $wall_width = 0) {
        $this->wall_width = $wall_width;
    }

    public function getWallwidth() : int {
        return $this->wall_width;
    }

    public function setWallsize(int $wall_size = 0) {
        $this->wall_size = $wall_size;
    }

    public function getWallsize() : int {
        return $this->wall_size;
    }

    public function setWallposition(String $wall_position = '__null__') {
        $this->wall_position = $wall_position;
    }

    public function getWallposition() : String {
        return $this->wall_position;
    }

    public function setWallrepeat(String $wall_repeat = '__null__') {
        $this->wall_repeat = $wall_repeat;
    }

    public function getWallrepeat() : String {
        return $this->wall_repeat;
    }

    public function setWallattachment(String $wall_attachment = '__null__') {
        $this->wall_attachment = $wall_attachment;
    }

    public function getWallattachment() : String {
        return $this->wall_attachment;
    }
}
?>
