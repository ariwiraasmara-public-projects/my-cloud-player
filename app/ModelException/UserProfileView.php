<?php
namespace App\ModelException;

class UserProfileView {

    public function __construct(protected $nama = '__nama__', 
                                protected $user = '__user__', 
                                protected $email = '__email_', 
                                protected $tgl = '00-00-0000',
                                protected $tlp = '+628000', 
                                protected $pin = '000000', 
                                protected $level = '0') {
    
    }

    public function getNama() {
        if($this->nama == null) $this->nama = '__nama__';
        return $this->nama;
    }

    public function getUser() {
        if($this->user == null) $this->user = '__user__';
        return $this->user;
    }

    public function getEmail() {
        if($this->email == null) $this->email = '__email_';
        return $this->email;
    }

    public function getTgl() {
        if($this->tgl == null) $this->tgl = date('Y-m-d', strtotime('01-01-1970'));
        return date('Y-m-d', strtotime( $this->tgl ));
    }

    public function getTlp() {
        if($this->tlp == null) $this->tlp = '+628000';
        return strval($this->tlp);
    }

    public function getPin() {
        if($this->pin == null) $this->pin = '000000';
        return $this->pin;
    }

    public function getLevel() {
        if($this->level == null) $this->level = '0';
        return $this->level;
    }

}
?>