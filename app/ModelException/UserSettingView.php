<?php
namespace App\ModelException;

class UserSettingView {

    public function __construct(protected $theme = '#fff', 
                                protected $text = '#000', 
                                protected $bar = '#00f', 
                                protected $wall_img = null, 
                                protected $wall_height = null, 
                                protected $wall_width = null, 
                                protected $wall_size = null, 
                                protected $wall_position = null,  
                                protected $wall_repeat = null, 
                                protected $wall_attachment = null) {

    }

    public function getTheme() {
        if($this->theme == null) $this->theme = '#fff';
        return $this->theme;
    }

    public function getText() {
        if($this->text == null) $this->text = '#000';
        return $this->text;
    }

    public function getBar() {
        return $this->bar;
    }

    public function getWallImage() {
        return $this->wall_img;
    }

    public function getWallHeight() {
        return $this->wall_height;
    }

    public function getWallWidth() {
        return $this->wall_width;
    }

    public function getWallSize() {
        return $this->wall_size;
    }

    public function getWallPosition() {
        return $this->wall_position;
    }

    public function getWallRepeat() {
        return $this->wall_repeat;
    }

    public function getWallAttachment() {
        return $this->wall_attachment;
    }

    public function wapo() {
        return ["no-position",
                "left top",
                "left center",
                "left bottom",
                "right top",
                "right center",
                "right bottom",
                "center top",
                "center center",
                "center bottom",];
    }

    public function ware() {
        return ["no-repeat",
                "repeat",
                "repeat-x",
                "repeat-y",];
    }

    public function waat() {
        return ["no-attachment",
                "scroll",
                "fixed",];
    }

}
?>