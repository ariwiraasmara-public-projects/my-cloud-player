<?php
namespace App\ModelException;
use App\MyLibs\myfunction;

class UserEditProfileView {

    public function __construct(protected $nama = '__name__',
                                protected $pass = '__pass__',
                                protected $pin  = '000000',
                                protected $tlp  = '+628000',
                                protected $tgl  = '__tgl__') {

    }

    public function getNama() {
        if($this->nama == null) $this->nama = '__nama__';
        return $this->nama;
    }

    public function getPass() {
        if($this->pass == null) $this->pass = '__pass__';
        return myfunction::decrypt($this->pass);
    }

    public function getPin() {
        if($this->pin == null) $this->pin = '000000';
        return $this->pin;
    }

    public function getTlp() {
        if($this->tlp == null) $this->tlp = '+628000';
        return strval($this->tlp);
    }

    public function getTgl() {
        if($this->tgl == null) $this->tgl = date('yyyy-MM-dd', strtotime('01-01-1970'));
        return date('yy-MM-dd', strtotime( $this->tgl ));
    }

}
?>