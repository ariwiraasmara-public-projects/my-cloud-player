<?php
namespace App\ModelException;
use App\MyLibs\myfunction;

class UserFolderView {

    public function __construct(protected array $data) {

    }

    public function getData() {
        return $this->data;
    }

    public function setData() {
        
    }

    protected string $folder;
    public function setFolder(string $str) {
        $this->folder = $str;
    }

    public function getFolder() {
        return $this->folder;
    }

    public function setKet($str) {
        $this->ket = $str;
    }

    public function getKet() {
        if($this->ket == null) $this->ket = '__ket__';
        return $this->ket;
    }

}
?>