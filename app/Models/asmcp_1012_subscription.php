<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class asmcp_1012_subscription extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'asmcp_1012_subscription';
    protected $primaryKey = 'id_1012';
    protected $fillable = ["id_1012",
                            "id_1010",
                            "is_active",
                            "is_trial",
                            "datepay",
                            "amount",
                            "tilldate",];

    public $timestamps = false;
    public $incrementing = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

    public function asmcp1010() {
        // NamaModel::class, 'foreign_key', 'local_key'
        return $this->hasOne(asmcp_1010_paymenttype::class, 'id_1010', 'id_1010');
    }
}
