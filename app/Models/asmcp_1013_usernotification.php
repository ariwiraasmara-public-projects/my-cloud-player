<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class asmcp_1013_usernotification extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'asmcp_1013_usernotification';
    protected $primaryKey = 'id_1013';
    protected $fillable = ["id_1013",
                            "id_user",
                            "date",
                            "message",
                            "isread",
                            "deleted_at",];

    public $timestamps = false;
    public $incrementing = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
