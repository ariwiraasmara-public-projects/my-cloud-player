<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class asmcp_1010_paymenttype extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'asmcp_1010_paymenttype';
    protected $primaryKey = 'id_1009';
    protected $fillable = ["id_1010",
                            "id_1001",
                            "type",];

    public $timestamps = false;
    public $incrementing = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

    public function asmcp1001() {
        // NamaModel::class, 'foreign_key', 'local_key'
        return $this->hasOne(asmcp_1001_userid::class, 'id_1001', 'id_1001');
    }

}
