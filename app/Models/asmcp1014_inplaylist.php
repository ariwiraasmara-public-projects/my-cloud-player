<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class asmcp1014_inplaylist extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'asmcp_1013_usernotification';
    // protected $primaryKey = 'id_1013';
    protected $primaryKey = false;
    protected $fillable = ["id_1005",
                            "id_1006",];

    public $timestamps = false;
    public $incrementing = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
