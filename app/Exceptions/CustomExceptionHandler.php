<?php
namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use App\Models\ErrorReport;

class CustomExceptionHandler extends ExceptionHandler {

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception

    public function report(Throwable $exception) {
        if ($this->shouldReport($exception)) {
            $errorReport = new ErrorReport();
            $errorReport->message = $exception->getMessage();
            $errorReport->file = $exception->getFile();
            $errorReport->line = $exception->getLine();
            $errorReport->stack_trace = $exception->getTraceAsString();
            $errorReport->save();
        }

        parent::report($exception);
    }
    */

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable

    public function render($request, Throwable $exception) {
        if ($exception instanceof Exception) {
            // tampilkan error message pada user sesuai dengan kebutuhan Anda
            return response()->view('errors.500', [], 500);
        }

        return parent::render($request, $exception);
    }
    */
}
?>
