<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1011_onetimepaymentRepository;
interface asmcp_1011_onetimepaymentServiceInterface {

    public function __construct(asmcp_1011_onetimepaymentRepository $asmcp_1011_onetimepaymentRepository);
    public function getAll(String $by = 'id_1011', String $orderBy = 'asc');
    public function getOne(array $where = null, String $by = 'id_1011', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
