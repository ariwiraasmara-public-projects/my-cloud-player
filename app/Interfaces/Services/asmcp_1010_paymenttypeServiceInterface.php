<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1010_paymenttypeRepository;
interface asmcp_1010_paymenttypeServiceInterface {

    public function __construct(asmcp_1010_paymenttypeRepository $asmcp_1010_paymenttypeRepository);
    public function getAll(String $by = 'id_1010', String $orderBy = 'asc');
    public function getOne(array $where = null);
    public function store(array $data = null);
    public function update(String $val = null, String $id = null);
    public function delete(String $id = null);

}
?>
