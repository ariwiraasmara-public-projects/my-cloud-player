<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1005_userplaylistRepository;
interface asmcp_1005_userplaylistServiceInterface {

    public function __construct(asmcp_1005_userplaylistRepository $asmcp_1005_userplaylistRepository);
    public function getAll(String $id = null, String $orderby = 'asc');
    public function get(String $id = null);
    public function store(String $data = null);
    public function update(String $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
