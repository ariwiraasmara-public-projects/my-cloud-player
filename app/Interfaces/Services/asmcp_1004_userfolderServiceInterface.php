<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1004_userfolderRepository;
interface asmcp_1004_userfolderServiceInterface {

    public function __construct(asmcp_1004_userfolderRepository $asmcp_1004_userfolderRepository);
    public function getAll(String $id = null, String $by = null, String $orderby = 'asc');
    public function get(String $id = null);
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
