<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1006_userfileRepository;
interface asmcp_1006_userfileServiceInterface {

    public function __construct(asmcp_1006_userfileRepository $asmcp_1006_userfileRepository);
    public function getAll(String $id1 = null, String $id2 = null, String $order = 'filename', String $by = 'asc', String $fp = 'folder');
    public function get(String $id1006 = null);
    public function store(String $data = null);
    public function update(String $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
