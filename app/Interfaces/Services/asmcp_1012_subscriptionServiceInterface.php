<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1012_subscriptionRepository;
interface asmcp_1012_subscriptionServiceInterface {

    public function __construct(asmcp_1012_subscriptionRepository $asmcp_1012_subscriptionRepository);
    public function getAll(String $by = 'id_1012', String $orderBy = 'asc');
    public function getOne(array $where = null, String $by = 'id_1012', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
