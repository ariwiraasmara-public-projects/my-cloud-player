<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1002_userdirRepository;
interface asmcp_1002_userdirServiceInterface {

    public function __construct(asmcp_1002_userdirRepository $asmcp_1002_userdirRepository);
    public function get(String $id = null);
    public function store(array $data = null);
    public function update(String $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
