<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1008_levelingsystemRepository;
interface asmcp_1008_levelingsystemServiceInterface {

    public function __construct(asmcp_1008_levelingsystemRepository $asmcp_1008_levelingsystemRepository);
    public function getAll();
    public function getOne(array $where = null, String $by = 'id_1008', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
