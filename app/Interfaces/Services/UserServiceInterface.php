<?php
namespace App\Interfaces\Services;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use App\Interfaces\Services\UserServiceInterface;
use App\Libraries\myfunction;
use App\Repositories\UserRepository;
use App\Repositories\asmcp_1001_useridRepository;
use App\Repositories\asmcp_1002_userdirRepository;
use App\Repositories\asmcp_1003_usersecurityRepository;
use App\Repositories\asmcp_1004_userfolderRepository;
use App\Repositories\asmcp_1005_userplaylistRepository;
use App\Repositories\asmcp_1006_userfileRepository;
use App\Repositories\asmcp_1007_usersettingRepository;
use App\Repositories\asmcp_1009_userstoragelevelRepository;
use App\Repositories\asmcp_1010_paymenttypeRepository;
use App\Repositories\asmcp_1011_onetimepaymentRepository;
use App\Repositories\asmcp_1012_subscriptionRepository;
use App\Repositories\asmcp_1013_usernotificationRepository;
interface UserServiceInterface {

    public function __construct(myfunction $fun,
                                UserRepository $user,
                                asmcp_1001_useridRepository $rp1001,
                                asmcp_1002_userdirRepository $rp1002,
                                asmcp_1003_usersecurityRepository $rp1003,
                                asmcp_1004_userfolderRepository $rp1004,
                                asmcp_1005_userplaylistRepository $rp1005,
                                asmcp_1006_userfileRepository $rp1006,
                                asmcp_1007_usersettingRepository $rp1007,
                                asmcp_1009_userstoragelevelRepository $rp1009,
                                asmcp_1010_paymenttypeRepository $rp1010,
                                asmcp_1011_onetimepaymentRepository $rp1011,
                                asmcp_1012_subscriptionRepository $rp1012,
                                asmcp_1013_usernotificationRepository $rp1013);
    public function login(string $user = null, String $pass = null);
    public function store(array $data = null);
    public function update(array $data = null);
    public function forgot_password();
    public function generateToken();
    public function getUserProfile(array $where = null);
    public function getUserEditProfile(array $where = null);
    public function getUserSetting(array $where = null);
    public function delete(array $data = null);

}
?>
