<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1009_userstoragelevelRepository;
interface asmcp_1009_userstoragelevelServiceInterface {

    public function __construct(asmcp_1009_userstoragelevelRepository $asmcp_1009_userstoragelevelRepository);
    public function getAll(String $by = 'id_1009');
    public function getOne(String $id1001 = null);
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
