<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1003_usersecurityRepository;
interface asmcp_1003_usersecurityServiceInterface {

    public function __construct(asmcp_1003_usersecurityRepository $asmcp_1003_usersecurityRepository);
    public function get(String $id = null);
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
