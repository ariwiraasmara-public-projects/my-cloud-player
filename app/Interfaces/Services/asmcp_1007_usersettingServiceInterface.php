<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1007_usersettingRepository;
interface asmcp_1007_usersettingServiceInterface {

    public function __construct(asmcp_1007_usersettingRepository $asmcp1007);
    public function get(String $id1001 = null);
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
