<?php
namespace App\Interfaces;

interface generateIDServiceInterface {

    public function __construct();
    public function getID();
    public function setRUCID(int $val = null);
    public function getRUCID();
    public function setPUBUSERID(string $str = null);
    public function getPUBUSERID();
    public function getID1001();
    public function setID1001(string $id = null);
    public function SID1001();
    public function getID1002();
    public function setUserPath(string $path);
    public function getUserPath();
    public function getID1003();
    public function getID1004(string $id1001 = null);
    public function getID1005(string $id1001 = null);
    public function getID1006(string $id1001 = null);
    public function getID1007();
    public function getID1009();
    public function getID1010();
    public function getID1011();
    public function getID1012();
    public function getID1013();

}
?>
