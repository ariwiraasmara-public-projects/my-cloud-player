<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1001_useridRepository;
interface asmcp_1001_useridServiceInterface {

    public function __construct(asmcp_1001_useridRepository $asmcp_1001_useridRepository);
    public function store(array $data = null);
    public function getUserEditProfile(String $id1001 = null);
    public function updateProfile(array $data, int $id = null);

}
?>
