<?php
namespace App\Interfaces\Services;

use App\Repositories\asmcp_1013_usernotificationRepository;
interface asmcp_1013_usernotificationServiceInterface {

    public function __construct(asmcp_1013_usernotificationRepository $asmcp_1013_usernotificationRepository);
    public function getAll(String $by = 'id_user', String $orderBy = 'asc');
    public function getOne(array $where = null, String $by = 'id_user', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, String $id = null) ;
    public function softDelete(String $id = null);
    public function hardDelete(String $id = null);

}
?>
