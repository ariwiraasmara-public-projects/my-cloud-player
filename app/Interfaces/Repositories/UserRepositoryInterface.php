<?php
namespace App\Interfaces\Repositories;

use App\Models\User;
interface UserRepositoryInterface {

    public function __construct(User $user);
    public function generateID();
    public function getAll(String $by = 'id');
    public function getOne(array $where = null);
    public function userProfile(array $where = null);
    public function store(array $data = null);
    public function update(array $data = null, int $id = null);
    public function softDelete(int $id = null);
    public function hardDelete(int $id = null);

}
?>
