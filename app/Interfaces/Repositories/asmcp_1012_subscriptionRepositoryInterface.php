<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1012_subscription;
interface asmcp_1012_subscriptionRepositoryInterface {

    public function __construct(asmcp_1012_subscription $asmcp_1012_subscription);
    public function getAll(String $by = 'id_1012', String $orderBy = 'asc');
    public function getOne(array $where = null, String $by = 'id_1012', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, int $id = null);
    public function delete(String $id = null);

}
?>
