<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1011_onetimepayment;
interface asmcp_1011_onetimepaymentRepositoryInterface {

    public function __construct(asmcp_1011_onetimepayment $asmcp_1011_onetimepayment);
    public function getAll(String $by = 'id_1011', String $orderBy = 'asc');
    public function getOne(array $where = null, String $by = 'id_1011', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, int $id = null);
    public function delete(String $id = null);

}
?>
