<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1008_levelingsystem;
interface asmcp_1008_levelingsystemRepositoryInterface {

    public function __construct(asmcp_1008_levelingsystem $asmcp_1008_levelingsystem);
    public function getAll();
    public function getOne(array $where = null, String $by = 'id_1008', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
