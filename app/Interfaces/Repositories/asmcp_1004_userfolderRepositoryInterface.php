<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1004_userfolders;
interface asmcp_1004_userfolderRepositoryInterface {

    public function __construct(asmcp_1004_userfolders $asmcp_1004_userfolders);
    public function getID($id);
    public function getAll(String $id1001 = null, String $by = 'foldername', String $orderby = 'asc');
    public function get(String $id1004 = null);
    public function store(array $data = null);
    public function update(array $data = null, int $id = null);
    public function delete(String $id = null);

}
?>
