<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1005_userplaylists;
interface asmcp_1005_userplaylistRepositoryInterface {

    public function __construct(asmcp_1005_userplaylists $asmcp_1005_userplaylists);
    public function getID($id);
    public function getAll(String $id = null, String $orderby = 'asc');
    public function get(String $id1005 = null);
    public function store(array $data = null);
    public function update(String $val = null, String $id = null);
    public function delete(String $id = null);

}
?>
