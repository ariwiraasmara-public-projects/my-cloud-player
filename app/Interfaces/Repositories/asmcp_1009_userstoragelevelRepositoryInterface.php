<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1009_userstoragelevel;
interface asmcp_1009_userstoragelevelRepositoryInterface {

    public function __construct(asmcp_1009_userstoragelevel $asmcp_1009_userstoragelevel);
    public function getAll(String $by = 'id_1009');
    public function getOne(String $id1001 = null);
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
