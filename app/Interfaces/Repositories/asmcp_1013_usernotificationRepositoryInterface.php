<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1013_usernotification;
interface asmcp_1013_usernotificationRepositoryInterface {

    public function __construct(asmcp_1013_usernotification $asmcp_1013_usernotification);
    public function getAll(String $by = 'id_user', String $orderBy = 'asc');
    public function getOne(array $where = null, String $by = 'id_user', String $orderBy = 'asc');
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function softDelete(String $id = null);
    public function hardDelete(String $id = null);

}
?>
