<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1003_usersecurity;
interface asmcp_1003_usersecurityRepositoryInterface {

    public function __construct(asmcp_1003_usersecurity $asmcp_1003_usersecurity);
    public function getAll(String $by = 'id_1003');
    public function getOne(String $id1001 = null);
    public function store(array $data = null);
    public function update(array $data = null, int $id = null);
    public function delete(String $id = null);


}
?>
