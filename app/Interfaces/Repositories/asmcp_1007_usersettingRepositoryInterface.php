<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1007_usersetting;
interface asmcp_1007_usersettingRepositoryInterface {

    public function __construct(asmcp_1007_usersetting $asmcp_1007_usersetting);
    public function getAll(String $by = 'id_1001');
    public function get(String $id1001 = 'id_1001');
    public function store(array $data = null);
    public function update(array $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
