<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1001_userid;
interface asmcp_1001_useridRepositoryInterface {

    public function __construct(asmcp_1001_userid $asmcp_1001_useridRepository);
    public function getAll(String $by = 'nama');
    public function getOne(String $id1001 = null);
    public function store(array $data = null);
    public function update(array $data = null, int $id = null);
    public function delete(String $id = null);

}
?>
