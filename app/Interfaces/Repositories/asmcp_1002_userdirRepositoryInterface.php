<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1002_userdir;
interface asmcp_1002_userdirRepositoryInterface {

    public function __construct(asmcp_1002_userdir $asmcp_1002_userdir);
    public function getAll(string $orderby = 'asc');
    public function getOne(String $id1001 = null);
    public function store(array $data = null);
    public function update(String $data = null, String $id = null);
    public function delete(String $id = null);

}
?>
