<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1006_userfiles;
use Log;
interface asmcp_1006_userfileRepositoryInterface {

    public function __construct(asmcp_1006_userfiles $asmcp_1006_userfile);
    public function getID($id);
    public function getAll(String $id1 = null, String $id2 = null, String $order = 'filename', String $by = 'asc', String $fp = 'folder');
    public function get(String $id1006 = null);
    public function store(array $data = null);
    public function update(array $data = null, int $id = null);
    public function delete(String $id = null);

}
?>
