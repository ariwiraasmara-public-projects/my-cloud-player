<?php
namespace App\Interfaces\Repositories;

use App\Models\asmcp_1010_paymenttype;
interface asmcp_1010_paymenttypeRepositoryInterface {

    public function __construct(asmcp_1010_paymenttype $asmcp_1010_paymenttype);
    public function getAll(String $by = 'id_1010', String $orderBy = 'asc');
    public function getOne(String $where = null);
    public function store(array $data = null);
    public function update(String $val = null, String $id = null);
    public function delete(String $id = null);

}
?>
