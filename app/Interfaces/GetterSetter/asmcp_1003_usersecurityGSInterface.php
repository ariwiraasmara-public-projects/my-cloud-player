<?php
namespace App\Interfaces;

interface asmcp_1003_usersecurityGSInterface {

    public function __construct(String $id_1003 = '__null__',
                                String $id_1001 = '__null__',
                                String $device1 = '__null__',
                                String $device_type1 = '__null__',
                                String $device_os1 = '__null__',
                                String $device2 = '__null__',
                                String $device_type2 = '__null__',
                                String $device_os2 = '__null__');

    public function setID1003(String $id_1003 = '__null__');
    public function getID1003() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setDevice1(String $device1 = '__null__');
    public function getDevice1();
    public function setDevicetype1(String $device_type1 = '__null__');
    public function getDevicetype1() : String;
    public function setDeviceos1(String $device_os1 = '__null__');
    public function getDeviceos1() : String;
    public function setDevice2(String $device2 = '__null__');
    public function getDevice2();
    public function setDevicetype2(String $device_type2 = '__null__');
    public function getDevicetype2() : String;
    public function setDeviceos2(String $device_os2 = '__null__');
    public function getDeviceos2() : String;

}
?>
