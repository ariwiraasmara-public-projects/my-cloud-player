<?php
namespace App\Interfaces;

interface UserGSInterface {

    public function __construct(String $username = '__null__',
                                String $email = '__null__',
                                String $password = '__null__',
                                String $path = '__null__');

    public function setUsername(String $username = '__null__');
    public function getUsername() : String;
    public function setEmail(String $email = '__null__');
    public function getEmail() : String;
    public function setPassword(String $password = '__null__');
    public function getPassword() : String;
    public function setPath(String $path = '__null__');
    public function getPath() : String;

}
?>
