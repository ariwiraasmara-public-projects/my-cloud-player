<?php
namespace App\Interfaces;

interface asmcp_1011_onetimepaymentGSInterface {

    public function __construct(String $id_1011 = '__null__',
                                String $id_1010 = '__null__',
                                String $datepay = null,
                                int $amount = 0);

    public function setID1010(String $id_1010 = '__null__');
    public function getID1010() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setType(String $type = null);
    public function getType() : String;
    public function setAmount(int $amount = 0);
    public function getAmount() : String;

}
?>
