<?php
namespace App\Interface;

interface asmcp_1008_levelingsystemGSInterface {

    public function __construct(int $id = 0,
                                int $level = 0,
                                int $memory = 0,
                                int $monthly_pay_usd = 0,
                                int $yearly_pay_usd = 0,
                                int $lifetime_pay_usd = 0);

    public function setID(int $id = 0);
    public function getID() : int;
    public function setLevel(int $level = 0);
    public function getLevel() : int;
    public function setMemory(int $memory = 0);
    public function getMemory() : int;
    public function setMonthlyPayUSD(int $monthly_pay_usd = 0);
    public function getMonthlyPayUSD() : int;
    public function setYearlyPayUSD(int $yearly_pay_usd) : int;
    public function getYearlyPayUSD() : int;
    public function setLifetimePayUSD(int $lifetime_pay_usd = 0);
    public function getLifetimePayUSD() : int;

}
?>
