<?php
namespace App\Interfaces;

interface asmcp_1002_userdirGSInterface {

    public function __construct(String $id_1002 = '__null__',
                                String $id_1001 = '__null__',
                                String $rootdir = '__null__');

    public function setID1002(String $id_1002 = '__null__');
    public function getID1002() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setRootdir(String $rootdir = '__null__');
    public function getRootdir() : String;

}
?>
