<?php
namespace App\Interfaces;

interface asmcp_1006_userfileGSInterface {

    public function __construct(String $id_1006 = '__null__',
                                String $id_1001 = '__null__',
                                String $filename = '__null__',
                                String $genre = '__null__',
                                String $artist = '__null__',
                                String $album = '__null__',
                                String $composer = '__null__',
                                String $publisher = '__null__',
                                String $ket = '__null__',
                                String $favorited = '__null__',
                                String $folder = '__null__',
                                String $playlist = '__null__');

    public function setID1006(String $id_1006 = '__null__');
    public function getID1006() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setFilename(String $filename = '__null__');
    public function getFilename() : String;
    public function setGenre(String $genre = '__null__');
    public function getGenre() : String;
    public function setArtist(String $artist = '__null__');
    public function getArtist() : String;
    public function setAlbum(String $album = '__null__');
    public function getAlbum() : String;
    public function setComposer(String $composer = '__null__');
    public function getComposer() : String;
    public function setPublisher(String $publisher = '__null__');
    public function getPublisher() : String;
    public function setKet(String $ket = '__null__');
    public function getKet() : String;
    public function setFavorited(String $favorited = '__null__');
    public function getFavorited() : String;
    public function setFolder(String $folder = '__null__');
    public function getFolder() : String;
    public function setPlaylist(String $playlist = '__null__');
    public function getPlaylist() : String;

}
?>
