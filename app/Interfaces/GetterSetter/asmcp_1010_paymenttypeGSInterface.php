<?php
namespace App\Interfaces;

interface asmcp_1010_paymenttypeGSInterface {

    public function __construct(String $id_1010 = '__null__',
                                String $id_1001 = '__null__',
                                int $type = 0);

    public function setID1010(String $id_1010 = '__null__');
    public function getID1010() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setType(int $type = 0);
    public function getType() : int;

}
?>
