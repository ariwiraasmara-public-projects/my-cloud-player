<?php
namespace App\Interfaces;

interface asmcp_1007_usersettingGSInterface {

    public function __construct(String $id_1007 = '__null__',
                                String $id_1001 = '__null__',
                                String $theme = '__null__',
                                String $text = '__null__',
                                String $bar = '__null__',
                                String $wall_img = '__null__',
                                String $wall_heigth = '__null__',
                                String $wall_width = '__null__',
                                String $wall_size = '__null__',
                                String $wall_position = '__null__',
                                String $wall_repeat = '__null__',
                                String $wall_attachment = '__null__');

    public function setID1007(String $id_1006 = '__null__');
    public function getID1007() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setTheme(String $theme = '__null__');
    public function getTheme() : String;
    public function setText(String $text = '__null__');
    public function getText() : String;
    public function setBar(String $bar = '__null__');
    public function getBar() : String;
    public function setWallimg(String $wall_img = '__null__');
    public function getWallimg() : String;
    public function setWallheight(int $wall_height = 0);
    public function getWallheight() : int;
    public function setWallwidth(int $wall_width = 0);
    public function getWallwidth() : int;
    public function setWallsize(int $wall_size = 0);
    public function getWallsize() : int;
    public function setWallposition(String $wall_position = '__null__');
    public function getWallposition() : String;
    public function setWallrepeat(String $wall_repeat = '__null__');
    public function getWallrepeat() : String;
    public function setWallattachment(String $wall_attachment = '__null__');
    public function getWallattachment() : String;

}
?>
