<?php
namespace App\Interfaces;

interface asmcp_1001_useridGSInterface {

    public function __construct(String $id_1001 = '__null__',
                                String $nama = '__null__',
                                String $tempat_lahir = '__null__',
                                String $tgl_lahir = '__null__',
                                String $alamat = '__null__',
                                int $id = 0);

    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setID(int $id = null);
    public function getID() :int;
    public function setNama(String $nama = '__null__');
    public function getNama() : String;
    public function setTempatLahir(String $tempat_lahir = '__null__');
    public function getTempatLahir() : String;
    public function setTglLahir(String $tgl_lahir = null);
    public function getTglLahir() : String;
    public function setAlamat(String $alamat = '__null__');
    public function getAlamat() : String;

}
?>
