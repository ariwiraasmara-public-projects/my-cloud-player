<?php
namespace App\Interfaces;

interface asmcp_1012_subscriptionGSInterface {

    public function __construct(String $id_1012 = '__null__',
                                String $id_1010 = '__null__',
                                int $is_active = 0,
                                int $is_trial = 0,
                                String $datepay = null,
                                int $amount = 0,
                                String $tilldate = null);

    public function setID1012(String $id_1010 = '__null__');
    public function getID1012() : String;
    public function setID1010(String $id_1001 = '__null__');
    public function getID1010() : String;
    public function setIsactive(int $is_active = 0);
    public function getIsactive() : int;
    public function setIstrial(int $is_trial = 0);
    public function getIstrial() : int;
    public function setDatepay(String $datepay = null);
    public function getDatepay() : String;
    public function setAmount(int $amount = 0);
    public function getAmount() : int;
    public function setTilldate(String $tilldate = null);
    public function getTilldate() : String;

}
?>
