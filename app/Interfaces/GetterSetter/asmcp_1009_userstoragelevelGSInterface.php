<?php
namespace App\Interfaces;

interface asmcp_1009_userstoragelevelGSInterface {

    public function __construct(String $id_1009 = '__null__',
                                String $id_1001 = '__null__',
                                int $level = 0,
                                String $upgraded_lvl2_at = null,
                                String $upgraded_lvl3_at = null,
                                String $upgraded_lvl4_at = null,
                                String $upgraded_lvl5_at = null,
                                String $upgraded_lvl6_at = null,
                                String $upgraded_lvl7_at = null,
                                String $upgraded_lvl8_at = null,
                                String $upgraded_lvl9_at = null);

    public function setID1009(String $id_1009 = '__null__');
    public function getID1009() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : int;
    public function setLevel(int $memory = 0);
    public function getLevel() : int;
    public function setLevel2At(String $upgraded_lvl2_at = null);
    public function getLevel2At() : int;
    public function setLevel3At(String $upgraded_lvl3_at = null);
    public function getLevel3At() : int;
    public function setLevel4At(String $upgraded_lvl4_at = null);
    public function getLevel4At() : int;
    public function setLevel5At(String $upgraded_lvl5_at = null);
    public function getLevel5At() : int;
    public function setLevel6At(String $upgraded_lvl6_at = null);
    public function getLevel6At() : int;
    public function setLevel7At(String $upgraded_lvl7_at = null);
    public function getLevel7At() : int;
    public function setLevel8At(String $upgraded_lvl8_at = null);
    public function getLevel8At() : int;
    public function setLevel9At(String $upgraded_lvl9_at = null);
    public function getLevel9At() : int;

}
?>
