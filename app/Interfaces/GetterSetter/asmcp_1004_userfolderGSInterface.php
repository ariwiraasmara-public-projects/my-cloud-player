<?php
namespace App\Interfaces;

interface asmcp_1004_userfolderGSInterface {

    public function __construct(String $id_1004,
                                String $id_1001,
                                String $foldername,
                                String $ket);

    public function setID1004(String $id_1004 = '__null__');
    public function getID1004() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setFoldername(String $foldername = '__null__');
    public function getFoldername() : String;
    public function setKet(String $ket = '__null__');
    public function getKet() : String;

}
?>
