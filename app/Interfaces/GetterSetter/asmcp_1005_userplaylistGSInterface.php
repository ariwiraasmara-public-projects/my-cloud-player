<?php
namespace App\Interfaces;

interface asmcp_1005_userplaylistGSInterface {

    public function __construct(String $id_1005,
                                String $id_1001,
                                String $playlist);

    public function setID1005(String $id_1005 = '__null__');
    public function getID1005() : String;
    public function setID1001(String $id_1001 = '__null__');
    public function getID1001() : String;
    public function setPlaylist(String $playlist = '__null__');
    public function getPlaylist() : String;

}
?>
