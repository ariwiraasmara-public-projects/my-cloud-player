<?php

namespace App\Http\Controllers;

use App\Models\Error_Reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ErrorReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Error_Reports $error_Reports)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Error_Reports $error_Reports)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Error_Reports $error_Reports)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Error_Reports $error_Reports)
    {
        //
    }
}
