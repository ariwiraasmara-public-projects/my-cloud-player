<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Redirect;
use File;

use App\Services\UserService;
use App\Services\asmcp_1001_useridService;
use App\Services\asmcp_1002_userdirService;
use App\Services\asmcp_1003_usersecurityService;
use App\Services\asmcp_1004_userfolderService;
use App\Services\asmcp_1005_userplaylistService;
use App\Services\asmcp_1006_userfileService;
use App\Services\asmcp_1007_usersettingService;
use App\Services\asmcp_1009_userstoragelevelService;
use App\Services\asmcp_1010_paymenttypeService;
use App\Services\asmcp_1011_onetimepaymentService;
use App\Services\asmcp_1012_subscriptionService;
use App\Services\asmcp_1013_usernotificationService;

use App\Http\Requests\Storeuser as req_storeuser;
use App\Http\Requests\Updateuser as req_updateuser;
use App\Http\Resources\UserResource as res_userresource;
use App\Http\Resources\UserCollection as res_usercollection;

use App\Http\Requests\Storeasmcp_1000_userRequest;
use App\Http\Requests\Updateasmcp_1000_userRequest;
use App\Http\Resources\asmcp_1000_userResource;
use App\Http\Resources\asmcp_1000_userCollection;

use App\Http\Requests\Storeasmcp_1001_useridRequest;
use App\Http\Requests\Updateasmcp_1001_useridRequest;
use App\Http\Resources\asmcp_1001_useridResource;
use App\Http\Resources\asmcp_1001_useridCollection;

use App\Http\Requests\Storeasmcp_1002_userdirRequest;
use App\Http\Requests\Updateasmcp_1002_userdirRequest;
use App\Http\Resources\asmcp_1002_userdirResource;
use App\Http\Resources\asmcp_1002_userdirCollection;

use App\Http\Requests\Storeasmcp_1003_usersecurityRequest;
use App\Http\Requests\Updateasmcp_1003_usersecurityRequest;
use App\Http\Resources\asmcp_1003_usersecurityResource;
use App\Http\Resources\asmcp_1003_usersecurityCollection;

use App\Http\Requests\Storeasmcp_1004_userfoldersRequest;
use App\Http\Requests\Updateasmcp_1004_userfoldersRequest;
use App\Http\Resources\asmcp_1004_userfoldersResource;
use App\Http\Resources\asmcp_1004_userfoldersCollection;

use App\Http\Requests\Storeasmcp_1005_userplaylistsRequest;
use App\Http\Requests\Updateasmcp_1005_userplaylistsRequest;
use App\Http\Resources\asmcp_1005_userplaylistsResource;
use App\Http\Resources\asmcp_1005_userplaylistsCollection;

use App\Http\Requests\Storeasmcp_1006_userfilesRequest;
use App\Http\Requests\Updateasmcp_1006_userfilesRequest;
use App\Http\Resources\asmcp_1006_userfilesResource;
use App\Http\Resources\asmcp_1006_userfilesCollection;

use App\Http\Requests\Storeasmcp_1007_usersettingRequest;
use App\Http\Requests\Updateasmcp_1007_usersettingRequest;
use App\Http\Resources\asmcp_1007_usersettingResource;
use App\Http\Resources\asmcp_1007_usersettingCollection;

use App\Http\Requests\Storeasmcp_1009_userstoragelevelRequest;
use App\Http\Requests\Updateasmcp_1009_userstoragelevelRequest;
use App\Http\Resources\asmcp_1009_userstoragelevelResource;
use App\Http\Resources\asmcp_1009_userstoragelevelCollection;

use App\Libraries\myfunction;
use App\Libraries\jsr;
use App\MyLibs\generateid;
use App\MyLibs\crud;
use App\MyLibs\auth as mcrauth;

class APIController extends Controller {
    //

    // public static $fun;
    protected $cfun;
    protected $cjsr;
    protected $muser;
    protected $m1001;
    protected $m1002;
    protected $m1003;
    protected $m1004;
    protected $m1005;
    protected $m1006;
    protected $m1007;
    protected $m1009;
    protected $m1010;
    protected $m1011;
    protected $m1012;
    protected $m1013;

    public function __construct(protected myfunction $fun,
                                protected jsr $jsr,
                                protected UserService $user,
                                protected asmcp_1001_useridService $asmcp1001,
                                protected asmcp_1002_userdirService $asmcp1002,
                                protected asmcp_1003_usersecurityService $asmcp1003,
                                protected asmcp_1004_userfolderService $asmcp1004,
                                protected asmcp_1005_userplaylistService $asmcp1005,
                                protected asmcp_1006_userfileService $asmcp1006,
                                protected asmcp_1007_usersettingService $asmcp1007,
                                protected asmcp_1009_userstoragelevelService $asmcp1009,
                                protected asmcp_1010_paymenttypeService $asmcp1010,
                                protected asmcp_1011_onetimepaymentService $asmcp1011,
                                protected asmcp_1012_subscriptionService $asmcp1012,
                                protected asmcp_1013_usernotificationService $asmcp1013) {
        // set_time_limit(8000000);
        date_default_timezone_set("Asia/Jakarta");
        $this->cfun  = $fun;
        $this->cjsr  = $jsr;
        $this->muser = $user;
        $this->m1001 = $asmcp1001;
        $this->m1002 = $asmcp1002;
        $this->m1003 = $asmcp1003;
        $this->m1004 = $asmcp1004;
        $this->m1005 = $asmcp1005;
        $this->m1006 = $asmcp1006;
        $this->m1007 = $asmcp1007;
        $this->m1009 = $asmcp1009;
        $this->m1010 = $asmcp1010;
        $this->m1011 = $asmcp1011;
        $this->m1012 = $asmcp1012;
        $this->m1013 = $asmcp1013;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function coba() {
        // return response()->json(['msg' => 'Coba Success!', 'success' => 1], 200);
        return $this->cjsr->r(['msg' => 'Coba Success!', 'success' => 1], 'ok');
     }

    // BEFORE LOGIN {
        public function login(Request $request) {
            // set_time_limit(0);
            $validated = $request->validate([
                'user'      => 'required|string',
                'pass'      => 'required|string',
            ]);

            if(!$validated) {
                return new JsonResponse(
                    ['msg' => 'You\'re Not Validated', 'error' => 1],
                    status: JsonResponse::HTTP_BAD_REQUEST,
                );
            }

            $data = $this->muser->login($request->user, $request->pass);

            if($data->get('success')) {
                $dafi = array(
                    "mcr_x_aswq_1"     => $this->cfun->encrypt($data['success'][0]['id']),
                    "mcr_x_aswq_2"     => $this->cfun->encrypt($data['success'][0]['id_1001']),
                    "mcr_x_aswq_3"     => $this->cfun->encrypt($data['success'][0]['username']),
                    "mcr_x_aswq_4"     => $this->cfun->encrypt($data['success'][0]['email']),
                    "mcr_x_aswq_5"     => $this->cfun->encrypt($data['success'][0]['remember_token']),
                    "theme"            => $data['success'][0]['theme'],
                    "text"             => $data['success'][0]['text'],
                    "bar"              => $data['success'][0]['bar'],
                    "wall_img"         => $data['success'][0]['wall_img'],
                    "wall_heigth"      => $data['success'][0]['wall_heigth'],
                    "wall_width"       => $data['success'][0]['wall_width'],
                    "wall_size"        => $data['success'][0]['wall_size'],
                    "wall_position"    => $data['success'][0]['wall_position'],
                    "wall_repeat"      => $data['success'][0]['wall_repeat'],
                    "wall_attachment"  => $data['success'][0]['wall_attachment'],
                );

                $this->cfun->setCookie([
                    'islogin'      => 1,
                    "mcr_x_aswq_1" => $data['success'][0]['id'],
                    "mcr_x_aswq_2" => $data['success'][0]['id_1001'],
                    "mcr_x_aswq_3" => $data['success'][0]['username'],
                    "mcr_x_aswq_4" => $data['success'][0]['email'],
                    "mcr_x_aswq_5" => $data['success'][0]['remember_token'],
                ], true, 1, 24, 60, 60);

                $this->cfun->setCookie([
                    "theme"            => $data['success'][0]['theme'],
                    "text"             => $data['success'][0]['text'],
                    "bar"              => $data['success'][0]['bar'],
                    "wall_img"         => $data['success'][0]['wall_img'],
                    "wall_heigth"      => $data['success'][0]['wall_heigth'],
                    "wall_width"       => $data['success'][0]['wall_width'],
                    "wall_size"        => $data['success'][0]['wall_size'],
                    "wall_position"    => $data['success'][0]['wall_position'],
                    "wall_repeat"      => $data['success'][0]['wall_repeat'],
                    "wall_attachment"  => $data['success'][0]['wall_attachment'],
                ], false, 1, 24, 60, 60);

                // $this->cfun->setOneCookie('islogin', 1, 1);
                // $this->cfun->setOneCookie('theme', 1, 1);
                // $this->cfun->setOneCookie('text', 1, 1);
                // $this->cfun->setOneCookie('bar', 1, 1);
                // $this->cfun->setOneCookie('wall_img', 1, 1);
                // $this->cfun->setOneCookie('wall_heigth', 1, 1);
                // $this->cfun->setOneCookie('wall_width', 1, 1);
                // $this->cfun->setOneCookie('wall_size', 1, 1);
                // $this->cfun->setOneCookie('wall_position', 1, 1);
                // $this->cfun->setOneCookie('wall_repeat', 1, 1);
                // $this->cfun->setOneCookie('wall_attachment', 1, 1);

                return new JsonResponse(
                    ['msg'=>'You\'re Login!', 'success'=>1, 'data'=>$dafi, 'login_at'=>date('Y-m-d h:i:s')],
                    status: JsonResponse::HTTP_OK,
                );
            }

            return match($data->get('error')){
                1 => new JsonResponse(
                    ['msg' => 'Wrong Username / Email', 'error'=> 1],
                    status: JsonResponse::HTTP_BAD_REQUEST,
                ),
                2 => new JsonResponse(
                    ['msg' => 'Wrong Password!', 'error'=> 2],
                    status: JsonResponse::HTTP_BAD_REQUEST,
                ),
                default => new JsonResponse(
                    ['msg' => 'Something\'s Wrong!', 'error'=> -1],
                    status: JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                )
            };
        }

        public function saveuser(Request $request) {
            try {
                $validated = $request->validate([
                    'username'  => 'required|max:255',
                    'email'     => 'required|max:255',
                    'password'  => 'required',
                ]);

                $res = $this->muser->store([
                    'username'  => $request->username,
                    'email'     => $request->email,
                    'password'  => $request->password
                ]);

                return match($res['error']) {
                    1 => new JsonResponse(
                        ['msg' => 'Register Failed! Can not Save!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    ),
                    2 => new JsonResponse(
                        ['msg' => 'Register Failed! Duplicate Username!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Register Success!', 'success' => 1, 'data' => $res],
                        status: JsonResponse::HTTP_OK,
                    ),
                };
            }
            catch(Exception $e) {
                return new JsonResponse(
                    ['msg' => 'Register Failed! Something\'s Wrong!', 'error' => -1],
                    status: JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                );
            }
        }
    // } BEFORE LOGIN

    // AFTER LOGIN {
        public function logout() {
            $this->cfun->setCookieOff('islogin');
            $this->cfun->setCookieOff('mcr_x_aswq_1');
            $this->cfun->setCookieOff('mcr_x_aswq_2');
            $this->cfun->setCookieOff('mcr_x_aswq_3');
            $this->cfun->setCookieOff('mcr_x_aswq_4');
            $this->cfun->setCookieOff('mcr_x_aswq_5');
            return new JsonResponse(
                ['msg' => 'You\'re Logout! ', 'success' => 1],
                status: JsonResponse::HTTP_OK,
            );
        }

        // USER PROFILE {
            public function getUserProfile($id) {
                $id = $this->fun->denval($id, true);
                return new JsonResponse(
                    ['msg'      => 'Your Profile!',
                     'success'  => 1,
                     'data'     => $this->muser->getUserProfile(array("users.id"=>$id))],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function getUserEditProfile($id) {
                $id = $this->fun->denval($id, true);
                return new JsonResponse(
                    ['msg'      => 'For Your Edit Profile!',
                     'success'  => 1,
                     'data'     => $this->muser->getUserEditProfile(array("users.id"=>$id))],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function updateUserProfile(Request $request, $id) {
                $denid = $this->cfun->denval($id, true);

                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Update Profile Fail! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $validated = $request->validate([
                    'field' => 'required|string|max:100',
                    'value'   => 'required',
                ]);

                $res = $this->user->update([
                    $request->field => $request->value,
                ], $denid, $request->type);

                if($res) {
                    return new JsonResponse(
                        ['msg'=>'Update Profile Success!', 'success'=>1, 'data'=>$res, 'update_at'=>date('Y-m-d h:i:s')],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg'=>'Update Profile Fail! Can\'t Save The Data', 'error'=>2],
                    status: JsonResponse::HTTP_OK,
                );
            }
        // }

        // USER SETTING {
            public function getUserSetting($id) {
                $denid = $this->cfun->denval($id, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Fetching You\'re Setting! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $data = $this->m1007->get($denid);
                return new JsonResponse(
                    ['msg' => 'For Your Edit Profile!', 'success' => 1, 'data'=>$data],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function updateUserSetting(Request $request, $id) {
                $denid = $this->cfun->denval($id, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Update Setting Fail! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $res = $this->m1007->update([
                    "theme"           => $request->theme,
                    "text"            => $request->text,
                    "bar"             => $request->bar,
                    "wall_img"        => $request->wall_img,
                    "wall_height"     => $request->wall_height,
                    "wall_width"      => $request->wall_width,
                    "wall_size"       => $request->wall_size,
                    "wall_position"   => $request->wall_position,
                    "wall_repeat"     => $request->wall_repeat,
                    "wall_attachment" => $request->wall_attachment
                ], $denid);
                // return $res;
                if($res) {
                    $this->cfun->setCookie([
                        "theme"           => $request->theme,
                        "text"            => $request->text,
                        "bar"             => $request->bar,
                        "wall_img"        => $request->wall_img,
                        "wall_height"     => $request->wall_height,
                        "wall_width"      => $request->wall_width,
                        "wall_size"       => $request->wall_size,
                        "wall_position"   => $request->wall_position,
                        "wall_repeat"     => $request->wall_repeat,
                        "wall_attachment" => $request->wall_attachment
                    ], false, 1, 24, 60, 60);

                    return new JsonResponse(
                        ['msg' => 'Update Setting Success!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg' => 'Update Setting Fail! Can\'t Save The Data!', 'error' => 1, 'data'=>$res],
                    status: JsonResponse::HTTP_BAD_REQUEST,
                );
            }
        // }

        // PLAYER {
            public function getPlayer() {

            }

            public function getFFL() {

            }

            public function getMusicList() {

            }
        // } PLAYER

        // USER FOLDER {
            public function getFolder($id, $order, $by) {
                $denid = $this->cfun->denval($id, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Fetching You\'re Folder! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $data = $this->m1004->getAll($denid, $order, $by);
                if($data) {
                    return new JsonResponse(
                        ['msg' => 'Your Folders!', 'success' => 1, 'empty' => 0, 'data'=>$data],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg' => 'Your Folders!', 'success' => 1, 'empty' => 1, 'data'=>$data],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function createFolder(Request $request, $id) {
                $denid = $this->cfun->denval($id, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Creating You\'re Folder! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $validated = $request->validate([
                    'foldername' => 'required|max:255',
                ]);

                return match($this->m1004->store([
                    'id_1004'    => $this->m1004->getID($denid),
                    'id_1001'    => $denid,
                    'foldername' => $request->foldername,
                    'ket'        => $request->ket,
                ])) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Create You\'re Folder!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Creating You\'re Folder!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }

            public function updateFolder(Request $request, $id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Updating You\'re Folder! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $validated = $request->validate([
                    'foldername' => 'required|max:255',
                ]);

                return match($this->m1004->update([
                    'foldername' => $request->foldername,
                    'ket'        => $request->ket
                ], $id)) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Update You\'re Folder!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Updating You\'re Folder!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }

            public function deleteFolder($id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Deleting You\'re Folder! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                return match($this->m1004->delete($id)) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Delete You\'re Folder!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Deleting You\'re Folder!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }
        // }

        // USER PLAYLIST {
            public function getPlaylist($id, $by) {
                $denid = $this->cfun->denval($id, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Fetching You\'re Playlist! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $data = $this->m1005->getAll($denid, $by);
                // return $data;
                if($data) {
                    return new JsonResponse(
                        ['msg' => 'Your Playlists!', 'success' => 1, 'empty' => 0, 'data'=>$data],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg' => 'Your Playlists!', 'success' => 1, 'empty' => 1, 'data'=>$data],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function getDetailPlaylist() {

            }


            public function createPlaylist(Request $request, $id) {
                $denid = $this->cfun->denval($id, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Creating You\'re Playlist! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $validated = $request->validate([
                    'playlist' => 'required|max:255',
                ]);

                return match($this->m1005->store([
                    'id_1005'  => $this->m1005->getID($denid),
                    'id_1004'  => $denid,
                    'playlist' => $request->playlist
                ])) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Create You\'re Playlist!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Creating You\'re Playlist!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }

            public function updatePlaylist($id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Updating You\'re Playlist! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $validated = $request->validate([
                    'playlist' => 'required|max:255',
                ]);

                return match($this->m1005->update($request->playlist, $id)) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Update You\'re Playlist!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Updating You\'re Playlist!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }

            public function deletePlaylist($id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Deleting You\'re Playlist! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                return match($this->m1005->delete($id)) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Delete You\'re Playlist!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Deleting You\'re Playlist!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }
        // }

        // USER FILE {
            protected function checkFolderOrPlaylist($fv = null) {
                if(asmcp_1004_userfolders::where('id_1004', '=', $request->fv)->count() > 0) {
                    $df = asmcp_1004_userfolders::where('id_1004', '=', $request->fv)->get();
                    $fv = $df[0]['foldername'];
                }

                if(asmcp_1005_userplaylists::where('id_1005', '=', $request->fv)->count() > 0) {
                    $df = asmcp_1005_userplaylists::where('id_1005', '=', $request->fv)->get();
                    $fv = $df[0]['playlist'];
                }

                return $fv;
            }

            public function getFile($id1, $id2, $order, $by, $fp) {
                $denid = $this->cfun->denval($id1, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Fetching You\'re Files! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                if($id2 == 'root') $id2 = null;
                $data = $this->m1006->getAll($denid, $id2, $order, $by, $fp);
                // return $data;
                if($data) {
                    return new JsonResponse(
                        ['msg' => 'You\'re Files in '.$fp.'!', 'success' => 1, 'is_empty' => 0, 'data' => $data],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg' => 'You\'re Files in '.$fp.'!', 'success' => 1, 'is_empty' => 1, 'data' => $data],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function getFileInFolderOrPlaylist(Request $request) {
                $fv = 0; $fov = 0;
                $validated = $request->validate([
                    'fv' => 'required|string|max:255',
                ]);

                if(asmcp_1004_userfolders::where('id_1004', '=', $request->fv)->count() > 0) {
                    $df  = asmcp_1004_userfolders::where('id_1004', '=', $request->fv)->get();
                    $fv  = $df[0]['foldername'];
                    $fov = 'folder';
                }

                if(asmcp_1005_userplaylists::where('id_1005', '=', $request->fv)->count() > 0) {
                    $df  = asmcp_1005_userplaylists::where('id_1005', '=', $request->fv)->get();
                    $fv  = $df[0]['playlist'];
                    $fov = 'playlist';
                }

                if($fv > 0) return response()->json(['msg' => 'Your Files in '.$fov, 'data'=>$fv], 200);
                return response()->json(['msg' => 'Your Files is not in folder or playlist ', 'data'=>$fv], 200);
            }

            public function getDetailFile($id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Fetching You\'re Detail File! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $res = $this->m1006->get($id);
                if($res) {
                    return new JsonResponse(
                        ['msg' => 'You\'re Detail File!', 'success' => 1, 'data' => $data],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg' => 'You\'re File Did not Exist!', 'error' => 2],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function userUploadFile(Request $request, $id) {
                $denid = $this->cfun->denval($id1, true);
                if(is_null($denid) || empty($denid) || $denid == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Uploading You\'re Files! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $validated = $request->validate([
                    'filename' => 'required',
                ]);

                // For upload file
                $res = $this->m1006->store([
                    'id_1006'   => $this->m1006->getID($denid),
                    "id_1001"   => $denid,
                    "filename"  => $request->file
                ]);

                if($res) {
                    // moving files from folder1 (source folder) to folder2 (folder destination)
                    // =>
                    return new JsonResponse(
                        ['msg' => 'Success Upload You\'re Music File!', 'success' => 1, 'data' => $data],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg' => 'Fail to Upload You\'re Music File!', 'error' => 2],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function moveFile(Request $request, $id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Moving You\'re Music File! Unauthorized!', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                $validated = $request->validate([
                    'folder1'    => 'required',
                    'folder2'    => 'required',
                ]);

                $res = $this->m1006->update(['folder' => $folder2], $id);
                if($res) {
                    // moving files from folder1 (source folder) to folder2 (folder destination)
                    // =>
                    return new JsonResponse(
                        ['msg' => 'Success Moving You\'re Music File!', 'success' => 1, 'data' => $data],
                        status: JsonResponse::HTTP_OK,
                    );
                }

                return new JsonResponse(
                    ['msg' => 'Fail to Moving You\'re Music File!', 'error' => 2],
                    status: JsonResponse::HTTP_OK,
                );
            }

            public function userUpdateFile(Request $request, $id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Updating You\'re Music File! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                return match($this->m1006->updateAll([
                    "genre"     => $request->genre,
                    "artist"    => $request->artist,
                    "album"     => $request->album,
                    "composer"  => $request->composer,
                    "publisher" => $request->publisher,
                    "ket"       => $request->ket
                ], $request->id)) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Update You\'re Music File!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Updating You\'re Music File!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }

            public function userDeleteFile($id) {
                if(is_null($id) || empty($id) || $id == '') {
                    return new JsonResponse(
                        ['msg' => 'Fail Deleting You\'re Music File! Unauthorized', 'error' => 1],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    );
                }

                return match($this->m1006->delete($request->id)) {
                    1 => new JsonResponse(
                        ['msg' => 'Success Delete You\'re Music File!', 'success' => 1],
                        status: JsonResponse::HTTP_OK,
                    ),
                    default => new JsonResponse(
                        ['msg' => 'Fail Deleting You\'re Music File!', 'error' => 2],
                        status: JsonResponse::HTTP_BAD_REQUEST,
                    )
                };
            }
        // }
    // } AFTER LOGIN
}
