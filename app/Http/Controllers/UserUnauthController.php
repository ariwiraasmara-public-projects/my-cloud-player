<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;
use Illuminate\Routing\Redirector;
use Inertia\Inertia;
use Redirect;
use Response;
use Cookie;
use URL;
use Meta;

use App\Models\User;
use App\Models\Storeuser;
use App\Models\Updateuser;

use App\Http\Requests\Storeuser as req_storeuser;
use App\Http\Requests\Updateuser as req_updateuser;

use App\Models\asmcp_1000_user;
use App\Http\Requests\Storeasmcp_1000_userRequest;
use App\Http\Requests\Updateasmcp_1000_userRequest;

use App\Models\asmcp_1001_userid;
use App\Http\Requests\Storeasmcp_1001_useridRequest;
use App\Http\Requests\Updateasmcp_1001_useridRequest;

use App\Models\asmcp_1002_userdir;
use App\Http\Requests\Storeasmcp_1002_userdirRequest;
use App\Http\Requests\Updateasmcp_1002_userdirRequest;

use App\Models\asmcp_1003_usersecurity;
use App\Http\Requests\Storeasmcp_1003_usersecurityRequest;
use App\Http\Requests\Updateasmcp_1003_usersecurityRequest;

use App\Models\asmcp_1004_userfolders;
use App\Http\Requests\Storeasmcp_1004_userfoldersRequest;
use App\Http\Requests\Updateasmcp_1004_userfoldersRequest;

use App\Models\asmcp_1005_userplaylists;
use App\Http\Requests\Storeasmcp_1005_userplaylistsRequest;
use App\Http\Requests\Updateasmcp_1005_userplaylistsRequest;

use App\Models\asmcp_1006_userfiles;
use App\Http\Requests\Storeasmcp_1006_userfilesRequest;
use App\Http\Requests\Updateasmcp_1006_userfilesRequest;

use App\Models\asmcp_1007_usersetting;
use App\Http\Requests\Storeasmcp_1007_usersettingRequest;
use App\Http\Requests\Updateasmcp_1007_usersettingRequest;

use App\MyLibs\auth as mcrauth;
use App\Libraries\myfunction;
use App\MyLibs\generateid;
use App\MyLibs\crud;

class UserUnauthController extends Controller {
    //

    public function __construct(protected myfunction $fun) {
        // set_time_limit(8000000);
        $this->fun = new myfunction;
    }

    public function meta(String $title = null, String $description = null, String $page = null) {
        meta()->set('csrf-token', csrf_token())
              ->set('charset', 'utf-8')
              ->title($title)->set('og:title', $title)
              ->description($description)
              ->set('og:description', $description)
              ->keywords('my cloud player '.$page.',
                          your own media music player online '.$page.',
                          private music player '.$page.',
                          private online music player '.$page.',
                          private offline music player '.$page.',
                          private online offline music player $page,
                          storage music player '.$page.',
                          cache music player '.$page.',
                          local music player '.$page.',
                          portable music player '.$page.',
                          free 4GB online storage music player '.$page)
              ->canonical(URL::current())
              ->robots('all')
              ->viewport('initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no')
              ->set('apple-mobile-web-app-capable', 'yes')
              ->set('apple-touch-fullscreen', 'yes')
              ->setRawTag('<link rel="preconnect" href="https://fonts.googleapis.com" />')
              ->setRawTag('<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />')
              ->setRawTag('<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif+Georgian:wght@100;400;900&family=Nunito:wght@400;700&display=swap">')
              ->setRawTag('<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>')
              ->setRawTag('<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>')
              ->setRawTag('<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login() {
        if( isset($_COOKIE['islogin']) ) return Redirect::to('/player');
        $this->meta(
            'Login',
            'Login | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are',
            'login'
        );
        return Inertia::render('1/login', [
            'title'              => 'Your Very Own Music Player',
            'footer'             => 1,
            'link_aboutus'       => '/p/aboutus',
            'link_levelup'       => '/p/storage_levelup',
            'link_forgotpassword'=> '/p/password/forgot'
        ]);
    }

    public function signin() {
        if( isset($_COOKIE['islogin']) ) return Redirect::to('/player');
        $this->meta(
            'Register',
            'Register | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are',
            'register'
        );
        return Inertia::render('1/register', [
            'title'      => 'Sign In',
            'footer'     => 1,
            'link_login' => '/p/login'
        ]);
    }

    public function aboutus() {
        if( isset($_COOKIE['islogin']) ) return Redirect::to('/player');
        $this->meta(
            'What We Are',
            'What We Are | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are',
            'about us'
        );
        return Inertia::render('1/about_us', [
            'title'      => 'What We Are',
            'footer'     => 1,
            'link_login' => '/p/login'
        ]);
    }

    public function forgotpassword() {
        if( isset($_COOKIE['islogin']) ) return Redirect::to('/player');
        $this->meta(
            'I\'ve Forgotten it!',
            'I\'ve Forgotten it! | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are',
            'forgot password'
        );
        return Inertia::render('1/forgot_password', [
            'title'      => 'What We Are',
            'footer'     => 1,
            'link_login' => '/p/login'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request) {
        // $where = array('asmcp_1000_user.id' => myfunction::getCookie('id'));
        // $query = asmcp_1000_user::join('asmcp_1001_userid', 'asmcp_1000_user.id', '=','asmcp_1001_userid.id')
        //                             ->join('asmcp_1009_userstoragelevel', 'asmcp_1001_userid.id_1001', '=', 'asmcp_1009_userstoragelevel.id_1001')
        //                             ->where($where)
        //                             ->select('asmcp_1000_user.username as username', 'asmcp_1000_user.email as email',
        //                                     'asmcp_1000_user.tlp as tlp', 'asmcp_1000_user.pin as pin',
        //                                     'asmcp_1001_userid.nama as nama', 'asmcp_1001_userid.tgl_lahir as tgl',
        //                                     'asmcp_1009_userstoragelevel.level as level')
        //                             ->get();

        // return $query[0]['username'];
        // Get the currently authenticated user...
        //$response = Http::get('http://localhost/@gitlab/my-cloud-player/public/api/user/2'); // Http::get('https://jsonplaceholder.typicode.com/posts');

        // $response = Http::post('https://jsonplaceholder.typicode.com/posts',
        //                     ['title' => 'Tsdsdsdm',
        //                      'body'  => 'ggg',]);

        // $response = Http::put('https://jsonplaceholder.typicode.com/posts/1',
        //                     ['title' => 'This is test from ItSolutionStuff.com',
        //                      'body'  => 'This is test from ItSolutionStuff.com as body',]);

        // $response = Http::delete('https://jsonplaceholder.typicode.com/posts/1');
        //
        // dd($jsonData);

        //return $response->json();

        //$response = Http::get(URL::to('/api/alluser'));
        //$jsonData = $response->json(); ("Hello World!")
        // echo myfunction::toMD5('abc123');
        // echo '<br/>';
        // echo myfunction::toMD5('aaa');
        // echo '<br/>';
        // echo myfunction::toMD5('ariwiraasmara');
        // echo '<br/>';
        // echo Crypt::decryptString('eyJpdiI6InlncEdRa3Y4dE53SWRCY0lKeG1HUWc9PSIsInZhbHVlIjoiVlBnYzlLN3pjS2xNUk1aZnRtejFzZz09IiwibWFjIjoiMGEzMDNmNGI5OGY4NmVjMGM0NDg1OWM0Y2I2MWQ3YzJhY2M2YjQzYjQwMzUwZTU4YTQ5NWYzMTgyZjBhMDJlMiIsInRhZyI6IiJ9
        // ');
        // echo '<br/>';
        // echo '<br/>';

        // $data = Http::withHeaders(['mcr-x-aswq'=>'mC12x@12!5Of!a', 'accept'=>'application/json'])
        //             ->get(URL::to('/api/user/edit/get/'.myfunction::getCookie('mcr-x-aswq-1')));

        $data = Http::post(URL::to('/api/login'),
                            ['user' => 'ari',
                            'pass'  => 'asd']);
        // $jdt    = $request->query('response', $response->json());
        // $status = $response->getStatusCode();

        //$cookie = myfunction::getCookie('coba');
        // echo 'id cookie : '.myfunction::getCookie('id');
        // echo '<br/>';
        // echo 'id1001 cookie : '.myfunction::getCookie('id1001');
        // echo '<br/>';
        // echo 'username cookie : '.myfunction::getCookie('username');
        // echo '<br/>';
        // echo 'email cookie : '.myfunction::getCookie('email');
        // echo '<br/>';
        // echo 'id session : '.$request->session()->get('id');
        // echo '<br/>';
        // echo 'id1001 session : '.$request->session()->get('id1001');
        // echo '<br/>';
        // echo 'username session : '.$request->session()->get('username');
        // echo '<br/>';
        // echo 'email session : '.$request->session()->get('email');
        //echo mcrauth::mcr(2);
        return $data;
    }

    public function loginprocess(Request $request) {
        set_time_limit(0);
        $validated = $request->validate([
            'user' => 'required|max:255',
            'pass' => 'required',
        ]);
        $response   = Http::post(URL::to('/api/login'),
                            ['user' => $request->user,
                             'pass' => $request->pass]);
        $jdt        = $request->query('response', $response->json());
        $status     = $response->getStatusCode();
        if($status == 201) {
            $upv        = $jdt['data']['upv'];
            $usv        = $jdt['data']['usv'];

            // COOKIE & SESSION {
                myfunction::setOneCookie('islogin', 1, 7);
                $x = 0;
                foreach($jdt['data'] as $dc) {
                    if($x < 5) {
                        myfunction::setOneCookie(mcrauth::cookieSeries()[$x], $dc, 7);
                        $x++;
                    }
                }

                // Store User Profile View Information {
                    $yc = 1;
                    foreach($jdt['data']['upv'] as $upv) {
                        myfunction::setOneCookie('mcp-upv-'.$yc, $upv);
                        $yc++;
                    }

                    // myfunction::setOneCookie('mcp-upv-1', ''); //
                    // myfunction::setOneCookie('mcp-upv-2', ''); //
                    // myfunction::setOneCookie('mcp-upv-3', ''); //
                    // myfunction::setOneCookie('mcp-upv-4', ''); //
                    // myfunction::setOneCookie('mcp-upv-5', ''); //
                // } Store User Profile View Information

                // Store User Setting View Setup and Information {
                    $zc = 1;
                    foreach($jdt['data']['usv'] as $usv) {
                        myfunction::setOneCookie('mcp-usv-'.$zc, $usv);
                        $zc++;
                    }

                    // myfunction::setOneCookie('mcp-usv-1', ''); //
                    // myfunction::setOneCookie('mcp-usv-2', ''); //
                    // myfunction::setOneCookie('mcp-usv-3', ''); //
                    // myfunction::setOneCookie('mcp-usv-4', ''); //
                    // myfunction::setOneCookie('mcp-usv-5', ''); //
                    // myfunction::setOneCookie('mcp-usv-6', ''); //
                    // myfunction::setOneCookie('mcp-usv-7', ''); //
                    // myfunction::setOneCookie('mcp-usv-8', ''); //
                    // myfunction::setOneCookie('mcp-usv-9', ''); //
                    // myfunction::setOneCookie('mcp-usv-10', ''); //
                // } Store User Setting View Setup and Information
            // } COOKIE & SESSION

            // return response()->json(['msg' => 'Login Success!', 'success' => 1, 'status'=>$status, 'place'=>'userunauth', 'data'=>$jdt['data']['data']], 200);
            return Redirect::to('/player')->with('msg','Login Success!');
        }
        // return response()->json(['msg' => 'Login Fail!', 'success' => 0, 'status'=>$status, 'place'=>'userunauth'], 500);
        // return $response;
        return Redirect::to('/')->with(['msg'=>'Login Fail! User or Password Incorrect!', 'typenotif'=>'is-danger']);
    }

    public function saveuser(req_storeuser $request, Storeuser $user) {
        $response = Http::post(URL::to('/api/user/save'),
                                    ['username' => $request->username,
                                    'email'     => $request->email,
                                    'password'  => $request->password]);
        $validated = $request->validate([
            'username'  => 'required|max:255',
            'email'     => 'required',
            'password'  => 'required',
        ]);
        $status = $response->getStatusCode();
        if($status == 201) return Redirect::to('/')->with(['msg'=>'Register Success!', 'typenotif'=>'is-success']);
        return Redirect::to('/')->with(['msg'=>'Fail To Register!', 'typenotif'=>'is-danger']);
        // if($status == 201) return Redirect::to('/')->with(['msg'=>'Register Success!', 'typenotif'=>'is-success']);
        // return Redirect::to('/')->with(['msg'=>'Fail To Register!', 'typenotif'=>'is-danger']);
    }

    public function forgotpasswordprocess(Request $request) {

    }

}
