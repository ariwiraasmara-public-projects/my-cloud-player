<?php
namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;
use Illuminate\Routing\Redirector;
use Inertia\Inertia;
use Redirect;
use Response;
use Cookie;
use URL;
use Meta;

use App\Models\User;
use App\Models\asmcp_1001_userid;
use App\Models\asmcp_1002_userdir;
use App\Models\asmcp_1003_usersecurity;
use App\Models\asmcp_1004_userfolders;
use App\Models\asmcp_1005_userfiles;
use App\Models\asmcp_1006_userplaylists;
use App\Models\asmcp_1007_usersetting;

use App\MyLibs\auth as mcrauth;
use App\Libraries\myfunction;
use App\MyLibs\generateid;
use App\MyLibs\crud;

class UserAuthController extends Controller {
    //

    protected $fun;
    public function __construct(myfunction $myfunction) {
        $this->fun = $myfunction;
    }

    public function index() {
        if( !isset($_COOKIE['islogin']) ) return Redirect::to('/login');
        $title = 'Dashboard';
        $description = 'Dashboard | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are';
        $page = 'dashboard';
        meta()->set('csrf-token', csrf_token())
              ->set('charset', 'utf-8')
              ->title($title)->set('og:title', $title)
              ->description($description)
              ->set('og:description', $description)
              ->keywords('my cloud player '.$page.',
                          your own media music player online '.$page.',
                          private music player '.$page.',
                          private online music player '.$page.',
                          private offline music player '.$page.',
                          private online offline music player $page,
                          storage music player '.$page.',
                          cache music player '.$page.',
                          local music player '.$page.',
                          portable music player '.$page.',
                          free 4GB online storage music player '.$page)
              ->canonical(URL::current())
              ->robots('all')
              ->viewport('initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no')
              ->set('apple-mobile-web-app-capable', 'yes')
              ->set('apple-touch-fullscreen', 'yes')
              ->setRawTag('<link rel="preconnect" href="https://fonts.googleapis.com" />')
              ->setRawTag('<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />')
              ->setRawTag('<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif+Georgian:wght@100;400;900&family=Nunito:wght@400;700&display=swap" />')
              ->setRawTag('<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>')
              ->setRawTag('<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>')
              ->setRawTag('<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>');

        $iconplayer = array("first"     => URL::to('assets/icons/player/first.svg'),
                            "previous"  => URL::to('assets/icons/player/previous.svg'),
                            "play"      => URL::to('assets/icons/player/play.svg'),
                            "next"      => URL::to('assets/icons/player/next.svg'),
                            "last"      => URL::to('assets/icons/player/last.svg'),
                            "reone"     => URL::to('assets/icons/player/reone.png'),
                            "relist"    => URL::to('assets/icons/player/relist.png'),
                            "reall"     => URL::to('assets/icons/player/reall.png'),
                            "reoff"     => URL::to('assets/icons/player/reoff.png'));

        // return $userprofile[0]['id_1001'];
        return Inertia::render('Dashboard',[
            // 'userprofile' => $userprofile,
            'iconplayer' => $iconplayer
        ]);
    }

    public function test() {
        return ['testvar' => 'test success'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(req_updateuser $request, $id) {
        //
        $validated = $request->validate([
            'formcardsave' => 'required',
        ]);
        $form  = fun::decrypt($request->formcardsave);
        $field = fun::decrypt($request->field);

        $response = Http::post(URL::to('/api/user/edit/save/'.$id),
                                    ['username' => $request->username,
                                    'email'     => $request->email,
                                    'password'  => $request->password]);
        $status = $response->getStatusCode();
        if($status == 201) return Redirect::to('/')->with('msg','Register Success!');
        // else return Redirect::to('/')->with('msg','Fail To Register!');

        return 'user updated request formcardsave: '.$form.' : '.$field;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        //
        return 'user deleted';
    }

}
