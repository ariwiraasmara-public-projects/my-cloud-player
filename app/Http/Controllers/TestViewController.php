<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Repositories\UserRepository;
use App\Libraries\myfunction;
use URL;
use Meta;

class TestViewController extends Controller {
    //
    public function __construct(protected UserRepository $user, protected myfunction $fun) {
        // $this->user = $user;
        $this->fun = new myfunction;
    }

    public function login() {
        // return $this->fun->getCookie('mcr-x-aswq-5');

        $title = 'Login Page';
        $description = 'Login Page | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are';
        $page = 'login page';
        meta()->set('csrf-token', csrf_token())
              ->set('charset', 'utf-8')
              ->title($title)->set('og:title', $title)
              ->description($description)
              ->set('og:description', $description)
              ->keywords('my cloud player '.$page.',
                          your own media music player online '.$page.',
                          private music player '.$page.',
                          private online music player '.$page.',
                          private offline music player '.$page.',
                          private online offline music player $page,
                          storage music player '.$page.',
                          cache music player '.$page.',
                          local music player '.$page.',
                          portable music player '.$page.',
                          free 4GB online storage music player '.$page)
              ->canonical(URL::current())
              ->robots('all')
              ->viewport('initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no')
              ->set('apple-mobile-web-app-capable', 'yes')
              ->set('apple-touch-fullscreen', 'yes')
              ->setRawTag('<link rel="preconnect" href="https://fonts.googleapis.com" />')
              ->setRawTag('<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />')
              ->setRawTag('<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif+Georgian:wght@100;400;900&family=Nunito:wght@400;700&display=swap">')
              ->setRawTag('<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>')
              ->setRawTag('<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>')
              ->setRawTag('<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>');
        return Inertia::render('1/login',
                                ['link_aboutus'=>'/test/aboutus',
                                 'link_levelup'=>'/test/storage_levelup',
                                 'link_forgotpassword'=>'/test/forgot_password'
                                ]);
    }

    public function signin() {
        return Inertia::render('1/register', ['link_login'=>'/test/login']);
    }

    public function aboutus() {
        return Inertia::render('1/about_us', ['link_login'=>'/test/login']);
    }

    public function forgot_password() {
        return Inertia::render('1/forgot_password', ['link_login'=>'/test/login']);
    }

    public function storage_levelup() {
        return Inertia::render('1/storage_levelup', ['link_login'=>'/test/login']);
    }

    public function dashboard() {
        $userprofile = $this->user->userProfile();
        $iconplayer = array("first"     => URL::to('assets/icons/player/first.svg'),
                            "previous"  => URL::to('assets/icons/player/previous.svg'),
                            "play"      => URL::to('assets/icons/player/play.svg'),
                            "next"      => URL::to('assets/icons/player/next.svg'),
                            "last"      => URL::to('assets/icons/player/last.svg'),
                            "reone"     => URL::to('assets/icons/player/reone.png'),
                            "relist"    => URL::to('assets/icons/player/relist.png'),
                            "reall"     => URL::to('assets/icons/player/reall.png'),
                            "reoff"     => URL::to('assets/icons/player/reoff.png'));
        // return $userprofile[0]['id_1001'];
        return Inertia::render('Dashboard',
                              ['userid' => 1,
                               'id1001' => $userprofile[0]['id_1001'],
                            //    'userprofile' => $userprofile,
                               'iconplayer' => $iconplayer
                              ]);
    }

}
