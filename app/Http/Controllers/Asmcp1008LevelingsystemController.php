<?php
namespace App\Http\Controllers;

use App\Models\asmcp_1008_levelingsystem;
use App\Http\Requests\Storeasmcp_1008_levelingsystemRequest;
use App\Http\Requests\Updateasmcp_1008_levelingsystemRequest;
use Inertia\Inertia;
use Route;
use URL;
use Meta;

class Asmcp1008LevelingsystemController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $title = 'Level Up!';
        $description = 'Level Up! | My Cloud Player, Your Own Media Music Player Online, that can play music online or offline wherever and whenever you are';
        $page = 'storage level up';
        meta()->set('csrf-token', csrf_token())
              ->set('charset', 'utf-8')
              ->title($title)->set('og:title', $title)
              ->description($description)
              ->set('og:description', $description)
              ->keywords('my cloud player '.$page.',
                          your own media music player online '.$page.',
                          private music player '.$page.',
                          private online music player '.$page.',
                          private offline music player '.$page.',
                          private online offline music player $page,
                          storage music player '.$page.',
                          cache music player '.$page.',
                          local music player '.$page.',
                          portable music player '.$page.',
                          free 4GB online storage music player '.$page)
              ->canonical(URL::current())
              ->robots('all')
              ->viewport('initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no')
              ->set('apple-mobile-web-app-capable', 'yes')
              ->set('apple-touch-fullscreen', 'yes')
              ->setRawTag('<link rel="preconnect" href="https://fonts.googleapis.com" />')
              ->setRawTag('<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />')
              ->setRawTag('<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif+Georgian:wght@100;400;900&family=Nunito:wght@400;700&display=swap">')
              ->setRawTag('<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>')
              ->setRawTag('<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>')
              ->setRawTag('<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>');
        $data = asmcp_1008_levelingsystem::orderBy('level', 'ASC')->get();
        return Inertia::render('1/storage_levelup', [
            'title'      => 'Level Up!',
            'footer'     => 1,
            'link_login' => '/p/login'
        ]);
    }

    public function getAll() {
        $data = asmcp_1008_levelingsystem::orderBy('level', 'ASC')->get();
        return response()->json(['msg' => 'Get All Level Description!', 'success' => 1, 'data'=>$data], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Storeasmcp_1008_levelingsystemRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storeasmcp_1008_levelingsystemRequest $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\asmcp_1008_levelingsystem  $asmcp_1008_levelingsystem
     * @return \Illuminate\Http\Response
     */
    public function show(asmcp_1008_levelingsystem $asmcp_1008_levelingsystem) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\asmcp_1008_levelingsystem  $asmcp_1008_levelingsystem
     * @return \Illuminate\Http\Response
     */
    public function edit(asmcp_1008_levelingsystem $asmcp_1008_levelingsystem) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Updateasmcp_1008_levelingsystemRequest  $request
     * @param  \App\Models\asmcp_1008_levelingsystem  $asmcp_1008_levelingsystem
     * @return \Illuminate\Http\Response
     */
    public function update(Updateasmcp_1008_levelingsystemRequest $request, asmcp_1008_levelingsystem $asmcp_1008_levelingsystem) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\asmcp_1008_levelingsystem  $asmcp_1008_levelingsystem
     * @return \Illuminate\Http\Response
     */
    public function destroy(asmcp_1008_levelingsystem $asmcp_1008_levelingsystem) {
        //
    }
}
