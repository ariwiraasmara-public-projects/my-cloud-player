<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Storeuser extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true; // false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() {
        return [
            //
            'username' => 'required|max:255|',
            'email' => 'required:email|max:255|',
            'password' => 'required',
        ];
    }
}
