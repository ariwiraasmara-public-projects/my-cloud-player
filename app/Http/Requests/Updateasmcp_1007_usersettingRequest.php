<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Updateasmcp_1007_usersettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            "theme"             => "required|string",
            "text"              => "required|string",
            "bar"               => "required|string",
            "wall_img"          => "required|string",
            "wall_heigth"       => "required|int",
            "wall_width"        => "required|int",
            "wall_size"         => "required|string",
            "wall_position"     => "required|string",
            "wall_repeat"       => "required|string",
            "wall_attachment"   => "required|string",
        ];
    }
}
