<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Response;
use App\MyLibs\myfunction;
use App\MyLibs\auth as mcrauth;

class checkHeader {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next) {
        //$response = $next($request);

        if(!isset($_SERVER['HTTP_MCR_X_ASWQ'])) {
            return Response::json(array('error'=>'There\'s No First Header! Add The First Header!'));
        }

        if(!isset($_SERVER['HTTP_ACCEPT'])) {
            return Response::json(array('error'=>'There\'s No Second Header! Add The Second Header!'));
        }

        // if( $_SERVER['HTTP_MCR_X_ASWQ'] != mcrauth::token() ){
        //     return Response::json(array('error'=>'Wrong Header!', 'data'=>[$_SERVER['HTTP_MCR_X_ASWQ'], mcrauth::token()]));
        // }

        if($_SERVER['HTTP_MCR_X_ASWQ'] != 'mC12x@12!5Of!a'){
            return Response::json(array('error'=>'Wrong Header!'));
        }

        if($_SERVER['HTTP_ACCEPT'] != 'application/json'){
            return Response::json(array('error'=>'Wrong Header!'));
        }

        return $next($request);
    }
}
