<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1011_onetimepaymentRepositoryInterface;
use App\Models\asmcp_1011_onetimepayment;
class asmcp_1011_onetimepaymentRepository implements asmcp_1011_onetimepaymentRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1011_onetimepayment $asmcp_1011_onetimepayment) {
        $this->model = $asmcp_1011_onetimepayment;
    }

    public function getAll(String $by = 'id_1011', String $orderBy = 'asc') {
        return $this->model->orderBy($by, $orderBy)->get();
    }

    public function getOne(array $where = null, String $by = 'id_1011', String $orderBy = 'asc') {
        return $this->model->where($where)->orderBy($by, $orderBy)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, int $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1011', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
