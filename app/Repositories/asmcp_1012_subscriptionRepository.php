<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1012_subscriptionRepositoryInterface;
use App\Models\asmcp_1012_subscription;
class asmcp_1012_subscriptionRepository implements asmcp_1012_subscriptionRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1012_subscription $asmcp_1012_subscription) {
        $this->model = $asmcp_1012_subscription;
    }

    public function getAll(String $by = 'id_1012', String $orderBy = 'asc') {
        return $this->model->orderBy($by, $orderBy)->get();
    }

    public function getOne(array $where = null, String $by = 'id_1012', String $orderBy = 'asc') {
        return $this->model->where($where)->orderBy($by, $orderBy)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, int $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }
    
    public function delete(String $id = null) {
        $res = $this->model->where('id_1012', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
