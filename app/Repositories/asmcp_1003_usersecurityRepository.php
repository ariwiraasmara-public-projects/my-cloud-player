<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1003_usersecurityRepositoryInterface;
use App\Models\asmcp_1003_usersecurity;
class asmcp_1003_usersecurityRepository implements asmcp_1003_usersecurityRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1003_usersecurity $asmcp_1003_usersecurity) {
        $this->model = $asmcp_1003_usersecurity;
    }

    public function getAll(String $by = 'id_1003') {
        return $this->model->orderBy($by, 'asc')->get();
    }

    public function getOne(String $id1001 = null) {
        return $this->model->where('id_1001', '=', $id)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, int $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1001', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
