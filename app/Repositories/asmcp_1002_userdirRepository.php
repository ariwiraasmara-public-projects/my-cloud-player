<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1002_userdirRepositoryInterface;
use App\Models\asmcp_1002_userdir;
class asmcp_1002_userdirRepository implements asmcp_1002_userdirRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1002_userdir $asmcp_1002_userdir) {
        $this->model = $asmcp_1002_userdir;
    }

    public function getAll(String $orderby = 'asc') {
        return $this->model->orderBy('rootdir', $orderby)->get();
    }

    public function getOne(String $id1001 = null) {
        return $this->model->where(['id_1001' => $id1001])->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(String $data = null, String $id = null) {
        $res = $this->model->where('id_1001', '=', $id)->update([
            "rootdir" => $data,
        ]);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1001', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
