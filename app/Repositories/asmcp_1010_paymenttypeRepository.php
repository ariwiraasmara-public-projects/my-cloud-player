<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1010_paymenttypeRepositoryInterface;
use App\Models\asmcp_1010_paymenttype;
class asmcp_1010_paymenttypeRepository implements asmcp_1010_paymenttypeRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1010_paymenttype $asmcp_1010_paymenttype) {
        $this->model = $asmcp_1010_paymenttype;
    }

    public function getAll(String $by = 'id_1010', String $orderBy = 'asc') {
        return $this->model->orderBy($by, $orderBy)->get();
    }

    public function getOne(String $where = null) {
        return $this->model->where($where)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(String $val = null, String $id = null) {
        $res = $this->model->where('id_1010', '=', $id)->update([
            "type" => $val,
        ]);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1010', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
