<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1013_usernotificationRepositoryInterface;
use App\Models\asmcp_1013_usernotification;
class asmcp_1013_usernotificationRepository implements asmcp_1013_usernotificationRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1013_usernotification $asmcp_1013_usernotification) {
        $this->model = $asmcp_1013_usernotification;
    }

    public function getAll(String $by = 'id_user', String $orderBy = 'asc') {
        return $this->model->orderBy($by, $orderBy)->get();
    }

    public function getOne(array $where = null, String $by = 'id_user', String $orderBy = 'asc') {
        return $this->model->where($where)->orderBy($by, $orderBy)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }

    public function softDelete(String $id = null) {
        $res = $this->model->where('id_1013', '=', $id)->update([
            'deleted_at' => date('Y-m-d'),
        ]);
        if($res) return $res;
        return 0;
    }

    public function hardDelete(String $id = null) {
        $res = $this->model->where('id_1013', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
