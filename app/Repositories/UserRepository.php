<?php
namespace App\Repositories;

use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface {

    protected $model;
    public function __construct(User $user) {
        $this->model = $user;
    }

    public function generateID() {
        // $table = \DB::select("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'mycloudplayer' AND TABLE_NAME = 'users'");
        $table = $this->model->select("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'mycloudplayer' AND TABLE_NAME = 'users'");
        if (!empty($table)) {
            return $table[0]->AUTO_INCREMENT;
        }
        return 0;
    }

    public function getAll(String $by = 'id') {
        if($this->model->orderBy($by, 'asc')->first()) return $this->model->orderBy($by, 'asc')->get();
        return null;
    }

    public function getOne(array $where = null) {
        if($this->model->where($where)->first()) return $this->model->where($where)->get();
        return null;
    }

    public function userProfile(array $where = null) {
        return $this->model->select('users.id', 'users.username', 'users.email', 'users.password', 'users.pin', 'users.tlp', 'users.remember_token',
                                   'users.path', 'asmcp_1001_userid.id_1001', 'asmcp_1001_userid.nama', 'asmcp_1001_userid.tempat_lahir', 'asmcp_1001_userid.tgl_lahir', 'asmcp_1001_userid.alamat',
                                   'asmcp_1001_userid.photo', 'asmcp_1007_usersetting.id_1007', 'asmcp_1007_usersetting.theme', 'asmcp_1007_usersetting.text', 'asmcp_1007_usersetting.bar',
                                   'asmcp_1007_usersetting.wall_img', 'asmcp_1007_usersetting.wall_height', 'asmcp_1007_usersetting.wall_width', 'asmcp_1007_usersetting.wall_size', 'asmcp_1007_usersetting.wall_position',
                                   'asmcp_1007_usersetting.wall_repeat', 'asmcp_1007_usersetting.wall_attachment', 'asmcp_1009_userstoragelevel.level'
                                  )
                          ->join('asmcp_1001_userid', 'asmcp_1001_userid.id', '=', 'users.id')
                          ->join('asmcp_1007_usersetting', 'asmcp_1007_usersetting.id_1001', '=', 'asmcp_1001_userid.id_1001')
                          ->join('asmcp_1009_userstoragelevel', 'asmcp_1009_userstoragelevel.id_1001', '=', 'asmcp_1001_userid.id_1001')
                          ->where($where)
                          ->get();
    }

    public function store(array $data = null) {
        // return $this->model->create($data);
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, int $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }

    public function softDelete(int $id = null) {
        $res = $this->model->where('id', '=', $id)->update([
            'deleted_at' => date('Y-m-d'),
        ]);
        if($res) return $res;
        return 0;
    }

    public function hardDelete(int $id = null) {
        $res = $this->model->where('id', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
