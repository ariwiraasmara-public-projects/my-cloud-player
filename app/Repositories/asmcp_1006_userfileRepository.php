<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1006_userfileRepositoryInterface;
use App\Models\asmcp_1006_userfiles;
use Log;
use Illuminate\Database\QueryException;
class asmcp_1006_userfileRepository {

    protected $model;
    public function __construct(asmcp_1006_userfiles $asmcp_1006_userfile) {
        $this->model = $asmcp_1006_userfile;
    }

    // $id1005 = $id1001.'@MFL:0000000000000001';
    public function getID($id) {
        if($this->model->where(['id_1001'=>$id])->first()) {
            $data   = $this->model->where(['id_1001'=>$id])->orderBy('id_1006', 'desc')->first();
            $strpos = (int)strpos($data['id_1006'], ":") + 1;
            $substr = (int)substr($data['id_1006'], $strpos)+1;
            return $id.'@MFL:'.str_pad($substr, 16, "0", STR_PAD_LEFT);
        }
        return $id.'@MFL:'.str_pad(1, 16, "0", STR_PAD_LEFT);
    }

    public function getAll(String $id1 = null, String $id2 = null, String $order = 'filename', String $by = 'asc', String $fp = 'folder') {
        $res = $this->model->where(['asmcp_1006_userfile.id_1001' => $id1])
                           ->where(function ($query) use ($id2) {
                                $query->where(['asmcp_1006_userfile.folder' => $id2])
                                      ->orWhere(['asmcp_1006_userfile.playlist' => $id2]);
                           });

        if ($res->first()) {
            if ($fp == 'playlist') {
                return $res->select('asmcp_1006_userfile.id_1006 as id1006',
                                    'asmcp_1006_userfile.filename as filename',
                                    'asmcp_1006_userfile.genre as genre',
                                    'asmcp_1006_userfile.artist as artist',
                                    'asmcp_1006_userfile.album as album',
                                    'asmcp_1006_userfile.composer as composer',
                                    'asmcp_1006_userfile.publisher as publisher',
                                    'asmcp_1006_userfile.ket as ket',
                                    'asmcp_1006_userfile.folder as idfolder',
                                    'asmcp_1006_userfile.playlist as idplaylist',
                                    'asmcp_1005_userplaylists.playlist as playlist')
                            ->leftJoin('asmcp_1005_userplaylists', 'asmcp_1005_userplaylists.id_1005', '=', 'asmcp_1006_userfile.playlist')
                            ->orderBy($order, $by)->all();
            } else if ($fp == 'folder') {
                return $res->select('asmcp_1006_userfile.id_1006 as id1006',
                                    'asmcp_1006_userfile.filename as filename',
                                    'asmcp_1006_userfile.genre as genre',
                                    'asmcp_1006_userfile.artist as artist',
                                    'asmcp_1006_userfile.album as album',
                                    'asmcp_1006_userfile.composer as composer',
                                    'asmcp_1006_userfile.publisher as publisher',
                                    'asmcp_1006_userfile.ket as ket',
                                    'asmcp_1006_userfile.folder as idfolder',
                                    'asmcp_1006_userfile.playlist as idplaylist',
                                    'asmcp_1004_userfolder.foldername as foldername')
                            ->leftJoin('asmcp_1004_userfolder', 'asmcp_1004_userfolder.id_1004', '=', 'asmcp_1006_userfile.folder')
                            ->orderBy($order, $by)->get();
            }
        }
        return 0;
    }

    public function get(String $id1006 = null) {
        return $this->model->where('id_1006', '=', $id1001)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, int $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1006', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
