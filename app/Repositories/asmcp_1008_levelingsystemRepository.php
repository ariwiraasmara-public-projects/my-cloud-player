<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1008_levelingsystemRepositoryInterface;
use App\Models\asmcp_1008_levelingsystem;
class asmcp_1008_levelingsystemRepository implements asmcp_1008_levelingsystemRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1008_levelingsystem $asmcp_1008_levelingsystem) {
        $this->model = $asmcp_1008_levelingsystem;
    }

    public function getAll() {
        return $this->model->orderBy('id', 'asc')->get();
    }

    public function getOne(array $where = null, String $by = 'id_1008', String $orderBy = 'asc') {
        return $this->model->where($where)->orderBy($by, $orderBy)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $res = $this->model->where('id_1008', '=', $id)->update($data);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1008', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
