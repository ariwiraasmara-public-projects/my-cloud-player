<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1005_userplaylistRepositoryInterface;
use App\Models\asmcp_1005_userplaylists;
class asmcp_1005_userplaylistRepository implements asmcp_1005_userplaylistRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1005_userplaylists $asmcp_1005_userplaylists) {
        $this->model = $asmcp_1005_userplaylists;
    }

    // $id1005 = $id1001.'@MPL:0000000000000001';
    public function getID($id) {
        if($this->model->where(['id_1001'=>$id])->first()) {
            $data   = $this->model->where(['id_1001'=>$id])->orderBy('id_1005', 'desc')->first();
            $strpos = (int)strpos($data['id_1005'], ":") + 1;
            $substr = (int)substr($data['id_1005'], $strpos)+1;
            return $id.'@MPL:'.str_pad($substr, 16, "0", STR_PAD_LEFT);
        }
        return $id.'@MPL:'.str_pad(1, 16, "0", STR_PAD_LEFT);
    }

    public function getAll(String $id = null, String $orderby = 'asc') {
        return $this->model->where(['id_1005' => $id])->get();
        // return $this->model->all();
    }

    public function get(String $id1005 = null) {
        return $this->model->where(['id_1005' => $id1005])->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(String $val = null, String $id = null) {
        $res = $this->model->where('id_1005', '=', $id)->update([
            "playlist" => $val,
        ]);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1005', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
