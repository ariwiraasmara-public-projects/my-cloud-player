<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1001_useridRepositoryInterface;
use App\Models\asmcp_1001_userid;
class asmcp_1001_useridRepository implements asmcp_1001_useridRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1001_userid $asmcp_1001_userid) {
        $this->model = $asmcp_1001_userid;
    }

    public function getAll(String $by = 'nama') {
        return $this->model->orderBy($by, 'asc')->get();
    }

    public function getOne(String $id1001 = null) {
        return $this->model->where('id_1001', '=', $id1001)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, int $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        return $this->model->where('id_1001', '=', $id)->delete();
    }

}
?>
