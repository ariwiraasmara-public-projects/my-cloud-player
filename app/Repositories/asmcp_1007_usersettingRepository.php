<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1007_usersettingRepositoryInterface;
use App\Models\asmcp_1007_usersetting;
class asmcp_1007_usersettingRepository implements asmcp_1007_usersettingRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1007_usersetting $asmcp_1007_usersetting) {
        $this->model = $asmcp_1007_usersetting;
    }

    public function getAll(String $by = 'id_1001') {
        return $this->model->orderBy($by, 'asc')->get();
    }

    public function get(String $id001 = null) {
        return $this->model->where('id_1001', '=', $id001)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $res = $this->model->where('id_1001', '=', $id)->update($data);
        if($res) return $res;
        return 0;
    }


    public function delete(String $id = null) {
        $res = $this->model->where('id_1001', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
