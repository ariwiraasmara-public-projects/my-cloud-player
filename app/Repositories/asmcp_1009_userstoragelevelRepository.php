<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1009_userstoragelevelRepositoryInterface;
use App\Models\asmcp_1009_userstoragelevel;
class asmcp_1009_userstoragelevelRepository implements asmcp_1009_userstoragelevelRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1009_userstoragelevel $asmcp_1009_userstoragelevel) {
        $this->model = $asmcp_1009_userstoragelevel;
    }

    public function getAll(String $by = 'id_1009') {
        return $this->model->orderBy($by, 'asc')->get();
    }

    public function getOne(String $id1001 = null) {
        return $this->model->where('id_1001', '=', $id1001)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $res = $this->model->where('id_1001', '=', $id)->update($data);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1001', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
