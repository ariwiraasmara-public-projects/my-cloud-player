<?php
namespace App\Repositories;

use App\Interfaces\Repositories\asmcp_1004_userfolderRepositoryInterface;
use App\Models\asmcp_1004_userfolders;
class asmcp_1004_userfolderRepository implements asmcp_1004_userfolderRepositoryInterface {

    protected $model;
    public function __construct(asmcp_1004_userfolders $asmcp_1004_userfolders) {
        $this->model = $asmcp_1004_userfolders;
    }

    // $id1004 = 'MCP:'.$pubuserid.'@FOLDER:0000000000000001';
    public function getID($id) {
        if($this->model->where(['id_1001'=>$id])->first()) {
            $data   = $this->model->where(['id_1001'=>$id])->orderBy('id_1004', 'desc')->first();
            $strpos = (int)strpos($data['id_1004'], ":") + 1;
            $substr = (int)substr($data['id_1004'], $strpos)+1;
            return $id.'@FOLDER:'.str_pad($substr, 16, "0", STR_PAD_LEFT);
        }
        return $id.'@FOLDER:'.str_pad(1, 16, "0", STR_PAD_LEFT);
    }

    public function getAll(String $id1001 = null, String $by = 'foldername', String $orderby = 'asc') {
        return $this->model->where('id_1001', '=', $id1001)->orderBy($by, $orderby)->get();
    }

    public function get(String $id1004 = null) {
        return $this->model->where('id_1004', '=', $id1004)->get();
    }

    public function store(array $data = null) {
        $res = $this->model->create($data);
        if($res) return $res;
        return 0;
    }

    public function update(array $data = null, int $id = null) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->model->where('id_1004', '=', $id)->delete();
        if($res) return $res;
        return 0;
    }

}
?>
