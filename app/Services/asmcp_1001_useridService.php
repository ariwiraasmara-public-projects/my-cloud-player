<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1001_useridServiceInterface;
use App\Repositories\asmcp_1001_useridRepository;
use Illuminate\Support\Collection;
class asmcp_1001_useridService implements asmcp_1001_useridServiceInterface {

    protected $repo;
    public function __construct(asmcp_1001_useridRepository $asmcp_1001_useridRepository) {
        $this->repo = $asmcp_1001_useridRepository;
    }

    public function store(array $data = null) {
        $data = $this->repo->store([
            "id_1001"       => $data['id1001'],
            "id"            => $data['id_user'],
            "nama"          => $data['nama'],
            "photo"         => 'user.png',
        ]);
        if($data > 0) return 1;
        return 0;
    }

    public function getUserEditProfile(String $id1001 = null) {
        $data = $this->repo->getOne($id1001);
        return collection(['nama'        => $data[0]['nama'],
                           'pass'        => $this->fun->decrypt($data[0]['password']),//$data[0]['password'],
                           'pin'         => $data[0]['pin'],
                           'tlp'         => $data[0]['tlp'],
                           'tgl_lahir'   => $data[0]['tgl_lahir']]);
        // return $dafi;
    }

    public function updateProfile(array $data, int $id = null) {
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

}
?>
