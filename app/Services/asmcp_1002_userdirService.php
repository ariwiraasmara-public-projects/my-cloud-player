<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1002_userdirServiceInterface;
use App\Repositories\asmcp_1002_userdirRepository;
class asmcp_1002_userdirService implements asmcp_1002_userdirServiceInterface {

    protected $repo;
    public function __construct(asmcp_1002_userdirRepository $asmcp_1002_userdirRepository) {
        $this->repo = $asmcp_1002_userdirRepository;
    }

    public function get(String $id = null) {
        return $this->repo->getOne($id);
        // return $data;
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(String $data = null, String $id = null){
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null){
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
