<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1011_onetimepaymentServiceInterface;
use App\Repositories\asmcp_1011_onetimepaymentRepository;
class asmcp_1011_onetimepaymentService implements asmcp_1011_onetimepaymentServiceInterface {

    protected $repo;
    public function __construct(asmcp_1011_onetimepaymentRepository $asmcp_1011_onetimepaymentRepository) {
        $this->repo = $asmcp_1011_onetimepaymentRepository;
    }

    public function getAll(String $by = 'id_1011', String $orderBy = 'asc') {
        return $this->repo->getAll($by, $orderBy);
    }

    public function getOne(array $where = null, String $by = 'id_1011', String $orderBy = 'asc') {
        return $this->repo->getOne($where, $by, $orderBy);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $data = $this->repo->updateAll($data, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
