<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use App\Interfaces\Services\UserServiceInterface;
use App\Libraries\myfunction;
use App\Repositories\UserRepository;
use App\Repositories\asmcp_1001_useridRepository;
use App\Repositories\asmcp_1002_userdirRepository;
use App\Repositories\asmcp_1003_usersecurityRepository;
use App\Repositories\asmcp_1004_userfolderRepository;
use App\Repositories\asmcp_1005_userplaylistRepository;
use App\Repositories\asmcp_1006_userfileRepository;
use App\Repositories\asmcp_1007_usersettingRepository;
use App\Repositories\asmcp_1009_userstoragelevelRepository;
use App\Repositories\asmcp_1010_paymenttypeRepository;
use App\Repositories\asmcp_1011_onetimepaymentRepository;
use App\Repositories\asmcp_1012_subscriptionRepository;
use App\Repositories\asmcp_1013_usernotificationRepository;
use File;
class UserService implements UserServiceInterface {

    protected $fun;
    protected $repository;
    protected $rp1001;
    protected $rp1002;
    protected $rp1003;
    protected $rp1004;
    protected $rp1005;
    protected $rp1006;
    protected $rp1007;
    protected $rp1009;
    protected $rp1010;
    protected $rp1011;
    protected $rp1012;
    protected $rp1013;
    public function __construct(myfunction $fun,
                                UserRepository $user,
                                asmcp_1001_useridRepository $rp1001,
                                asmcp_1002_userdirRepository $rp1002,
                                asmcp_1003_usersecurityRepository $rp1003,
                                asmcp_1004_userfolderRepository $rp1004,
                                asmcp_1005_userplaylistRepository $rp1005,
                                asmcp_1006_userfileRepository $rp1006,
                                asmcp_1007_usersettingRepository $rp1007,
                                asmcp_1009_userstoragelevelRepository $rp1009,
                                asmcp_1010_paymenttypeRepository $rp1010,
                                asmcp_1011_onetimepaymentRepository $rp1011,
                                asmcp_1012_subscriptionRepository $rp1012,
                                asmcp_1013_usernotificationRepository $rp1013) {
        $this->fun = $fun;
        $this->repository = $user;
        $this->rp1001 = $rp1001;
        $this->rp1002 = $rp1002;
        $this->rp1003 = $rp1003;
        $this->rp1004 = $rp1004;
        $this->rp1005 = $rp1005;
        $this->rp1006 = $rp1006;
        $this->rp1007 = $rp1007;
        $this->rp1009 = $rp1009;
        $this->rp1010 = $rp1010;
        $this->rp1011 = $rp1011;
        $this->rp1012 = $rp1012;
        $this->rp1013 = $rp1013;
    }

    public function login(string $user = null, String $pass = null) {
        $where = array('username' => $user);
        $cekUser = $this->repository->getOne($where);

        if( is_null($cekUser) ) return collect(['error'=>1]); //'Wrong Username / Email';
        if( !($pass == $this->fun->decrypt($cekUser[0]['password'])) ) return collect(['error'=>2]); //'Wrong Password!';

        return collect(['success' => $this->repository->userProfile($where)]);
    }

    public function store(array $data = null) {
        $check = $this->repository->getOne(['username'=>$data['username']]);
        if($check) return collect(['error' => 2]);

        $data = $this->repository->store([
            'username'          => $data['username'],
            'email'             => $data['email'],
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password'          => $this->fun->encrypt($data['password']),
            'path'              => null,
            'remember_token'    => $this->fun->random('combwisp', 10),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'         => null,
        ]);

        if($data) {
            $pubuserid = $this->fun->random('char', 3).$data['id'].$this->fun->random('numb', 4);
            $id1001 = 'MCP:'.$pubuserid;
            $this->rp1001->store([
                'id_1001'=> $id1001,
                'id'     => $data['id'],
            ]);

            $id1002 = 'MCP@ROOTDIR:'.$this->fun->random('numb',3).$data['id'].$this->fun->random('numb',3).'@\''.$this->fun->random('', 7).'\'';
            $path = public_path('userdir/').$pubuserid;
            $this->rp1002->store([
                "id_1001"  => $id1001,
                "rootdir"  => $path
            ]);
            File::makeDirectory($path, $mode = 0777, true, true);

            $this->rp1003->store([
                "id_1001" => $id1001
            ]);

            $id1004 = $id1001.'@FOLDER:0000000000000001';
            $foldername = 'My Music';
            $this->rp1004->store([
                "id_1004"    => $id1004,
                "id_1001"    => $id1001,
                "foldername" => $foldername,
                "ket"        => 'Folder Musikku',
            ]);
            File::makeDirectory($path.'/'.$foldername, $mode = 0777, true, true);

            $id1005 = $id1001.'@MPL:0000000000000001';
            $this->rp1005->store([
                "id_1005"  => $id1005,
                "id_1001"  => $id1001,
                "playlist" => 'default',
            ]);

            $this->rp1007->store([
                "id_1001" => $id1001,
                "theme"   => '#0A758F',
                "text"    => '#000',
                "bar"     => '#046ffb',

            ]);

            $this->rp1009->store([
                "id_1001" => $id1001,
                "level"   => 1,
            ]);

            $id1010 = 'MPT:'.$pubuserid;
            $this->rp1010->store([
                "id_1010" => $id1010,
                "id_1001" => $id1001,
                "type"    => 0,
            ]);

            $this->rp1012->store([
                "id_1012"   => 'MSUBS:'.$pubuserid,
                "id_1010"   => $id1010,
                "is_active" => 0,
                "is_trial"  => 1,
                "datepay"   => null,
                "amount"    => 0,
                "tilldate"  => null,
            ]);

            return collect([
                'username'       => $data['username'],
                'email'          => $data['email'],
                'password'       => $this->fun->encrypt($data['password']),
                'remember_token' => $this->fun->random('combwisp', 10),
                'created_at'     => date('Y-m-d H:i:s'),
                'error'          => 0
            ]);
        }
        return collect(['error' => 1]);
    }

    public function update(array $data = null, int $id = 0, int $type = 0) {
        return match($type) {
            1 => $this->repository->update($data, $id),
            2 => $this->rp1001->update($data, $id),
            default => 0
        };
    }

    public function forgot_password() {

    }

    public function generateToken(string $email = null, string $password = null) {
        /*
        $validate = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (!auth()->attempt($validate)) {
            return response(['message' => 'Invalid credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response(['user' => auth()->user(), 'access_token' => $accessToken]);
        */
    }

    public function getUserProfile(array $where = null) {
        $data = $this->repository->userProfile($where);
        return collect([
            'name'              => $data[0]['nama'],
            'username'          => $data[0]['username'],
            'email'             => $data[0]['email'],
            'level'             => $data[0]['level'],
            'tempat_lahir'      => $data[0]['tempat_lahir'],
            'tgl_lahir'         => $data[0]['tgl_lahir'],
            'tlp'               => $data[0]['tlp'],
            'pin'               => $data[0]['pin'],
        ]);
    }

    public function getUserEditProfile(array $where = null) {
        $data = $this->repository->userProfile($where);
        return collect([
            'name'      => $data[0]['nama'],
            'tgl_lahir' => $data[0]['tgl_lahir'],
            'tlp'       => $data[0]['tlp'],
            'password'  => $this->fun->decrypt($data[0]['password']),
            'pin'       => $data[0]['pin']
        ]);
    }

    public function getUserSetting(array $where = null) {
        $data = $this->repository->userProfile($where);
        return collect([
            "wall_img"          => $data[0]['wall_img'],
            "wall_heigth"       => $data[0]['wall_heigth'],
            "wall_width"        => $data[0]['wall_width'],
            "wall_size"         => $data[0]['wall_size'],
            "wall_position"     => $data[0]['wall_position'],
            "wall_repeat"       => $data[0]['wall_repeat'],
            "wall_attachment"   => $data[0]['wall_attachment'],
        ]);
    }

    public function delete(array $data = null) {
        return match($data['type']) {
            'soft' => $this->repository->softDelete($data['id']),
            'hard' => $this->repository->hardDelete($data['id']),
            default => 0
        };
    }

}
?>
