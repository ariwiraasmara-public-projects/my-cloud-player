<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1009_userstoragelevelServiceInterface;
use App\Repositories\asmcp_1009_userstoragelevelRepository;
class asmcp_1009_userstoragelevelService implements asmcp_1009_userstoragelevelServiceInterface {

    protected $repo;
    public function __construct(asmcp_1009_userstoragelevelRepository $asmcp_1009_userstoragelevelRepository) {
        $this->repo = $asmcp_1009_userstoragelevelRepository;
    }

    public function getAll(String $by = 'id_1009') {
        return $this->repo->getAll($by);
    }

    public function getOne(String $id1001 = null) {
        return $this->repo->getOne($id1001);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
