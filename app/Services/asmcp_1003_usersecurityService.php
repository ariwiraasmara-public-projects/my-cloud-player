<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1003_usersecurityServiceInterface;
use App\Repositories\asmcp_1003_usersecurityRepository;
class asmcp_1003_usersecurityService implements asmcp_1003_usersecurityServiceInterface {

    protected $repo;
    public function __construct(asmcp_1003_usersecurityRepository $asmcp_1003_usersecurityRepository) {
        $this->repo = $asmcp_1003_usersecurityRepository;
    }

    public function get(String $id = null) {
        return $this->repo->getOne($id);
        // return $data;
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
