<?php
namespace App\Libraries;

use App\Interfaces\generateIDServiceInterface;
use App\Models\asmcp_1001_userid;
use App\Models\asmcp_1002_userdir;
use App\Models\asmcp_1003_usersecurity;
use App\Models\asmcp_1004_userfolders;
use App\Models\asmcp_1006_userfiles;
use App\Models\asmcp_1005_userplaylists;
use App\Models\asmcp_1007_usersetting;

use App\MyLibs\myfunction;

class generateIDService implements generateIDServiceInterface {

    protected int $rucid = 0;
    protected string $pubuserid = '';

    protected string $id1001 = '';
    protected string $id1002 = '';
    protected string $id1003 = '';
    protected string $id1004 = '';
    protected string $id1005 = '';
    protected string $id1006 = '';
    protected string $id1007 = '';

    public function __construct() {
        $this->$id1001 = '__null__';
    }


    public function getID() {
        $table = \DB::select("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'mycloudplayer' AND TABLE_NAME = 'users'");
        if (!empty($table)) {
            return $table[0]->AUTO_INCREMENT;
        }
    }

    public function setRUCID(int $val = null) {
        $this->$rucid = $val;
    }

    public function getRUCID() {
        return $this->$rucid;
    }

    public function setPUBUSERID(string $str = null) {
        $this->$pubuserid = $str;
    }

    public function getPUBUSERID() {
        return $this->$pubuserid;
    }

    public function getID1001() {
        $count = asmcp_1001_userid::count();
        if($count > 0) {
            //echo 'data 1001 : ';
            $data = asmcp_1001_userid::orderByRaw('SUBSTRING_INDEX(id_1001, 10, 4) DESC')->limit(1)->get();
            $this->setRUCID((int)substr($data[0]['id_1001'], 10, 1) + 1);
            $this->setPUBUSERID(myfunction::random('char', 3).$this->$rucid.myfunction::random('numb', 4));
            //return $pubuserid;
        }
        else {
            //echo 'data 1001 kosong, ';
            $this->setPUBUSERID(myfunction::random('char', 3).'1'.myfunction::random('numb', 4));
        }
        $this->setID1001('MCP:'.$this->getPUBUSERID());
        return 'MCP:'.$this->getPUBUSERID();
    }

    public function setID1001(string $id = null) {
        $this->$id1001 = $id;
    }

    public function SID1001() {
        return $this->$id1001;
    }

    protected string $path;
    public function getID1002() {
        $raw = myfunction::random('numb',3).$this->$rucid.myfunction::random('numb',3).'@\''.myfunction::random('', 7).'\'';
        $id1002 = 'MCP@ROOTDIR:'.$raw;
        $this->setUserPath('userdir/'.$raw);
        return $id1002;
    }

    protected function setUserPath(string $path) {
        $this->$path = $path;
    }

    public function getUserPath() {
        return $this->$path;
    }

    public function getID1003() {
        return 'MCPS:'.$this->getPUBUSERID();
    }

    public function getID1004(string $id1001 = null) {
        //$id1001 = 'MCP$ID#GfJ16832';
        $count = asmcp_1004_userfolders::where('id_1001', '=', $id1001)->orderBy('id_1004', 'DESC')->count();
        if($count > 0) {
            $data = asmcp_1004_userfolders::where('id_1001', '=', $id1001)->orderBy('id_1004', 'DESC')->limit(1)->get();
            $counter = str_pad((int)substr($data[0]['id_1004'], -16) + 1, 16, 0, STR_PAD_LEFT);
            return $id1001.'@FOLDER:'.$counter;
        }
        else {
            return $id1001.'@FOLDER:0000000000000001';
        }
    }

    public function getID1005(string $id1001 = null) {
        //$id1001 = 'MCP$ID#GfJ16832';
        $count = asmcp_1005_userplaylists::where('id_1001', '=', $id1001)->orderBy('id_1006', 'DESC')->count();
        if($count > 0) {
            $data = asmcp_1005_userplaylists::where('id_1001', '=', $id1001)->orderBy('id_1006', 'DESC')->limit(1)->get();
            $counter = str_pad((int)substr($data[0]['id_1006'], -16) + 1, 16, 0, STR_PAD_LEFT);
            return $id1001.'@MPL:'.$counter;
        }
        else {
            return $id1001.'@MPL:0000000000000001';
        }
    }

    public function getID1006(string $id1001 = null) {
        //$id1001 = 'MCP$ID#GfJ16832';
        $count = asmcp_1006_userfiles::where('id_1001', '=', $id1001)->orderBy('id_1005', 'DESC')->count();
        if($count > 0) {
            $data = asmcp_1006_userfiles::where('id_1001', '=', $id1001)->orderBy('id_1005', 'DESC')->limit(1)->get();
            $counter = str_pad((int)substr($data[0]['id_1005'], -16) + 1, 16, 0, STR_PAD_LEFT);
            return $id1001.'@MFL:'.$counter;
        }
        else {
            return $id1001.'@MFL:0000000000000001';
        }
    }

    public function getID1007() {
        return 'MCPUS:'.$this->getPUBUSERID();
    }

    public function getID1009() {
        return 'MCPUSL:'.$this->getPUBUSERID();
    }

    public function getID1010() {
        return 'MCPUPT:'.$this->getPUBUSERID();
    }

    public function getID1011() {
        return 'MCPUOTP'.$this->getPUBUSERID();
    }

    public function getID1012() {
        return 'MCPUSB'.$this->getPUBUSERID();
    }

    public function getID1013() {
        return 'MCPUN'.$this->getPUBUSERID();
    }

}
?>
