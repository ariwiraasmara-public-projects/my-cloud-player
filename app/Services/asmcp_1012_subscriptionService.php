<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1012_subscriptionServiceInterface;
use App\Repositories\asmcp_1012_subscriptionRepository;
class asmcp_1012_subscriptionService implements asmcp_1012_subscriptionServiceInterface {

    protected $repo;
    public function __construct(asmcp_1012_subscriptionRepository $asmcp_1012_subscriptionRepository) {
        $this->repo = $asmcp_1012_subscriptionRepository;
    }

    public function getAll(String $by = 'id_1012', String $orderBy = 'asc') {
        return $this->repo->getAll($by, $orderBy);
    }

    public function getOne(array $where = null, String $by = 'id_1012', String $orderBy = 'asc') {
        return $this->repo->getOne($where, $by, $orderBy);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
