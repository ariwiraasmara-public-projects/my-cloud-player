<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1008_levelingsystemServiceInterface;
use App\Repositories\asmcp_1008_levelingsystemRepository;
class asmcp_1008_levelingsystemService implements asmcp_1008_levelingsystemServiceInterface {

    protected $repo;
    public function __construct(asmcp_1008_levelingsystemRepository $asmcp_1008_levelingsystemRepository) {
        $this->repo = $asmcp_1008_levelingsystemRepository;
    }

    public function getAll() {
        return $this->repo->getAll();
    }

    public function getOne(array $where = null, String $by = 'id_1008', String $orderBy = 'asc') {
        return $this->repo->getOne($where, $by, $orderBy);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
