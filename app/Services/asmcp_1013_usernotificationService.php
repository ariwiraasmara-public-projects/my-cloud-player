<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1013_usernotificationServiceInterface;
use App\Repositories\asmcp_1013_usernotificationRepository;
class asmcp_1013_usernotificationService implements asmcp_1013_usernotificationServiceInterface {

    protected $repo;
    public function __construct(asmcp_1013_usernotificationRepository $asmcp_1013_usernotificationRepository) {
        $this->repo = $asmcp_1013_usernotificationRepository;
    }

    public function getAll(String $by = 'id_user', String $orderBy = 'asc') {
        return $this->repo->getAll();
    }

    public function getOne(array $where = null, String $by = 'id_user', String $orderBy = 'asc') {
        return $this->repo->getOne($where);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

    public function softDelete(String $id = null) {
        $data = $this->repo->softDelete($id);
        if($data) return 1;
        return 0;
    }

    public function hardDelete(String $id = null) {
        $data = $this->repo->hardDelete($id);
        if($data) return 1;
        return 0;
    }

}
?>
