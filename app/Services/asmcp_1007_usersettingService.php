<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Interfaces\Services\asmcp_1007_usersettingServiceInterface;
use App\Repositories\asmcp_1007_usersettingRepository;
use App\Libraries\myfunction;

class asmcp_1007_usersettingService implements asmcp_1007_usersettingServiceInterface {

    protected $repo;
    public function __construct(asmcp_1007_usersettingRepository $asmcp_1007_usersettingRepository) {
        $this->repo = $asmcp_1007_usersettingRepository;
    }

    public function get(String $id1001 = null) {
        return $this->repo->get($id1001);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return $data;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $res = $this->repo->update($data, $id);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->repo->delete($id);
        if($res) return 1;
        return 0;
    }

}
?>
