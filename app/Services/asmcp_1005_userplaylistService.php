<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1005_userplaylistServiceInterface;
use App\Repositories\asmcp_1005_userplaylistRepository;
class asmcp_1005_userplaylistService implements asmcp_1005_userplaylistServiceInterface {

    protected $repo;
    public function __construct(asmcp_1005_userplaylistRepository $asmcp_1005_userplaylistRepository) {
        $this->repo = $asmcp_1005_userplaylistRepository;
    }

    public function getID($id) {
        return $this->repo->getID($id);
    }

    public function getAll(String $id = null, String $orderby = 'asc') {
        return $this->repo->getAll($id, $orderby);
    }

    public function get(String $id = null) {
        return $this->repo->get($id);
    }

    public function store(String $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(String $val = null, String $id = null) {
        $data = $this->repo->update($val, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
