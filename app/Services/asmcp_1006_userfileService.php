<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1006_userfileServiceInterface;
use App\Repositories\asmcp_1006_userfileRepository;
class asmcp_1006_userfileService implements asmcp_1006_userfileServiceInterface {

    protected $repo;
    public function __construct(asmcp_1006_userfileRepository $asmcp_1006_userfileRepository) {
        $this->repo = $asmcp_1006_userfileRepository;
    }

    public function getID($id) {
        return $this->repo->getID($id);
    }

    public function getAll(String $id1 = null, String $id2 = null, String $order = 'filename', String $by = 'asc', String $fp = 'folder') {
        return $this->repo->getAll($id1, $id2, $order, $by, $fp);
    }

    public function get(String $id1006 = null) {
        return $this->repo->get($id1006);
    }

    public function store(String $data = null) {
        $res = $this->repo->store($data);
        if($res) return 1;
        return 0;
    }

    public function update(String $data = null, String $id = null) {
        $res = $this->repo->update($data, $id);
        if($res) return $res;
        return 0;
    }

    public function delete(String $id = null) {
        $res = $this->repo->delete($id);
        if($res) return 1;
        return 0;
    }

}
?>
