<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1004_userfolderServiceInterface;
use App\Repositories\asmcp_1004_userfolderRepository;
class asmcp_1004_userfolderService implements asmcp_1004_userfolderServiceInterface {

    protected $repo;
    public function __construct(asmcp_1004_userfolderRepository $asmcp_1004_userfolderRepository) {
        $this->repo = $asmcp_1004_userfolderRepository;
    }

    public function getID($id) {
        return $this->repo->getID($id);
    }

    public function getAll(String $id = null, String $by = null, String $orderby = 'asc') {
        return $this->repo->getAll($id, $by, $orderby);
    }

    public function get(String $id = null) {
        return $this->repo->get($id);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(array $data = null, String $id = null) {
        $data = $this->repo->update($data, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
