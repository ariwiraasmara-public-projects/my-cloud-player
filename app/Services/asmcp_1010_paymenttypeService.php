<?php
namespace App\Services;

use App\Interfaces\Services\asmcp_1010_paymenttypeServiceInterface;
use App\Repositories\asmcp_1010_paymenttypeRepository;
class asmcp_1010_paymenttypeService implements asmcp_1010_paymenttypeServiceInterface {

    protected $repo;
    public function __construct(asmcp_1010_paymenttypeRepository $asmcp_1010_paymenttypeRepository) {
        $this->repo = $asmcp_1010_paymenttypeRepository;
    }

    public function getAll(String $by = 'id_1010', String $orderBy = 'asc') {
        return $this->repo->getAll($by, $orderBy);
    }

    public function getOne(array $where = null) {
        return $this->repo->getOne($where);
    }

    public function store(array $data = null) {
        $data = $this->repo->store($data);
        if($data) return 1;
        return 0;
    }

    public function update(String $val = null, String $id = null) {
        $data = $this->repo->updateAll($val, $id);
        if($data) return 1;
        return 0;
    }

    public function delete(String $id = null) {
        $data = $this->repo->delete($id);
        if($data) return 1;
        return 0;
    }

}
?>
