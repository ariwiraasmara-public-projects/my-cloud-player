<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class asmcp_1008_levelingsystemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
