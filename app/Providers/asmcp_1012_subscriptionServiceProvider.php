<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class asmcp_1012_subscriptionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
