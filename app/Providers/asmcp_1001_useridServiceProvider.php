<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class asmcp_1001_useridServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
